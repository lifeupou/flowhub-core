--liquibase formatted sql

--changeset daniel@kodality.com:config_flowable_users
--delete from act_id_membership;
--delete from act_id_group;
--delete from act_id_user;
--delete from act_id_priv_mapping;

--insert into act_id_user (id_, email_, pwd_) values ('admin', 'admin', 'test');
--insert into act_id_priv_mapping(id_, priv_id_, group_id_) (select id_ || '-group-wfm.admin', id_, 'wfm.admin' from act_id_priv);
--insert into act_id_priv_mapping(id_, priv_id_, user_id_) (select id_ || '-user-admin', id_, 'admin' from act_id_priv);
-- rollback select 1





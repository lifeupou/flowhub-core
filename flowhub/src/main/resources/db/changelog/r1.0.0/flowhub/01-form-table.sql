CREATE TABLE flowhub_form (
  key        TEXT PRIMARY KEY,
  definition JSONB NOT NULL,
  labels     JSONB,
  names      JSONB,
  engine     JSONB
  );
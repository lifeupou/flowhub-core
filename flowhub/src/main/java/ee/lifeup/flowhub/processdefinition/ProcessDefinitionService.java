/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.processdefinition;

import ee.lifeup.flowhub.form.FormInfo;
import lombok.AllArgsConstructor;
import org.flowable.engine.FormService;
import org.flowable.engine.RepositoryService;
import org.flowable.form.api.FormRepositoryService;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class ProcessDefinitionService {
  private final RepositoryService repositoryService;
  private final FormService formService;
  private final FormRepositoryService formRepositoryService;

  public List<ProcessDefinition> getProcessDefinitions() {
    return repositoryService.createProcessDefinitionQuery().latestVersion().list().stream().map(flProc -> {
      ProcessDefinition proc = new ProcessDefinition(flProc);
      if (flProc.hasStartFormKey()) {
        String formKey = formService.getStartFormKey(flProc.getId());
        proc.setForm(new FormInfo(formRepositoryService.getFormModelByKey(formKey)));
      }
      return proc;
    }).collect(toList());
  }

}

package ee.lifeup.flowhub.notification;

import ee.lifeup.flowhub.task.Task;
import ee.lifeup.flowhub.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.flowable.idm.api.User;
import org.flowable.task.service.impl.persistence.entity.TaskEntityImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static ee.lifeup.flowhub.util.DateUtil.SIMPLE_FORMAT;

@Slf4j
@Component
public class EmailNotificationService {

  private static final String SENDER_PERSONAL = "Tallinna Tehnikaülikooli teabehaldussüsteem(THS)";
  private static final String OVERDUE_COLOR = "color: rgb(255, 0, 0)";

  private final JavaMailSender sender;
  private final ClassPathResource logo;

  private final String taskAssignedTemplate;
  private final String defaultSenderEmail;
  private final String tasksDueTemplate;
  private final String taskDueRow;
  private final String webBaseUrl;

  @Autowired
  public EmailNotificationService(JavaMailSender sender,
                                  @Value("${spring.mail.username}") String defaultSenderEmail,
                                  @Value("${web.base.url}") String webBaseUrl,
                                  @Value("classpath:mail/task-assigned-template.html") Resource taskAssignedTemplate,
                                  @Value("classpath:mail/task-due-row.html") Resource taskDueRow,
                                  @Value("classpath:mail/tasks-due-template.html") Resource tasksDueTemplate
  ) throws IOException {
    this.defaultSenderEmail = defaultSenderEmail;
    this.webBaseUrl = webBaseUrl;
    this.taskAssignedTemplate = IOUtils.toString(taskAssignedTemplate.getInputStream(), Charset.defaultCharset());
    this.tasksDueTemplate = IOUtils.toString(tasksDueTemplate.getInputStream(), Charset.defaultCharset());
    this.taskDueRow = IOUtils.toString(taskDueRow.getInputStream(), Charset.defaultCharset());
    this.logo = new ClassPathResource("mail/logo.jpg");
    this.sender = sender;
  }


  public void notifyTasksOverdue(User user, List<Task> tasks) throws MessagingException {
    log.info("Task overdue notifications ({}) to user: {} with email {}", tasks.size(), user.getId(), user.getEmail());
    String html = buildDueTaskHtml(user, tasks, OVERDUE_COLOR, "Tööülesanded mille tähtaeg on ületatud");
    send(user.getEmail(), html, "Meeldetuletus: Tööülesande tähtaeg on ületatud!");
  }

  public void notifyTasksDueToday(User user, List<Task> tasks) throws MessagingException {
    log.info("Task overdue notifications ({}) to user: {} with email {}", tasks.size(), user.getId(), user.getEmail());
    String html = buildDueTaskHtml(user, tasks, "", "Tööülesanded mille tähtaeg on täna");
    send(user.getEmail(), html, "Meeldetuletus: Tööülesande tähtaeg on täna!");
  }

  public void notifyTaskAssigned(User user, TaskEntityImpl taskEntity, Map<String, Object> variables) throws MessagingException {
    Object caseName = variables.get("Document_title");
    Date dueDate = taskEntity.getDueDate();

    String taskName = taskEntity.getName();
    String taskDueDate = dueDate != null ? DateUtil.format(SIMPLE_FORMAT, dueDate) : "N/A";
    String taskCaseName = caseName != null ? caseName.toString() : "N/A";

    String text = taskAssignedTemplate
        .replaceAll("\\[taskName]", taskName)
        .replaceAll("\\[dueDate]", taskDueDate)
        .replaceAll("\\[documentName]", taskCaseName)
        .replaceAll("\\[taskAssignee]", user.getFirstName() + " " + user.getLastName())
        .replaceAll("\\[link]", this.webBaseUrl + "cases/" + taskEntity.getScopeId());
    String subject = String.format("%s %s %s", taskName, taskDueDate, taskCaseName);

    send(user.getEmail(), text, subject);
  }

  private String buildDueTaskHtml(User user, List<Task> tasks, String color, String header) {
    StringBuilder taskListBuilder = new StringBuilder();
    for (Task task : tasks) {
      Object documentName = task.getKeis().getVariables().get("Document_title");
      String row = taskDueRow
          .replaceAll("\\[taskName]", task.getName())
          .replaceAll("\\[dueDate]", DateUtil.format(SIMPLE_FORMAT, task.getDueDate()))
          .replaceAll("\\[documentName]", documentName != null ? documentName.toString() : "N/A")
          .replaceAll("\\[taskAssignee]", user.getFirstName() + " " + user.getLastName())
          .replaceAll("\\[sender]", SENDER_PERSONAL)
          .replaceAll("\\[taskDescription]", task.getDescription() != null ? task.getDescription() : "N/A")
          .replaceAll("\\[link]", this.webBaseUrl + "cases/" + task.getKeis().getId());
      taskListBuilder.append("\n").append(row);
    }
    return this.tasksDueTemplate
        .replaceAll("\\[tasks]", taskListBuilder.toString())
        .replaceAll("\\[dueDateColor]", color)
        .replaceAll("\\[header]", header);
  }

  private void send(String email, String html, String subject) throws MessagingException {
    if (email == null || email.isEmpty() || !email.contains("@")) {
      log.error("Failed to send email, incorrect address: {}", email);
      return;
    }

    MimeMessage message = sender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message, true);

    // order important
    helper.setText(html, true);
    helper.addInline("logo", logo);
    //

    helper.setTo(email);
    try {
      helper.setFrom(defaultSenderEmail, SENDER_PERSONAL);
    } catch (UnsupportedEncodingException ignored) {
      helper.setFrom(defaultSenderEmail);
    }
    helper.setSubject(subject);

    sender.send(message);
  }
}

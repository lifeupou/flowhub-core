/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.casedefinition;

import ee.lifeup.flowhub.flowable.ModelType;
import ee.lifeup.flowhub.util.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
@AllArgsConstructor
public class CaseModelRepository {

  private final JdbcTemplate jdbcTemplate;

  public String getCaseModelId(String key) {
    try {
      return jdbcTemplate.queryForObject("SELECT id FROM act_de_model WHERE model_key = ? AND model_type = ?", String.class, key, ModelType.CASE_MODEL);
    } catch (EmptyResultDataAccessException ignored) {
      return null;
    }
  }

  public void saveBinding(String caseDefinitionKey, String appKey, Map<String, String> names) {
    jdbcTemplate.update("INSERT INTO flowhub_case_defs (key, appkey, names) VALUES (?, ?, ?::jsonb)",
        caseDefinitionKey, appKey, JsonUtil.toJson(names));
  }

  public Map<String, String> getNames(String key) {
    try {
      String json = jdbcTemplate.queryForObject("SELECT names FROM flowhub_case_defs WHERE key = ?", String.class, key);
      return JsonUtil.fromJson(json, Map.class);
    } catch (EmptyResultDataAccessException ignored) {
      HashMap<String, String> names = new HashMap<>();
      names.put("en", key);
      return names;
    }
  }
}
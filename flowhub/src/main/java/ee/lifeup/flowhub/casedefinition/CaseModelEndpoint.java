/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.casedefinition;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Objects;

@Api
@RestController
@RequestMapping("/private/case-models")
@AllArgsConstructor
public class CaseModelEndpoint {

  private final CaseDefinitionService caseDefinitionService;

  @PutMapping("/{key}")
  public ResponseEntity<?> createOrUpdateCaseModel(@PathVariable String key, @RequestBody CaseDefinition definition) {
    if (!Objects.equals(key, definition.getKey())) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.TEXT_PLAIN).body("Path and body key attribute differs.");
    }
    caseDefinitionService.createOrUpdateCaseModel(definition);
    return ResponseEntity.ok().build();
  }

  @PostMapping("/import")
  public void importCaseFromFile(@RequestParam("file") MultipartFile file, @RequestParam String appKey) throws IOException {
    caseDefinitionService.importCaseModel(file.getOriginalFilename(), file.getBytes(), appKey);
  }


}

/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.casedefinition;

import com.fasterxml.jackson.databind.JsonNode;
import ee.lifeup.flowhub.form.FormInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class CaseDefinition {

  private Map<String, String> names;
  private String key;
  private String description;
  private String appKey;
  private JsonNode data;
  private int version;
  private String category;

  private String resourceName;
  private String deploymentId;
  private FormInfo form;

  public CaseDefinition() {
    //
  }

  public CaseDefinition(org.flowable.cmmn.api.repository.CaseDefinition flCase) {
    setCategory(flCase.getCategory());

    Map<String, String> names = new HashMap<>();
    names.put("en", flCase.getName());
    setNames(names);

    setKey(flCase.getKey());
    setDescription(flCase.getDescription());
    setVersion(flCase.getVersion());
    setResourceName(flCase.getResourceName());
    setDeploymentId(flCase.getDeploymentId());
  }

}

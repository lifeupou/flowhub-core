/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.casedefinition;

import ee.lifeup.flowhub.app.AppDefinition;
import ee.lifeup.flowhub.app.AppRepository;
import ee.lifeup.flowhub.flowable.FlowableModelerClient;
import ee.lifeup.flowhub.form.FormInfo;
import lombok.AllArgsConstructor;
import org.flowable.cmmn.api.CmmnRepositoryService;
import org.flowable.cmmn.model.Case;
import org.flowable.form.api.FormRepositoryService;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CaseDefinitionService {
  private final CmmnRepositoryService cmmnRepositoryService;
  private final FormRepositoryService formRepositoryService;
  private final AppRepository appRepository;
  private final CaseModelRepository caseModelRepository;
  private final FlowableModelerClient modelerClient;

  public List<CaseDefinition> getCaseDefinitions() {
    return cmmnRepositoryService.createCaseDefinitionQuery().latestVersion().list().stream().map(c -> {
      CaseDefinition def = new CaseDefinition(c);
      if (c.hasStartFormKey()) {
        Case caseModel = cmmnRepositoryService.getCmmnModel(c.getId()).getPrimaryCase();
        String formKey = caseModel.getPlanModel().getFormKey();
        def.setForm(new FormInfo(formRepositoryService.getFormModelByKey(formKey)));
      }
      def.setNames(caseModelRepository.getNames(def.getKey()));
      return def;
    }).collect(Collectors.toList());
  }

  public void deleteCaseDefinition(String key) {
    String deploymentId = cmmnRepositoryService.createCaseDefinitionQuery().caseDefinitionKey(key).singleResult().getDeploymentId();
    cmmnRepositoryService.deleteDeployment(deploymentId, true);
  }


  public void createOrUpdateCaseModel(CaseDefinition caseDefinition) {
    String modelId = caseModelRepository.getCaseModelId(caseDefinition.getKey());

    CmmnModel cmmnModel;
    if (modelId == null) {
      cmmnModel = modelerClient.createCmmnModel(caseDefinition);
    } else {
      cmmnModel = modelerClient.updateCmmnModel(caseDefinition, modelId);
    }

    bindCmmnModelToApp(cmmnModel, caseDefinition.getAppKey());

    caseModelRepository.saveBinding(caseDefinition.getKey(), caseDefinition.getAppKey(), caseDefinition.getNames());
  }

  private void bindCmmnModelToApp(CmmnModel cmmnModel, String appKey) {
    Definition caseDef = new Definition(Collections.singletonList(cmmnModel));

    AppDefinition appDefinition = appRepository.getAppDefinition(appKey);
    appDefinition.setDefinition(caseDef);

    modelerClient.addDefinitionToApp(appDefinition);
  }


  public CaseDefinition getCaseDefinition(String key) {
    org.flowable.cmmn.api.repository.CaseDefinition flCase = cmmnRepositoryService.createCaseDefinitionQuery().caseDefinitionKey(key).latestVersion().singleResult();
    if (flCase == null) {
      return null;
    }
    CaseDefinition def = new CaseDefinition(flCase);
    def.setNames(getNames(def.getKey()));
    return def;
  }

  public Map<String, String> getNames(String key) {
    return caseModelRepository.getNames(key);
  }

  public void importCaseModel(String originalFilename, byte[] data, String appKey) {
    CmmnModel cmmnModel = modelerClient.importCaseModel(originalFilename, data);
    bindCmmnModelToApp(cmmnModel, appKey);
  }
}

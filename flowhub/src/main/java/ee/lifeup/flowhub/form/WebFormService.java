/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.form;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import ee.lifeup.flowhub.flowable.FlowableModelerClient;
import ee.lifeup.flowhub.flowable.ModelType;
import ee.lifeup.flowhub.form.model.ModelRepresentation.Field;
import ee.lifeup.flowhub.form.model.ModelRepresentation.FormDefinition;
import ee.lifeup.flowhub.form.model.ModelRepresentation.FormRepresentation;
import ee.lifeup.flowhub.form.model.ModelRepresentation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@AllArgsConstructor
@Slf4j
public class WebFormService {

  // map from form.io type to flowable type
  private static final Map<String, String> typeMapping = new HashMap<>();

  static {
    typeMapping.put("textfield", "text");
    typeMapping.put("select", "dropdown");
    typeMapping.put("resource", "people");
    typeMapping.put("datetime", "date");
    typeMapping.put("datagrid", "text");
    typeMapping.put("editgrid", "text");
    typeMapping.put("number", "decimal");
    typeMapping.put("password", "password");
    typeMapping.put("checkbox", "boolean");
    typeMapping.put("radio", "radio-buttons");
  }

  private final FormRepository formRepository;
  private final FlowableModelerClient modelerClient;

  public Form getForm(String key) {
    List<Form> forms = formRepository.get(key);
    return forms.isEmpty() ? null : forms.get(0);
  }

  @Transactional
  public void create(Form form) throws FormAlreadyExistsException {
    if (getForm(form.getKey()) != null) {
      throw new FormAlreadyExistsException("Key is not unique. Form with that key already exists.");
    }

    String formName = form.getNames().get("en");
    String modelId = modelerClient.createModel(formName, form.getKey(), ModelType.FORM_MODEL);
    log.info("created model with id {}", modelId);

    List<Field> fields = new ArrayList<>();

    JsonNode def = form.getDefinition();
    populateFields(fields, def);

    saveToModeler(form.getKey(), modelId, formName, fields);

    formRepository.save(form);
  }

  private void populateFields(List<Field> fields, JsonNode def) {
    Iterator<JsonNode> componentIterator = def.get("components").elements();

    while (componentIterator.hasNext()) {
      JsonNode component = componentIterator.next();
      if(component.get("type").asText().equals("panel")){
        populateFields(fields, component);
      } else {
        String type = component.get("type").asText();
        String compKey = component.get("key").asText();
        String label = component.get("label").asText();

        String fieldType = typeMapping.getOrDefault(type, "text");
        Field field = new Field(compKey, label, fieldType);

        Map<String, Object> map = new HashMap<>();
        if (type.equals("resource")) {
          map.put("formIoComponent", component);
        }
        field.setParams(map);

        fields.add(field);
      }
    }
  }

  private void saveToModeler(String key, String modelId, String fromName, List<Field> fields) {
    FormDefinition formDef = new FormDefinition(fromName, key, fields);
    ModelRepresentation modelRepresentation = new ModelRepresentation(new FormRepresentation(modelId, fromName, key, formDef));

    modelerClient.saveModelRepresentation(modelId, modelRepresentation);
  }

  public void update(String key, Form form) {
    try {
      formRepository.update(key, form);
    } catch (JsonProcessingException e) {
      log.error("Failed to update form", e);
    }
  }

  public void delete(String key) {
    formRepository.delete(key);
  }

  public List<Form> getForms() {
    return formRepository.getAll();
  }
}

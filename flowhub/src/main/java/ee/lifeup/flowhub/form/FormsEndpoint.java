/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.form;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api
@RestController
@RequestMapping("/private/forms")
@AllArgsConstructor
@Slf4j
public class FormsEndpoint {

  private final WebFormService webFormService;

  @GetMapping("{key}")
  public Form get(@PathVariable String key) {
    return webFormService.getForm(key);
  }

  @GetMapping()
  public List<Form> getAll() {
    return webFormService.getForms();
  }

  @PostMapping()
  public ResponseEntity<?> create(@RequestBody Form form) {
    try {
      webFormService.create(form);
      return ResponseEntity.ok().build();
    } catch (FormAlreadyExistsException e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.TEXT_PLAIN).body(e.getMessage());
    }
  }

  @PutMapping("{key}")
  public ResponseEntity<?> update(@PathVariable String key, @RequestBody Form form) {
    if(form.getKey() != null && !form.getKey().equals(key)){
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.TEXT_PLAIN).body("Path and body key attribute differs.");
    }
    webFormService.update(key, form);
    log.info("updated form: {}", form);
    return ResponseEntity.ok().build();
  }

  @DeleteMapping("{key}")
  public ResponseEntity<?> delete(@PathVariable String key) {
    webFormService.delete(key);
    return ResponseEntity.noContent().build();
  }
}

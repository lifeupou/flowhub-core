/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.form;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import ee.lifeup.flowhub.util.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
@Slf4j
public class FormRepository {

  private final JdbcTemplate jdbcTemplate;

  public List<Form> get(String key) {
    return jdbcTemplate.query("SELECT * FROM flowhub_form WHERE key = ?", new FormRowMapper(true), key);
  }

  public void save(Form form) {
    JsonNode labels = form.getLabels();
    jdbcTemplate.update("INSERT INTO flowhub_form (key, definition, labels, names, engine) VALUES (?, ?::jsonb, ?::jsonb, ?::jsonb, ?::jsonb)",
        form.getKey(),
        form.getDefinition().toString(),
        labels != null ? labels.toString() : null,
        JsonUtil.toJson(form.getNames()),
        JsonUtil.toJson(form.getEngine())
    );
  }

  public void update(String key, Form form) throws JsonProcessingException {
    JsonNode labels = form.getLabels();
    jdbcTemplate.update("UPDATE flowhub_form  SET definition = ?::jsonb, labels = ?::jsonb, names = ?::jsonb, engine = ?::jsonb WHERE key = ?",
        form.getDefinition().toString(),
        labels != null ? labels.toString() : null,
        JsonUtil.toJson(form.getNames()),
        JsonUtil.toJson(form.getEngine()),
        key
    );
  }

  public List<Form> getAll() {
    return jdbcTemplate.query("SELECT * FROM flowhub_form", new FormRowMapper(false));
  }

  public void delete(String key) {
  }

  private static class FormRowMapper implements RowMapper<Form> {

    private final boolean full;

    FormRowMapper(boolean full) {
      this.full = full;
    }

    @Override
    public Form mapRow(ResultSet rs, int rowNum) throws SQLException {

      Form form = new Form();
      form.setKey(rs.getString("key"));
      if (full) {
        form.setDefinition(JsonUtil.toJsonNode(rs.getString("definition")));

        String labels = rs.getString("labels");
        if (labels != null) {
          form.setLabels(JsonUtil.toJsonNode(labels));
        }
      }

      String names = rs.getString("names");
      if (names != null) {
        form.setNames(JsonUtil.fromJson(names, Map.class));
      }

      String engine = rs.getString("engine");
      if (engine != null) {
        form.setEngine(JsonUtil.fromJson(engine, Map.class));

      }
      return form;
    }
  }
}

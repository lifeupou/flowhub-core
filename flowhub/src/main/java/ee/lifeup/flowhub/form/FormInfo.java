/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.form;

import lombok.Getter;
import lombok.Setter;
import org.flowable.form.model.FormField;
import org.flowable.form.model.FormOutcome;
import org.flowable.form.model.SimpleFormModel;

import java.util.List;

@Getter
@Setter
public class FormInfo {
  protected String id;
  protected String name;
  protected String description;
  protected String key;
  protected int version;
  protected List<FormField> fields;
  protected List<FormOutcome> outcomes;
  protected String outcomeVariableName;

  public FormInfo() {
    //
  }

  public FormInfo(org.flowable.form.api.FormInfo formInfo) {
    this.id = formInfo.getId();
    this.name = formInfo.getName();
    this.description = formInfo.getDescription();
    this.key = formInfo.getKey();
    this.version = formInfo.getVersion();

    SimpleFormModel model = (SimpleFormModel) formInfo.getFormModel();
    this.fields = model.getFields();
    this.outcomes = model.getOutcomes();
    this.outcomeVariableName = model.getOutcomeVariableName();
  }

}

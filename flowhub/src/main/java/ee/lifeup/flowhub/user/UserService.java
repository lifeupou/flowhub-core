package ee.lifeup.flowhub.user;

import ee.lifeup.flowhub.proxy.alfresco.model.AlfrescoUser;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.IdentityService;
import org.flowable.idm.api.User;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
@AllArgsConstructor
public class UserService {

  public static final String PERSONAL_CODE_USER_INFO_FIELD = "personalCode";
  public static final String PHONE_NUMBER_USER_INFO_FIELD = "phoneNumber";
  private final IdentityService identityService;

  @Transactional
  public void addOrUpdate(AlfrescoUser alfrescoUser) {
    String userId = alfrescoUser.getUserName();

    User flowableUser = identityService.createUserQuery().userId(userId).singleResult();
    if (flowableUser == null) {
      flowableUser = identityService.newUser(userId);
    }

    flowableUser.setEmail(alfrescoUser.getEmail());
    flowableUser.setFirstName(alfrescoUser.getFirstName());
    flowableUser.setLastName(alfrescoUser.getLastName());

    identityService.saveUser(flowableUser);

    identityService.setUserInfo(userId, PERSONAL_CODE_USER_INFO_FIELD, alfrescoUser.getPersonalCode());
    identityService.setUserInfo(userId, PHONE_NUMBER_USER_INFO_FIELD, alfrescoUser.getPhoneNumber());

    log.info("Imported/Updated user from alfresco: {}", alfrescoUser);
  }

  public User getUser(String username) {
    return identityService.createUserQuery().userId(username).singleResult();
  }

  public UserInfo getUserInfo(String username) {
    String personalCode = identityService.getUserInfo(username, PERSONAL_CODE_USER_INFO_FIELD);
    String phoneNumber = identityService.getUserInfo(username, PHONE_NUMBER_USER_INFO_FIELD);
    User user = getUser(username);
    return new UserInfo(user, personalCode, phoneNumber);
  }
}

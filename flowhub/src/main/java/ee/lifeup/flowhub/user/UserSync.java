package ee.lifeup.flowhub.user;

import ee.lifeup.flowhub.proxy.alfresco.model.AlfrescoUser;
import ee.lifeup.flowhub.proxy.alfresco.service.user.AlfrescoUserService;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class UserSync {

  private final UserService userService;
  private final AlfrescoUserService alfrescoUserService;

  @Scheduled(cron = "0 5 0 * * *")
  public void sync() {
    alfrescoUserService.syncUsers(userService::addOrUpdate);
  }

  @FunctionalInterface
  public interface UserSynchronizer {
    void sync(AlfrescoUser alfrescoUser);
  }
}

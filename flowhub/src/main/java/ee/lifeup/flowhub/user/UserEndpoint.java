package ee.lifeup.flowhub.user;

import ee.lifeup.flowhub.security.AuthContext;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api
@RestController
@AllArgsConstructor
@RequestMapping("/internal/users")
public class UserEndpoint {

  private final UserSync userSync;
  private final UserService userService;

  @PostMapping("/sync")
  public ResponseEntity sync(){
    userSync.sync();
    return ResponseEntity.ok().build();
  }

  @GetMapping("/me/info")
  public UserInfo getUserInfo() throws IllegalAccessException {
    String username = AuthContext.getUser().orElseThrow(() -> new IllegalAccessException("No user found in auth context"));
    return userService.getUserInfo(username);
  }
}

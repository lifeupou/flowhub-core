package ee.lifeup.flowhub.user;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.flowable.idm.api.User;

@Getter
@Setter
@ToString
public class UserInfo {
  public UserInfo() {
  }

  public UserInfo(User user, String personalCode, String phoneNumber) {
    this.userName = user.getId();
    this.email = user.getEmail();
    this.firstName = user.getFirstName();
    this.lastName = user.getLastName();
    this.personalCode = personalCode;
    this.phoneNumber = phoneNumber;
  }

  private String userName;
  private String firstName;
  private String lastName;
  private String email;
  private String personalCode;
  private String phoneNumber;
}

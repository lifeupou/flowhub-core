/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.digitalsign.service.impl;

import ee.lifeup.flowhub.annotation.ProxyQualifier;
import ee.lifeup.flowhub.digitalsign.mapper.DigitalSignCardMapper;
import ee.lifeup.flowhub.digitalsign.model.BaseSignRequest;
import ee.lifeup.flowhub.digitalsign.model.ContainerMetadata;
import ee.lifeup.flowhub.digitalsign.model.SignContainerAware;
import ee.lifeup.flowhub.digitalsign.model.card.CreateSignHashCardRequest;
import ee.lifeup.flowhub.proxy.alfresco.service.AlfrescoContainerProxyService;
import ee.lifeup.flowhub.proxy.alfresco.service.AlfrescoFileProxyService;
import ee.lifeup.flowhub.proxy.digidoc.service.DigidocProxyService;
import ee.lifeup.flowhub.util.FlowhubFileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ee.lifeup.flowhub.annotation.ProxyQualifier.Proxy.ALFRESCO_CONTAINER_PROXY;
import static ee.lifeup.flowhub.annotation.ProxyQualifier.Proxy.ALFRESCO_FILE_PROXY;

/**
 * Base class for digital signature.
 */
public abstract class DigitalSignAbstractBaseService {

    @Autowired
    @ProxyQualifier(ALFRESCO_FILE_PROXY)
    protected AlfrescoFileProxyService alfrescoFileProxyService;
    @Autowired
    @ProxyQualifier(ALFRESCO_CONTAINER_PROXY)
    protected AlfrescoContainerProxyService alfrescoContainerProxyService;
    @Autowired
    protected DigidocProxyService digidocProxyService;
    @Autowired
    protected DigitalSignCardMapper digitalSignCardMapper;

    /**
     * Creates zip archive with files to be signed.
     *
     * @param baseSignRequest Request for sign document.
     * @return Zip archive as bytes.
     */
    protected byte[] getZipSignFile(BaseSignRequest baseSignRequest) throws IOException, URISyntaxException {
        List<CreateSignHashCardRequest.FileData> files = baseSignRequest.getFiles();
        Map<String, InputStream> filesToSign = new HashMap<>();
        for (CreateSignHashCardRequest.FileData fileData : files) {
            InputStream fileContent = alfrescoFileProxyService.getFileContent(fileData.getFileNodeRef());
            filesToSign.put(fileData.getFileName(), fileContent);
        }
        return FlowhubFileUtils.createZipFile(filesToSign);
    }

    /**
     * Store container to the Alfresco.
     *
     * @param container               BDOC container.
     * @param containerMetadata       container metadata.
     * @param signRequest Request for sign document.
     * @return signed file reference.
     */
    protected String storeContainer(SignContainerAware signRequest, Resource container, ContainerMetadata containerMetadata ) throws IOException, URISyntaxException {
        return alfrescoContainerProxyService.storeContainer(signRequest, container, containerMetadata);
    }

}

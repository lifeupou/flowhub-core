/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.digitalsign.endpoint;

import ee.lifeup.flowhub.digitalsign.model.mobile.SignDocumentMobileRequest;
import ee.lifeup.flowhub.digitalsign.model.mobile.SignMobileStatus;
import ee.lifeup.flowhub.digitalsign.model.mobile.SignStatusMobileRequest;
import ee.lifeup.flowhub.digitalsign.service.DigitalSignMobileService;
import ee.lifeup.flowhub.document.DocumentRegistry;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URISyntaxException;

@Api
@RestController
@RequestMapping("/digidoc/mobile")
@AllArgsConstructor
public class DigitalSignMobileEndpoint {

    private final DigitalSignMobileService digitalSignMobileService;
    private final DocumentRegistry documentRegistry;

    /**
     * Request for sign document.
     *
     * @param signDocumentMobileRequest Request for sign document by Mobile ID.
     */
    @PostMapping(value = "/document/sign")
    public ResponseEntity<?> signDocument(@RequestBody SignDocumentMobileRequest signDocumentMobileRequest, HttpMethod method, HttpServletRequest servletRequest) throws IOException, URISyntaxException {
        documentRegistry.registerDocument(signDocumentMobileRequest.getCaseId(), null);
        return digitalSignMobileService.signDocument(signDocumentMobileRequest, method, servletRequest);
    }

    /**
     * Request for getting current status of the signature procedure by Mobile ID.
     *
     * @param signStatusMobileRequest Request for checking signature status by Mobile ID.
     * @return Object that contains the status of the signature procedure.
     */
    @PostMapping(value = "/document/sign/status")
    public ResponseEntity<SignMobileStatus> getSigningStatus(@RequestBody SignStatusMobileRequest signStatusMobileRequest, HttpServletRequest servletRequest) throws URISyntaxException, IOException {
        SignMobileStatus signingStatus = digitalSignMobileService.getSigningStatus(signStatusMobileRequest, servletRequest);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(signingStatus);
    }

}

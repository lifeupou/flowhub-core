/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.digitalsign.service;

import ee.lifeup.flowhub.digitalsign.model.mobile.SignDocumentMobileRequest;
import ee.lifeup.flowhub.digitalsign.model.mobile.SignMobileStatus;
import ee.lifeup.flowhub.digitalsign.model.mobile.SignStatusMobileRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Service for work with digital signature by Mobile ID.
 */
public interface DigitalSignMobileService {

    /**
     * Launches signature procedure.
     *
     * @param signDocumentMobileRequest Request for sign document by Mobile ID.
     * @param method                    HTTP request method.
     * @param servletRequest            HTTP Servlet request.
     */
    ResponseEntity<?> signDocument(SignDocumentMobileRequest signDocumentMobileRequest, HttpMethod method, HttpServletRequest servletRequest) throws IOException, URISyntaxException;

    /**
     * Returns of the current status of the signature procedure.
     *
     * @param signStatusMobileRequest Request for checking signature status by Mobile ID.
     * @param servletRequest          HTTP Servlet request.
     * @return Object that contains the status of the signature procedure.
     */
    SignMobileStatus getSigningStatus(SignStatusMobileRequest signStatusMobileRequest, HttpServletRequest servletRequest) throws URISyntaxException, IOException;
}

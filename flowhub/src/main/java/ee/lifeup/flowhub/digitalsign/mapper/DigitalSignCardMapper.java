/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.digitalsign.mapper;

import ee.lifeup.flowhub.digitalsign.model.BaseSignRequest;
import ee.lifeup.flowhub.digitalsign.model.card.CreateSignHashCardRequest;
import ee.lifeup.flowhub.digitalsign.model.mobile.SignDocumentMobileRequest;
import ee.lifeup.flowhub.proxy.digidoc.model.FileResource;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * Mapper for digital signature.
 */
@Component
public class DigitalSignCardMapper {

    public MultiValueMap<String, Object> mapFromSignHashCardRequest(CreateSignHashCardRequest flowhubRequest, byte[] zipFile) {
        MultiValueMap<String, Object> requestData = mapFromBaseSignRequest(flowhubRequest, zipFile);
        requestData.add("cert", flowhubRequest.getCert());
        return requestData;
    }

    public MultiValueMap<String, Object> mapFormSignDocumentMobileRequest(SignDocumentMobileRequest flowhubRequest, byte[] zipFile) {
        MultiValueMap<String, Object> requestData = mapFromBaseSignRequest(flowhubRequest, zipFile);
        requestData.add("phoneNumber", flowhubRequest.getPhoneNumber());
        requestData.add("idCode", flowhubRequest.getIdCode());
        return requestData;
    }

    private MultiValueMap<String, Object> mapFromBaseSignRequest(BaseSignRequest baseSignRequest, byte[] zipFile) {
        MultiValueMap<String, Object> requestData = new LinkedMultiValueMap<>();
        requestData.add("id", baseSignRequest.getId());
        requestData.add("file", new FileResource(baseSignRequest.getId() + ".zip", zipFile));
        return requestData;
    }
}


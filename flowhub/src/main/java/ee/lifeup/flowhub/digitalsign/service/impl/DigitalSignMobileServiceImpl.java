/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.digitalsign.service.impl;

import com.google.common.base.Preconditions;
import ee.lifeup.flowhub.digitalsign.model.ContainerMetadata;
import ee.lifeup.flowhub.digitalsign.model.mobile.SignDocumentMobileRequest;
import ee.lifeup.flowhub.digitalsign.model.mobile.SignMobileStatus;
import ee.lifeup.flowhub.digitalsign.model.mobile.SignStatusMobileRequest;
import ee.lifeup.flowhub.digitalsign.model.mobile.SigningStatus;
import ee.lifeup.flowhub.digitalsign.service.DigitalSignMobileService;
import ee.lifeup.flowhub.valueset.ApiException;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Objects;

/**
 * {@link DigitalSignMobileService} implementation.
 */
@Service
public class DigitalSignMobileServiceImpl extends DigitalSignAbstractBaseService implements DigitalSignMobileService {

    private static final String GET_CONTAINER_URI = "/digidoc/mobile/container";
    private static final String EXTRACT_CONTAINER_METADATA_MOBILE_URI = "/digidoc/mobile/container/metadata/extract";

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<?> signDocument(SignDocumentMobileRequest signDocumentMobileRequest, HttpMethod method, HttpServletRequest servletRequest) throws IOException, URISyntaxException {
        Preconditions.checkNotNull(signDocumentMobileRequest, "Request to sign a document must not be null");

        MultiValueMap<String, Object> digidocRequest = digitalSignCardMapper.mapFormSignDocumentMobileRequest(signDocumentMobileRequest, getZipSignFile(signDocumentMobileRequest));
        return digidocProxyService.proxy(digidocRequest, method, servletRequest, MediaType.MULTIPART_FORM_DATA);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SignMobileStatus getSigningStatus(SignStatusMobileRequest signStatusMobileRequest, HttpServletRequest servletRequest) throws URISyntaxException, IOException {
        Preconditions.checkNotNull(signStatusMobileRequest, "Request for checking signature status must not be null");

        String requestId = signStatusMobileRequest.getId();
        ResponseEntity<?> statusResponse = digidocProxyService.proxy(null, HttpMethod.GET, servletRequest, createRequestIdQuery(requestId), null, SignMobileStatus.class);
        SignMobileStatus signMobileStatus = (SignMobileStatus) statusResponse.getBody();
        if (signMobileStatus == null) {
            throw new ApiException(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR, "Mobile status response is empty");
        }

        if (Objects.equals(signMobileStatus.getStatus(), SigningStatus.DONE)) {
            Resource container = getContainer(requestId);
            ContainerMetadata containerMetadata = extractContainerMetadata(requestId);
            String signedFileRef = storeContainer(signStatusMobileRequest, container, containerMetadata);
            signMobileStatus.setSignedFileRef(signedFileRef);
        }
        return signMobileStatus;
    }

    private Resource getContainer(String requestId) throws URISyntaxException, IOException {
        ResponseEntity<?> containerResponse = digidocProxyService.proxy(null, HttpMethod.GET, GET_CONTAINER_URI, createRequestIdQuery(requestId),null, Resource.class);
        Resource container = (Resource) containerResponse.getBody();
        if (container == null || container.contentLength() == 0) {
            throw new ApiException(org.apache.http.HttpStatus.SC_NO_CONTENT, "BDOC container is empty");
        }
        return container;
    }

    private ContainerMetadata extractContainerMetadata(String requestId) throws URISyntaxException {
        ResponseEntity<?> containerMetadataResponse = digidocProxyService.proxy(null, HttpMethod.GET, EXTRACT_CONTAINER_METADATA_MOBILE_URI, createRequestIdQuery(requestId), null, ContainerMetadata.class);
        return (ContainerMetadata) containerMetadataResponse.getBody();
    }

    private String createRequestIdQuery(String requestId) {
        return "id=" + requestId;
    }

}

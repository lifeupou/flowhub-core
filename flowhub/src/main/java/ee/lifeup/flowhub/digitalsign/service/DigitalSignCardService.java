/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.digitalsign.service;

import ee.lifeup.flowhub.digitalsign.model.card.CreateSignHashCardRequest;
import ee.lifeup.flowhub.digitalsign.model.card.SignDocumentCardRequest;
import org.springframework.http.HttpMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Service for work with digital signature by ID Card.
 */
public interface DigitalSignCardService {

    /**
     * Create signature hash.
     *
     * @param createSignHashCardRequest Request for digital signature creation.
     * @param method                    HTTP request method.
     * @param servletRequest            HTTP Servlet request.
     * @return signature hash in hex.
     */
    String createSignatureHash(CreateSignHashCardRequest createSignHashCardRequest, HttpMethod method, HttpServletRequest servletRequest) throws IOException, URISyntaxException;

    /**
     * Sign document.
     *
     * @param signDocumentCardRequest Request for sign document by ID Card.
     * @param method                  HTTP request method.
     * @param servletRequest          HTTP Servlet request.
     * @return signed file reference.
     */
    String signDocument(SignDocumentCardRequest signDocumentCardRequest, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException, IOException;
}

/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.digitalsign.endpoint;

import ee.lifeup.flowhub.digitalsign.model.card.CreateSignHashCardRequest;
import ee.lifeup.flowhub.digitalsign.model.card.SignDocumentCardRequest;
import ee.lifeup.flowhub.digitalsign.service.DigitalSignCardService;
import ee.lifeup.flowhub.document.DocumentRegistry;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URISyntaxException;

@Api
@RestController
@RequestMapping("/digidoc/card")
@AllArgsConstructor
public class DigitalSignCardEndpoint {

  private final DigitalSignCardService digitalSignCardService;
  private final DocumentRegistry documentRegistry;

  /**
   * Request for create signature hash.
   *
   * @param createSignHashCardRequest Request for create signature hash.
   * @return signature hash.
   */
  @PostMapping(value = "/signature/hash/create")
  public ResponseEntity<String> createSignatureHash(@RequestBody CreateSignHashCardRequest createSignHashCardRequest, HttpMethod method, HttpServletRequest servletRequest) throws IOException, URISyntaxException {
    String signHash = digitalSignCardService.createSignatureHash(createSignHashCardRequest, method, servletRequest);
    return ResponseEntity.ok().body(signHash);
  }

  /**
   * Request for sign document.
   *
   * @param signDocumentCardRequest Request for sign document by ID Card.
   * @return signed file reference.
   */
  @PostMapping(value = "/document/sign")
  public ResponseEntity<String> signDocument(@RequestBody SignDocumentCardRequest signDocumentCardRequest, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException, IOException {
    documentRegistry.registerDocument(signDocumentCardRequest.getCaseId(), null);
    String signedFileReference = digitalSignCardService.signDocument(signDocumentCardRequest, method, servletRequest);
    return ResponseEntity.ok().body(signedFileReference);
  }

}

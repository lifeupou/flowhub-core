/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.digitalsign.service.impl;


import com.google.common.base.Preconditions;
import ee.lifeup.flowhub.digitalsign.model.ContainerMetadata;
import ee.lifeup.flowhub.digitalsign.model.card.CreateSignHashCardRequest;
import ee.lifeup.flowhub.digitalsign.model.card.SignDocumentCardRequest;
import ee.lifeup.flowhub.digitalsign.service.DigitalSignCardService;
import ee.lifeup.flowhub.proxy.digidoc.model.FileResource;
import ee.lifeup.flowhub.valueset.ApiException;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * {@link DigitalSignCardService} implementation.
 */
@Service
public class DigitalSignCardServiceImpl extends DigitalSignAbstractBaseService implements DigitalSignCardService {

    private static final String EXTRACT_CONTAINER_METADATA_CARD_URI = "/digidoc/card/container/metadata/extract";

    /**
     * {@inheritDoc}
     */
    @Override
    public String createSignatureHash(CreateSignHashCardRequest createSignHashCardRequest, HttpMethod method, HttpServletRequest servletRequest) throws IOException, URISyntaxException {
        Preconditions.checkNotNull(createSignHashCardRequest, "Request for signature hash creation must not be null");

        MultiValueMap<String, Object> digidocRequest = digitalSignCardMapper.mapFromSignHashCardRequest(createSignHashCardRequest, getZipSignFile(createSignHashCardRequest));
        ResponseEntity<?> response = digidocProxyService.proxy(digidocRequest, method, servletRequest, MediaType.MULTIPART_FORM_DATA, String.class);
        return String.valueOf(response.getBody());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String signDocument(SignDocumentCardRequest signDocumentCardRequest, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException, IOException {
        Preconditions.checkNotNull(signDocumentCardRequest, "Request to sign a document must not be null");
        Resource container = getContainer(signDocumentCardRequest, method, servletRequest);
        ContainerMetadata containerMetadata = extractContainerMetadata(container, signDocumentCardRequest.getContainerName());
        return storeContainer(signDocumentCardRequest,  container, containerMetadata);
    }

    private Resource getContainer(SignDocumentCardRequest signDocumentCardRequest, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException, IOException {
        ResponseEntity<?> containerResponse = digidocProxyService.proxy(signDocumentCardRequest, method, servletRequest, MediaType.APPLICATION_JSON, Resource.class);
        Resource container = (Resource) containerResponse.getBody();
        if (container == null || container.contentLength() == 0) {
            throw new ApiException(org.apache.http.HttpStatus.SC_NO_CONTENT, "BDOC container is empty");
        }
        return container;
    }

    private ContainerMetadata extractContainerMetadata(Resource container, String containerName) throws URISyntaxException, IOException {
        MultiValueMap<String, Resource> requestData = new LinkedMultiValueMap<>(1);
        requestData.add("containerFile", new FileResource(containerName, IOUtils.toByteArray(container.getInputStream())));
        ResponseEntity<?> metadataResponse = digidocProxyService.proxy(requestData, HttpMethod.POST, EXTRACT_CONTAINER_METADATA_CARD_URI, MediaType.MULTIPART_FORM_DATA, ContainerMetadata.class);
        return (ContainerMetadata) metadataResponse.getBody();
    }
}

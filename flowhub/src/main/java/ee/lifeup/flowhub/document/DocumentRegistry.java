package ee.lifeup.flowhub.document;

import ee.lifeup.flowhub.proxy.alfresco.service.AlfrescoCaseInstanceService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class DocumentRegistry {

  public static final String DOCUMENT_PROFILE_NAME = "documentProfileName";

  private final CmmnRuntimeService cmmnRuntimeService;
  private final AlfrescoCaseInstanceService alfrescoCaseInstanceService;

  public Map<String, Object> registerDocument(String caseId, String documentProfileName) {

    Map<String, Object> variables = cmmnRuntimeService.getVariables(caseId);
    if (documentProfileName != null) {
      variables.put(DOCUMENT_PROFILE_NAME, documentProfileName);
    }
    try {
      alfrescoCaseInstanceService.registerDocument(variables);
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }
    cmmnRuntimeService.setVariables(caseId, variables);
    return variables;
  }

  public void createDocument(String id, String documentProfileName) {
    Map<String, Object> variables = cmmnRuntimeService.getVariables(id);
    try {
      alfrescoCaseInstanceService.createDocument(documentProfileName, variables);
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }
  }
}

package ee.lifeup.flowhub.task;

import ee.lifeup.flowhub.flowable.config.FlowableEngineConfiguration;
import ee.lifeup.flowhub.notification.EmailNotificationService;
import ee.lifeup.flowhub.user.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.common.engine.api.delegate.event.FlowableEvent;
import org.flowable.common.engine.api.delegate.event.FlowableEventListener;
import org.flowable.common.engine.impl.event.FlowableEntityEventImpl;
import org.flowable.idm.api.User;
import org.flowable.task.service.impl.persistence.entity.TaskEntityImpl;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Map;

import static org.flowable.common.engine.api.delegate.event.FlowableEngineEventType.*;


@Component
@AllArgsConstructor
@Slf4j
public class TaskListener implements FlowableEventListener {

  private final FlowableEngineConfiguration flowableEngineConfiguration;
  private final EmailNotificationService notificationService;
  private final CmmnRuntimeService cmmnRuntimeService;
  private final UserService userService;

  @PostConstruct
  public void init() {
    flowableEngineConfiguration.addEventListener(this);
  }

  @Override
  public void onEvent(FlowableEvent event) {
    if (!Arrays.asList(TASK_CREATED.name(), TASK_ASSIGNED.name(), ENTITY_CREATED.name()).contains(event.getType().name()) || !(event instanceof FlowableEntityEventImpl)) {
      return;
    }
    FlowableEntityEventImpl eventImpl = (FlowableEntityEventImpl) event;
    if (!(eventImpl.getEntity() instanceof TaskEntityImpl)) {
      return;
    }

    TaskEntityImpl taskEntity = (TaskEntityImpl) eventImpl.getEntity();
    try {
      Map<String, Object> variables = cmmnRuntimeService.getVariables(taskEntity.getScopeId());
      String assignee = taskEntity.getAssignee();

      User user = userService.getUser(assignee);
      if (user != null) {
        notificationService.notifyTaskAssigned(user, taskEntity, variables);
      } else {
        log.error("Failed to notify user of task assigning, cannot find user for assignee {}", assignee);
      }
    } catch (Exception e) {
      log.error("Failed to notify user of task assigning", e);
    }
  }

  @Override
  public boolean isFailOnException() {
    return false;
  }

  @Override
  public boolean isFireOnTransactionLifecycleEvent() {
    return false;
  }

  @Override
  public String getOnTransaction() {
    return null;
  }

}

/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.task;

import com.fasterxml.jackson.annotation.JsonProperty;
import ee.lifeup.flowhub.keis.Milestone;
import lombok.Getter;
import lombok.Setter;
import org.flowable.identitylink.api.IdentityLinkInfo;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Getter
@Setter
public class Task {
  private String id;
  private String definitionId;
  private int priority;
  private String category;
  private String owner;
  private String assignee;
  private List<String> candidateUsers;
  private List<String> candidateGroups;
  private Date createTime;
  private Long dueDate;
  private Date claimTime;
  private String name;
  private String description;

  private String formKey;

  private TaskProcess process;
  @JsonProperty("case")
  private TaskCase keis;
  private String caseDefinitionId;

  ///
  private String formUrl;
  private String status;
  private Map<String, String> names;
  private Map<String, Object> variables;

  public Task() {
  }

  public Task(org.flowable.task.api.TaskInfo fl) {
    setId(fl.getId());
    setPriority(fl.getPriority());
    setCategory(fl.getCategory());
    setOwner(fl.getOwner());
    setAssignee(fl.getAssignee());
    setDefinitionId(fl.getTaskDefinitionKey());

    setClaimTime(fl.getClaimTime());
    setCreateTime(fl.getCreateTime());
    Date dueDate = fl.getDueDate();
    setDueDate(dueDate != null ? dueDate.getTime() : null);

    setFormKey(fl.getFormKey());

    setVariables(new HashMap<>());
    getVariables().putAll(fl.getProcessVariables());
    getVariables().putAll(fl.getTaskLocalVariables());

    setName(fl.getName());
    setDescription(fl.getDescription());
  }

  public void setCandidates(List<? extends IdentityLinkInfo> links) {
    setCandidateUsers(links.stream().map(il -> il.getUserId()).filter(a -> a != null).collect(toList()));
    setCandidateGroups(links.stream().map(il -> il.getGroupId()).filter(a -> a != null).collect(toList()));
  }

  @Getter
  @Setter
  public static class TaskProcess {
    private String definitionId;
    private String instanceId;
    private Map<String, String> names;
  }

  @Getter
  @Setter
  public static class TaskCase {
    private String id;
    private Map<String, String> names;
    private Milestone latestMilestone;
    private Map<String, Object> variables;
  }

}

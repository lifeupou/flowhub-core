package ee.lifeup.flowhub.task;

import ee.lifeup.flowhub.notification.EmailNotificationService;
import ee.lifeup.flowhub.user.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import org.flowable.idm.api.User;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
@Slf4j
public class TaskOverdueNotifier {

  private final TaskService taskService;
  private final EmailNotificationService notificationService;
  private final UserService userService;

  @Scheduled(cron = "0 0 8 * * *")
  @PostConstruct
  public void notifyUsers() {
    log.info("Started overdue notification process");
    TaskQuery query = new TaskQuery();
    query.setStatus(TaskStatus.in_progress);
    Map<String, List<Task>> tasks = taskService.search(query)
        .stream()
        .filter(task -> task.getDueDate() != null)
        .collect(Collectors.groupingBy(Task::getAssignee));
    tasks.forEach((this::notifyOverdue));
  }

  private void notifyOverdue(String assignee, List<Task> tasks) {
    User user = userService.getUser(assignee);
    if (user == null) {
      log.error("Cannot notify tasks overdue, unable to find user data for assignee: {}", assignee);
      return;
    }
    List<Task> dueToday = new ArrayList<>();
    List<Task> overdue = new ArrayList<>();
    tasks.forEach(task -> {
      if (sameDay(task.getDueDate())) {
        dueToday.add(task);
      } else if (task.getDueDate() < System.currentTimeMillis()) {
        overdue.add(task);
      }
    });

    try {
      if (!overdue.isEmpty()) {
        notificationService.notifyTasksOverdue(user, overdue);
      }
      if (!dueToday.isEmpty()) {
        notificationService.notifyTasksDueToday(user, dueToday);
      }
    } catch (MessagingException e) {
      log.error("Failed to notify users of task overdue", e);
    }
  }

  private boolean sameDay(Long dueDate) {
    return DateUtils.isSameDay(new Date(dueDate), new Date());
  }
}

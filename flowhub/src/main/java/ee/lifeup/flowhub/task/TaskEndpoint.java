/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.task;

import ee.lifeup.flowhub.core.model.QueryResult;
import ee.lifeup.flowhub.form.FormInfo;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.flowable.common.engine.impl.identity.Authentication;
import org.flowable.form.api.FormInstanceInfo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Api
@RestController
@RequestMapping("/private/tasks")
@AllArgsConstructor
public class TaskEndpoint {
  private final TaskService taskService;

  @GetMapping()
  public QueryResult<Task> search(TaskQuery query) {
    query.setCandidateUser(Authentication.getAuthenticatedUserId());

    return searchWithQuery(query);
  }

  private QueryResult<Task> searchWithQuery(TaskQuery query) {
    if (query.getLimit() == null) {
      return new QueryResult<>(taskService.search(query));
    }
    long count = taskService.count(query);
    QueryResult<Task> result = new QueryResult<>(Math.toIntExact(count), query);
    if (count > 0) {
      List<Task> tasks = taskService.search(query);
      result.setData(tasks);
    }
    return result;
  }

  @GetMapping("assigned")
  public QueryResult<Task> searchAssigned(TaskQuery query) {
    query.setAssignee(Authentication.getAuthenticatedUserId());
    return searchWithQuery(query);
  }

  @GetMapping("{id}")
  public Task get(@PathVariable String id) {
    return taskService.getTask(id);
  }

  @GetMapping("{id}/form-instance")
  public FormInstanceInfo getHistoricVariables(@PathVariable String id){
    return taskService.getFormInstance(id);
  }

  @GetMapping("{id}/form")
  public FormInfo getForm(@PathVariable String id) {
    return taskService.getTaskForm(id);
  }

  @PostMapping("{id}/complete")
  public ResponseEntity<?> completeTask(@PathVariable String id, @RequestBody CompleteTask action) {
    taskService.complete(id, action);
    return ResponseEntity.noContent().build();
  }

  @PostMapping("{id}/claim")
  public ResponseEntity<?> claimTask(@PathVariable String id) {
    taskService.claimTask(id);
    return ResponseEntity.noContent().build();
  }

  @PostMapping("{id}/assign")
  public ResponseEntity<?> assignTask(@PathVariable String id, @RequestBody TaskAssignment assignment) {
    taskService.assignTask(id, assignment);
    return ResponseEntity.noContent().build();
  }

  @PostMapping("{id}/return")
  public ResponseEntity<?> assignTask(@PathVariable String id, @RequestBody TaskReturn taskReturn) {
    taskReturn.setDate(new Date());
    taskReturn.setActor(Authentication.getAuthenticatedUserId());
    taskService.returnTask(id, taskReturn);
    return ResponseEntity.noContent().build();
  }

  @PostMapping("{id}/postpone")
  public ResponseEntity<?> postponeTask(@PathVariable String id, @RequestBody TaskPostpone taskPostpone){
    taskService.postponeTask(id, taskPostpone);
    return ResponseEntity.noContent().build();
  }

}

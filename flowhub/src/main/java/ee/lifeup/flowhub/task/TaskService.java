/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.task;

import ee.lifeup.flowhub.ValueSetCode;
import ee.lifeup.flowhub.form.FormInfo;
import ee.lifeup.flowhub.keis.CaseService;
import ee.lifeup.flowhub.task.Task.TaskCase;
import ee.lifeup.flowhub.task.Task.TaskProcess;
import ee.lifeup.flowhub.valueset.ApiException;
import ee.lifeup.flowhub.valueset.ValueSetEntityService;
import ee.lifeup.flowhub.valueset.model.Entity;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.flowable.cmmn.api.CmmnTaskService;
import org.flowable.common.engine.impl.identity.Authentication;
import org.flowable.engine.HistoryService;
import org.flowable.form.api.FormInstance;
import org.flowable.form.api.FormInstanceInfo;
import org.flowable.form.api.FormService;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.flowable.task.api.history.HistoricTaskInstanceQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Service("taskService0")
@AllArgsConstructor
@Slf4j
public class TaskService {
  private static final String DEFAULT_FORM = "dynform";
  private final org.flowable.engine.TaskService flowableTaskService;
  private final CmmnTaskService cmmnTaskService;
  private final ValueSetEntityService valueSetEntityService;
  private final HistoryService historyService;
  private final FormService formService;
  private final CaseService caseService;


  public List<Task> getHistoricTasksForCase(String caseId) {
    HistoricTaskInstanceQuery query = historyService.createHistoricTaskInstanceQuery();
    query.caseInstanceId(caseId);
    List<HistoricTaskInstance> tasks = query.list();
    return tasks.stream().map(t -> decorate(t)).collect(toList());
  }

  public FormInstanceInfo getFormInstance(String taskId) {
    FormInstance instance = formService.createFormInstanceQuery().taskId(taskId).singleResult();
    return formService.getFormInstanceModelById(instance.getId(), null);
  }

  public List<Task> search(TaskQuery query) {
    org.flowable.task.api.TaskQuery q = buildFlowableQuery(query);
    List<org.flowable.task.api.Task> tasks =
        query.getLimit() != null && query.getOffset() != null
            ? q.listPage(query.getOffset(), query.getLimit())
            : q.list();
    return tasks.stream().map(t -> decorate(t)).collect(toList());
  }

  public long count(TaskQuery query) {
    return buildFlowableQuery(query).count();
  }

  private org.flowable.task.api.TaskQuery buildFlowableQuery(TaskQuery query) {
    org.flowable.task.api.TaskQuery q =
        query.getCaseId() != null ? cmmnTaskService.createTaskQuery() : flowableTaskService.createTaskQuery();
    q.includeTaskLocalVariables();
    q.includeProcessVariables();
    q.includeIdentityLinks();

    if (query.getId() != null) {
      q.taskId(query.getId());
    }
    if (query.getCaseId() != null) {
      q.caseInstanceId(query.getCaseId());
    }
    if (query.getCandidateUser() != null) {
      q.taskCandidateOrAssigned(query.getCandidateUser());
    }
    if (query.getAssignee() != null) {
      q.taskAssignee(query.getAssignee());
    }
    if (query.getOwner() != null) {
      q.taskOwner(query.getOwner());
    }
    if (query.getGroup() != null) {
      q.taskCandidateGroup(query.getGroup());
    }
    if (query.getProcessId() != null) {
      q.processInstanceId(query.getProcessId());
    }
    if (query.getName() != null) {
      q.taskName(query.getName());
    }
    if (query.getProcessDefKey() != null) {
      q.processDefinitionKey(query.getProcessDefKey());
    }
    if (query.getDueDateRange() != null) {
      String[] split = query.getDueDateRange().split("-");

      Date start = new Date(Long.valueOf(split[0]));
      Date end = new Date(Long.valueOf(split[1]));
      q.taskDueAfter(start);
      q.taskDueBefore(end);
    }

    if (query.getCreatedDateRange() != null) {
      String[] split = query.getCreatedDateRange().split("-");

      Date start = new Date(Long.valueOf(split[0]));
      Date end = new Date(Long.valueOf(split[1]));
      q.taskCreatedAfter(start);
      q.taskCreatedBefore(end);
    }

    if (query.getPriority() != null) {
      q.taskPriority(query.getPriority());
    }
    if (query.getStatus() != null) {
      if (query.getStatus().equals(TaskStatus.in_progress)) {
        q.taskAssigneeLike("%%");
        q.active();
      }
    }
    q.active();
    q.orderByTaskCreateTime().desc();
    return q;
  }

  public Task getTask(String taskId) {
    org.flowable.task.api.TaskQuery q = flowableTaskService.createTaskQuery();
    q.includeTaskLocalVariables();
    q.includeProcessVariables();
    return decorate(q.taskId(taskId).active().singleResult());
  }

  public FormInfo getTaskForm(String taskId) {
    Task task = getTask(taskId);
    if (task.getProcess() != null) {
      return new FormInfo(flowableTaskService.getTaskFormModel(taskId));
    }
    return new FormInfo(cmmnTaskService.getTaskFormModel(task.getId()));
  }

  @Transactional
  public void claimTask(String taskId) {
    flowableTaskService.claim(taskId, Authentication.getAuthenticatedUserId());
  }

  @Transactional
  public void assignTask(String taskId, TaskAssignment assignment) {
    if (assignment.getAssignee() != null) {
      flowableTaskService.setAssignee(taskId, assignment.getAssignee());
    }
    if (assignment.getCandidateUsers() != null) {
      setCandidateUsers(taskId, new HashSet<>(assignment.getCandidateUsers()));
    }
    if (assignment.getCandidateGroups() != null) {
      setCandidateGroups(taskId, new HashSet<>(assignment.getCandidateGroups()));
    }
  }

  @Transactional
  public void returnTask(String taskId, TaskReturn taskReturn) {
    flowableTaskService.setAssignee(taskId, null);
    flowableTaskService.setVariableLocal(taskId, "return", taskReturn);
  }

  private void setCandidateGroups(String taskId, Set<String> groups) {
    Set<String> current = flowableTaskService.getIdentityLinksForTask(taskId)
        .stream()
        .map(il -> il.getGroupId())
        .filter(g -> g != null)
        .collect(toSet());
    current.stream().filter(group -> !groups.contains(group)).forEach(group -> {
      flowableTaskService.deleteCandidateGroup(taskId, group);
    });
    groups.stream().filter(group -> !current.contains(group)).forEach(group -> {
      flowableTaskService.addCandidateGroup(taskId, group);
    });
  }

  private void setCandidateUsers(String taskId, Set<String> users) {
    Set<String> current = flowableTaskService.getIdentityLinksForTask(taskId)
        .stream()
        .map(il -> il.getUserId())
        .filter(u -> u != null)
        .collect(toSet());
    current.stream().filter(user -> !users.contains(user)).forEach(user -> {
      flowableTaskService.deleteCandidateUser(taskId, user);
    });
    users.stream().filter(user -> !current.contains(user)).forEach(user -> {
      flowableTaskService.addCandidateUser(taskId, user);
    });
  }

  @Transactional
  public void complete(String taskId, CompleteTask action) {
    Task task = getTask(taskId);

    if (task.getProcess() != null) {
      if (action.getFormId() != null) {
        flowableTaskService.completeTaskWithForm(taskId, action.getFormId(), action.getOutcome(), action.getValues());
      } else {
        flowableTaskService.complete(taskId, action.getValues());
      }
    } else {
      if (action.getFormId() != null) {
        cmmnTaskService.completeTaskWithForm(taskId, action.getFormId(), action.getOutcome(), action.getValues());
      } else {
        cmmnTaskService.complete(taskId, action.getValues());
      }
    }
  }

  private Task decorate(org.flowable.task.api.TaskInfo flTask) {
    Task task = new Task(flTask);
    task.setFormUrl(getFormUrl(task));
//    task.setStatus(task.getAssignee() == null ? TaskStatus.unassigned : TaskStatus.in_progress);

    task.setNames(Collections.singletonMap("en", flTask.getName()));

    if (flTask.getProcessInstanceId() != null) {
      TaskProcess process = new TaskProcess();
      process.setDefinitionId(flTask.getProcessDefinitionId());
      process.setInstanceId(flTask.getProcessInstanceId());
      String processKey = StringUtils.substringBefore(process.getDefinitionId(), ":");
      process.setNames(Collections.singletonMap("en", processKey));
      task.setProcess(process);
    }
    if ("cmmn".equals(flTask.getScopeType())) {

      TaskCase taskCase = caseService.getTaskCase(flTask.getScopeId());
      task.setKeis(taskCase);
    }

    //    task.setCandidates(flTask.getIdentityLinks());
    //XXX wtf?
//    task.setCandidates(flowableTaskService.getIdentityLinksForTask(task.getId())
//        .stream()
//        .filter(il -> il.getType().equals("candidate"))
//        .collect(toList()));
    return task;
  }

  private String getFormUrl(Task task) {
    String url = getFormUrl(task.getFormKey());
    if (url == null) {
      return null;
    }
    Map<String, String> template = new HashMap<>();
    template.put("task_id", task.getId());
    task.getVariables().forEach((k, v) -> {
      if (v != null) {
        template.put(k, v.toString());
      }
    });
    return StringSubstitutor.replace(url, template, "{", "}");
  }

  private String getFormUrl(String formKey) {
    if (formKey == null) {
      return null;
    }
    Entity entity = valueSetEntityService.load(ValueSetCode.bpmForm, formKey);
    if (entity != null) {
      String url = (String) entity.getAttributes().get("form-url");
      if (url != null) {
        return url;
      }
      if (entity.getParent() != null) {
        return getFormUrl(entity.getParent());
      }
    }
    if (!formKey.equals(DEFAULT_FORM)) {
      return getFormUrl(DEFAULT_FORM);
    }
    throw new ApiException(500, "could not find form url for entity " + formKey);
  }

  @Transactional
  public void postponeTask(String taskId, TaskPostpone taskPostpone) {
    flowableTaskService.setDueDate(taskId, new Date(taskPostpone.getDueDate()));
    flowableTaskService.setPriority(taskId, taskPostpone.getPriority());
  }
}

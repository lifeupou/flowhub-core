/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.flowable;

import com.fasterxml.jackson.databind.node.ObjectNode;
import ee.lifeup.flowhub.app.AppDefinition;
import ee.lifeup.flowhub.casedefinition.CaseDefinition;
import ee.lifeup.flowhub.casedefinition.CmmnModel;
import ee.lifeup.flowhub.form.model.ModelRepresentation;
import ee.lifeup.flowhub.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.HttpCookie;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static ee.lifeup.flowhub.util.LangUtil.defaultName;

@Service
@Slf4j
public class FlowableModelerClient {
  @Value("${flowable.modeler.url}")
  private String modelerUrl;
  @Value("${flowable.idm.url}")
  private String idmUrl;
  @Value("${flowable.username}")
  private String username;
  @Value("${flowable.password}")
  private String password;
  private final RestTemplate restTemplate;

  public FlowableModelerClient() {
    restTemplate = new RestTemplate();
  }

  public String createModel(String name, String key, int type) {
    String url = modelerUrl + "/app/rest/models";
    Map<String, Object> payload = new HashMap<>();
    payload.put("name", name);
    payload.put("key", key);
    payload.put("description", "Auto-generated model on " + new Date().toString());
    payload.put("modelType", type);

    ResponseEntity<String> response = post(url, new HttpEntity<>(payload));
    return JsonUtil.read(response.getBody(), "$.id");
  }

  public void saveModelRepresentation(String modelId, ModelRepresentation modelRepresentation) {
    put(modelerUrl + "/app/rest/form-models/" + modelId, new HttpEntity<>(modelRepresentation));
  }

  public String findModel(String name) {
    String url = modelerUrl + "/app/rest/models?filterText=" + name + "&modelType=3&sort=modifiedDesc";
    ResponseEntity<String> json = get(url);
    return JsonUtil.read(json.getBody(), "$.data[0].key");
  }

  public CmmnModel importCaseModel(String filename, byte[] data) {
    String url = modelerUrl + "/app/rest/import-case-model";
    ResponseEntity<String> resp = importModel(url, new FilePart(filename, data));
    log.info("{}", resp);
    return JsonUtil.fromJson(resp.getBody(), CmmnModel.class);
  }

  public void importAppModel(String filename, byte[] modelZip) {
    String url = modelerUrl + "/app/rest/app-definitions/import";
    importModel(url, new FilePart(filename, modelZip));
  }

  public void importAppModel(String modelId, String filename, byte[] modelZip) {
    String url = modelerUrl + "/app/rest/app-definitions/" + modelId + "/import";
    importModel(url, new FilePart(filename, modelZip));
  }

  private ResponseEntity<String> importModel(String url, FilePart file) {
    MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
    body.add("file", file);
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.MULTIPART_FORM_DATA);

    return post(url, new HttpEntity<>(body, headers));
  }

  private ResponseEntity<String> post(String url, HttpEntity<Object> entity) {
    return request(HttpMethod.POST, url, entity);
  }

  private ResponseEntity<String> put(String url, HttpEntity<Object> entity) {
    return request(HttpMethod.PUT, url, entity);
  }

  private ResponseEntity<String> get(String url) {
    return request(HttpMethod.GET, url, new HttpEntity<Object>(null));
  }

  private ResponseEntity<String> request(HttpMethod method, String url, HttpEntity<Object> entity) {
    HttpHeaders headers = new HttpHeaders();
    headers.putAll(entity.getHeaders());
    HttpCookie cookie = authenticate();
    headers.add("Cookie", cookie.getName() + "=" + cookie.getValue());
    HttpEntity<Object> authEntity = new HttpEntity<>(entity.getBody(), headers);
    return exchange(method, url, authEntity);
  }

  private ResponseEntity<String> exchange(HttpMethod method, String url, HttpEntity<Object> entity) {
    try {
      ResponseEntity<String> resp = restTemplate.exchange(url, method, entity, String.class);
      if (resp.getStatusCodeValue() >= 300) {
        throw new RuntimeException("modeler request failed. status code: " + resp.getStatusCodeValue() + ". body: "
            + resp == null ? "" : resp.getBody());
      }
      return resp;
    } catch (HttpClientErrorException e) {
      throw new RuntimeException("modeler request failed. status code: " + e.getStatusCode().value() + ". body: "
          + e.getResponseBodyAsString());
    }
  }

  private HttpCookie authenticate() {
    MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
    body.add("j_username", username);
    body.add("j_password", password);
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

    ResponseEntity<String> resp =
        exchange(HttpMethod.POST, idmUrl + "/app/authentication", new HttpEntity<>(body, headers));
    String header = resp.getHeaders().get("Set-Cookie").get(0);
    return HttpCookie.parse(header).get(0);
  }


  public CmmnModel createCmmnModel(CaseDefinition caseDefinition) {
    String name = defaultName(caseDefinition.getNames());
    String modelId = createModel(name, caseDefinition.getKey(), ModelType.CASE_MODEL);

    return updateCmmnModel(caseDefinition, modelId);
  }

  public CmmnModel updateCmmnModel(CaseDefinition caseDefinition, String modelId) {
    String name = defaultName(caseDefinition.getNames());

    ((ObjectNode)caseDefinition.getData()).put("modelId", modelId);

    ObjectNode props = (ObjectNode) caseDefinition.getData().get("properties");
    props.put("case_id", caseDefinition.getKey());
    props.put("name", name);

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

    MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

    body.add("modeltype", "model");
    body.add("json_xml", JsonUtil.toJson(caseDefinition.getData()));
    body.add("name", name);
    body.add("key", caseDefinition.getKey());
    body.add("description", "");
    body.add("newversion", "true");
    body.add("comment", "");
    body.add("lastUpdated", JsonUtil.getObjectMapper().getDeserializationConfig().getDateFormat().format(new Date()));

    HttpEntity<Object> entity = new HttpEntity<>(body, headers);
    String url = modelerUrl + "/app/rest/models/" + modelId + "/editor/json";
    ResponseEntity<String> post = post(url, entity);
    return JsonUtil.fromJson(post.getBody(), CmmnModel.class);
  }

  public void publishAppDefinition(AppDefinition appDefinition) {

    String body = "{\"comment\":\"\"}";

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    ResponseEntity<String> resp = post(modelerUrl + "/app/rest/app-definitions/" + appDefinition.id + "/publish", new HttpEntity<>(body, headers));

    log.info("{}", resp);
  }

  public void addDefinitionToApp(AppDefinition appDefinition) {
    Map<String, Object> map = new HashMap<>();
    map.put("appDefinition", appDefinition);
    map.put("publish", false);

    HttpEntity<Object> entity = new HttpEntity<>(map);
    ResponseEntity<String> resp = put(modelerUrl + "/app/rest/app-definitions/" + appDefinition.id, entity);
    log.info("{}", resp);
  }

  private static class FilePart extends ByteArrayResource {

    private final String filename;

    public FilePart(String filename, byte[] byteArray) {
      super(byteArray);
      this.filename = filename;
    }

    @Override
    public String getFilename() {
      return filename;
    }

  }

}

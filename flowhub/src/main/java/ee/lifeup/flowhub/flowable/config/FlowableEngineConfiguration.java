/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.flowable.config;

import ee.lifeup.flowhub.flowable.FlowableCommandListener;
import org.apache.commons.collections4.CollectionUtils;
import org.flowable.cmmn.api.*;
import org.flowable.cmmn.engine.configurator.CmmnEngineConfigurator;
import org.flowable.cmmn.spring.SpringCmmnEngineConfiguration;
import org.flowable.cmmn.spring.configurator.SpringCmmnEngineConfigurator;
import org.flowable.common.engine.api.delegate.event.FlowableEvent;
import org.flowable.common.engine.api.delegate.event.FlowableEventListener;
import org.flowable.common.engine.api.variable.VariableContainer;
import org.flowable.common.engine.impl.cfg.TransactionState;
import org.flowable.common.engine.impl.context.Context;
import org.flowable.common.engine.impl.el.JsonNodeELResolver;
import org.flowable.common.engine.impl.el.ReadOnlyMapELResolver;
import org.flowable.common.engine.impl.interceptor.*;
import org.flowable.common.engine.impl.javax.el.*;
import org.flowable.engine.*;
import org.flowable.engine.impl.agenda.DefaultFlowableEngineAgendaFactory;
import org.flowable.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.flowable.engine.impl.el.ProcessExpressionManager;
import org.flowable.engine.impl.util.EngineServiceUtil;
import org.flowable.form.api.FormEngineConfigurationApi;
import org.flowable.form.api.FormRepositoryService;
import org.flowable.form.spring.configurator.SpringFormEngineConfigurator;
import org.flowable.job.service.impl.asyncexecutor.AsyncExecutor;
import org.flowable.job.service.impl.asyncexecutor.DefaultAsyncJobExecutor;
import org.flowable.spring.ProcessEngineFactoryBean;
import org.flowable.spring.SpringProcessEngineConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class FlowableEngineConfiguration {
  @Autowired
  private DataSource dataSource;
  @Autowired
  private PlatformTransactionManager transactionManager;
  @Autowired
  protected Environment environment;

  private final List<FlowableCommandListener> commandListeners = new ArrayList<>();
  private final List<FlowableEventListener> eventListeners = new ArrayList<>();

  public void addCommandListener(FlowableCommandListener listener) {
    commandListeners.add(listener);
  }

  public void addEventListener(FlowableEventListener listener) {
    eventListeners.add(listener);
  }

  private Map<Object, Object> getConfigurationBeans() {
    Map<Object, Object> beans = new HashMap<>();
    //    beans.put("accounting", Collections.singletonMap("url", environment.getProperty("accounting.url")));
    return beans;
  }

  @Bean(name = "processEngineFactory")
  public ProcessEngineFactoryBean processEngineFactoryBean() {
    ProcessEngineFactoryBean factoryBean = new ProcessEngineFactoryBean();
    factoryBean.setProcessEngineConfiguration(processEngineConfiguration());
    return factoryBean;
  }

  public ProcessEngine processEngine() {
    try {
      ProcessEngine engine = processEngineFactoryBean().getObject();
      return engine;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Bean(name = "cmmnEngineConfiguration")
  public CmmnEngineConfigurationApi cmmnEngineConfiguration() {
    ProcessEngineConfiguration processEngineConfiguration = processEngine().getProcessEngineConfiguration();
    return EngineServiceUtil.getCmmnEngineConfiguration(processEngineConfiguration);
  }

  //  @Bean(name = "dmnEngineConfiguration")
  //  public DmnEngineConfigurationApi dmnEngineConfiguration() {
  //    ProcessEngineConfiguration processEngineConfiguration = processEngine().getProcessEngineConfiguration();
  //    return EngineServiceUtil.getDmnEngineConfiguration(processEngineConfiguration);
  //  }

  @Bean(name = "formEngineConfiguration")
  public FormEngineConfigurationApi formEngineConfiguration() {
    ProcessEngineConfiguration processEngineConfiguration = processEngine().getProcessEngineConfiguration();
    return EngineServiceUtil.getFormEngineConfiguration(processEngineConfiguration);
  }

  //  @Bean(name = "contentEngineConfiguration")
  //  public ContentEngineConfigurationApi contentEngineConfiguration() {
  //    ProcessEngineConfiguration processEngineConfiguration = processEngine().getProcessEngineConfiguration();
  //    return EngineServiceUtil.getContentEngineConfiguration(processEngineConfiguration);
  //  }

  @Bean(name = "processEngineConfiguration")
  public ProcessEngineConfigurationImpl processEngineConfiguration() {
    SpringProcessEngineConfiguration processEngineConfiguration = new SpringProcessEngineConfiguration();
    processEngineConfiguration.setDataSource(dataSource);
    //    processEngineConfiguration.setDatabaseSchema("public");
    processEngineConfiguration.setDatabaseSchemaUpdate(environment.getProperty("flowable.liquibase.enabled"));
    processEngineConfiguration.setTransactionManager(transactionManager);
    processEngineConfiguration.setAsyncExecutorActivate(true);
    processEngineConfiguration.setAsyncExecutor(asyncExecutor());
    processEngineConfiguration.setCustomPreCommandInterceptors(new ArrayList<>());
    processEngineConfiguration.getCustomPreCommandInterceptors().add(new AfterCommandFlushed(commandListeners));
    processEngineConfiguration.setEventListeners(new ArrayList<>());
    processEngineConfiguration.getEventListeners().add(new AfterEventCommited(eventListeners));
    processEngineConfiguration.setCustomPostVariableTypes(new ArrayList<>());
    processEngineConfiguration.getCustomPostVariableTypes().add(new JsonVariableSupport());

    Map<Object, Object> beans = getConfigurationBeans();
    processEngineConfiguration.setBeans(beans);
    processEngineConfiguration.setExpressionManager(new WfmExpressionManager(beans));

    processEngineConfiguration.setMailServerHost(environment.getProperty("flowable.mail.host"));
    Integer mailPort = environment.getProperty("flowable.mail.port", Integer.class);
    if (mailPort != null) {
      processEngineConfiguration.setMailServerPort(mailPort);
    }

    processEngineConfiguration.setProcessDefinitionCacheLimit(128);

    // Enable safe XML. See http://www.flowable.org/docs/userguide/index.html#advanced.safe.bpmn.xml
    processEngineConfiguration.setEnableSafeBpmnXml(true);

    //    processEngineConfiguration.setDisableIdmEngine(true);
    processEngineConfiguration.addConfigurator(new SpringFormEngineConfigurator());

    CmmnEngineConfigurator cmmnEngineConfigurator = new SpringCmmnEngineConfigurator();
    cmmnEngineConfigurator.setCmmnEngineConfiguration(new SpringCmmnEngineConfiguration());
    cmmnEngineConfigurator.getCmmnEngineConfiguration().setCustomPreCommandInterceptors(new ArrayList<>());
    cmmnEngineConfigurator.getCmmnEngineConfiguration()
        .getCustomPreCommandInterceptors()
        .add(new AfterCommandFlushed(commandListeners));
    cmmnEngineConfigurator.getCmmnEngineConfiguration().setEventListeners(new ArrayList<>());
    cmmnEngineConfigurator.getCmmnEngineConfiguration().getEventListeners().add(new AfterEventCommited(eventListeners));
    processEngineConfiguration.addConfigurator(cmmnEngineConfigurator);

    //        SpringDmnEngineConfiguration dmnEngineConfiguration = new SpringDmnEngineConfiguration();
    //        dmnEngineConfiguration.setHistoryEnabled(true);

    // disables strict mode if setetObject() on the @Bean annotated processEngineFactoryBean(), will be
    // the fully initiali
    //        if (environment.getProperty("flowable.dmn.strict-mode", Boolean.class, true) == false) {
    //            LOGGER.info("disabling DMN engine strict mode");
    //            dmnEngineConfiguration.setStrictMode(false);
    //        }

    //        SpringDmnEngineConfigurator dmnEngineConfigurator = new SpringDmnEngineConfigurator();
    //        dmnEngineConfigurator.setDmnEngineConfiguration(dmnEngineConfiguration);
    //        processEngineConfiguration.addConfigurator(dmnEngineConfigurator);
    //
    //        SpringContentEngineConfiguration contentEngineConfiguration = new SpringContentEngineConfiguration();
    //        String contentRootFolder = environment.getProperty(PROP_FS_ROOT);
    //        if (contentRootFolder != null) {
    //            contentEngineConfiguration.setContentRootFolder(contentRootFolder);
    //        }

    //        Boolean createRootFolder = environment.getProperty(PROP_FS_CREATE_ROOT, Boolean.class);
    //        if (createRootFolder != null) {
    //            contentEngineConfiguration.setCreateContentRootFolder(createRootFolder);
    //        }
    //.setProcessDefinitionCacheLimit(environment
    //  .getProperty("flowable
    //        SpringContentEngineConfigurator springContentEngineConfigurator = new SpringContentEngineConfigurator();
    //        springContentEngineConfigurator.setContentEngineConfiguration(contentEngineConfiguration);

    //        processEngineConfiguration.addConfigurator(springContentEngineConfigurator);

    return processEngineConfiguration;
  }

  @Bean
  public FlowableEngineAgendaFactory agendaFactory() {
    return new DefaultFlowableEngineAgendaFactory();
  }

  @Bean
  public AsyncExecutor asyncExecutor() {
    DefaultAsyncJobExecutor asyncExecutor = new DefaultAsyncJobExecutor();
    asyncExecutor.setDefaultAsyncJobAcquireWaitTimeInMillis(5000);
    asyncExecutor.setDefaultTimerJobAcquireWaitTimeInMillis(5000);
    return asyncExecutor;
  }

  //  @Bean(name = "clock")
  //  @DependsOn("processEngineFactory")
  //  public Clock getClock() {
  //    return processEngineConfiguration().getClock();
  //  }

  @Bean
  public RepositoryService repositoryService() {
    return processEngine().getRepositoryService();
  }

  @Bean
  public RuntimeService runtimeService() {
    return processEngine().getRuntimeService();
  }

  @Bean
  public TaskService taskService() {
    return processEngine().getTaskService();
  }

  @Bean
  public HistoryService historyService() {
    return processEngine().getHistoryService();
  }

  @Bean
  public FormService formService() {
    return processEngine().getFormService();
  }

  @Bean
  public ManagementService managementService() {
    return processEngine().getManagementService();
  }

  @Bean
  public IdentityService identityService() {
    return processEngine().getIdentityService();
  }

  @Bean
  public FormRepositoryService formEngineRepositoryService() {
    return formEngineConfiguration().getFormRepositoryService();
  }

  @Bean
  public org.flowable.form.api.FormService formEngineFormService() {
    return formEngineConfiguration().getFormService();
  }

  //  @Bean
  //  public DmnRepositoryService dmnRepositoryService() {
  //    return dmnEngineConfiguration().getDmnRepositoryService();
  //  }
  //
  //  @Bean
  //  public DmnRuleService dmnRuleService() {
  //    return dmnEngineConfiguration().getDmnRuleService();
  //  }
  //
  //  @Bean
  //  public DmnHistoryService dmnHistoryService() {
  //    return dmnEngineConfiguration().getDmnHistoryService();
  //  }

  @Bean
  public CmmnRepositoryService cmmnRepositoryService() {
    return cmmnEngineConfiguration().getCmmnRepositoryService();
  }

  @Bean
  public CmmnRuntimeService cmmnRuntimeService() {
    return cmmnEngineConfiguration().getCmmnRuntimeService();
  }

  @Bean
  public CmmnTaskService cmmnTaskService() {
    return cmmnEngineConfiguration().getCmmnTaskService();
  }

  @Bean
  public CmmnHistoryService cmmnHistoryService() {
    return cmmnEngineConfiguration().getCmmnHistoryService();
  }

  //  @Bean
  //  public ContentService contentService() {
  //    return contentEngineConfiguration().getContentService();
  //  }

  private static class WfmExpressionManager extends ProcessExpressionManager {

    public WfmExpressionManager(Map<Object, Object> beans) {
      super(beans);
    }

    @Override
    protected ELResolver createElResolver(VariableContainer variableContainer) {
      CompositeELResolver compositeElResolver = new CompositeELResolver();
      compositeElResolver.add(createVariableElResolver(variableContainer));
      compositeElResolver.add(new ReadOnlyMapELResolver(beans));
      compositeElResolver.add(new ArrayELResolver());
      compositeElResolver.add(new ListELResolver());
      compositeElResolver.add(new MapELResolver());
      compositeElResolver.add(new JsonNodeELResolver());
      compositeElResolver.add(new JsonStringElResolver());
      compositeElResolver.add(new BeanELResolver());
      return compositeElResolver;
    }

  }

  private static class AfterCommandFlushed extends AbstractCommandInterceptor {
    private final List<FlowableCommandListener> listeners;

    public AfterCommandFlushed(List<FlowableCommandListener> listeners) {
      this.listeners = listeners;
    }

    @Override
    public <T> T execute(CommandConfig config, Command<T> command) {
      T resp = next.execute(config, command);
      if (Context.getCommandContext() == null || CollectionUtils.isEmpty(listeners)) {
        return resp;
      }
      Context.getCommandContext().addCloseListener(new CommandContextCloseListener() {

        @Override
        public void closed(CommandContext commandContext) {
          for (FlowableCommandListener l : listeners) {
            ///XXX hack. some day overwrite CommandContextInterceptor to hook after context is removed.
            Context.setCommandContext(null);
            l.afterCommand(resp, command);
            Context.removeCommandContext();
          }
        }

        @Override
        public void afterSessionsFlush(CommandContext commandContext) {
        }

        @Override
        public void closing(CommandContext commandContext) {
        }

        @Override
        public void closeFailure(CommandContext commandContext) {
        }

      });

      return resp;
    }

  }

  private static class AfterEventCommited implements FlowableEventListener {
    private final List<FlowableEventListener> listeners;

    public AfterEventCommited(List<FlowableEventListener> listeners) {
      this.listeners = listeners;
    }

    @Override
    public void onEvent(FlowableEvent event) {
      listeners.forEach(l -> l.onEvent(event));
    }

    @Override
    public boolean isFailOnException() {
      return true;
    }

    @Override
    public boolean isFireOnTransactionLifecycleEvent() {
      return true;
    }

    @Override
    public String getOnTransaction() {
      return TransactionState.COMMITTED.name();
    }

  }

}
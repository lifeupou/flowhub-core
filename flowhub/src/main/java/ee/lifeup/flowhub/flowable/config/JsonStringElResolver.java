/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.flowable.config;

import ee.lifeup.flowhub.util.JsonUtil;
import org.flowable.common.engine.impl.javax.el.ELContext;
import org.flowable.common.engine.impl.javax.el.ELResolver;

import java.beans.FeatureDescriptor;
import java.util.Iterator;
import java.util.Map;

public class JsonStringElResolver extends ELResolver {

  @Override
  public Class<?> getCommonPropertyType(ELContext context, Object base) {
    return isResolvable(base) ? Object.class : null;
  }

  @Override
  public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context, Object base) {
    if (!isResolvable(base)) {
      return null;
    }
    Map<String, Object> jsonMap = JsonUtil.toMap((String) base);
    return jsonMap.keySet().stream().map(key -> {
      FeatureDescriptor feature = new FeatureDescriptor();
      feature.setDisplayName(key == null ? "null" : key.toString());
      feature.setName(feature.getDisplayName());
      feature.setShortDescription("");
      feature.setExpert(true);
      feature.setHidden(false);
      feature.setPreferred(true);
      feature.setValue(TYPE, key == null ? "null" : key.getClass());
      feature.setValue(RESOLVABLE_AT_DESIGN_TIME, true);
      return feature;
    }).iterator();
  }

  @Override
  public Class<?> getType(ELContext context, Object base, Object property) {
    if (context == null) {
      throw new NullPointerException("context is null");
    }
    Class<?> result = null;
    if (isResolvable(base)) {
      result = Object.class;
      context.setPropertyResolved(true);
    }
    return result;
  }

  @Override
  public Object getValue(ELContext context, Object base, Object property) {
    if (context == null) {
      throw new NullPointerException("context is null");
    }
    if (!isResolvable(base)) {
      return null;
    }

    Object result = JsonUtil.read((String) base, property.toString());
    if (result == null) {
      return null;
    }
    context.setPropertyResolved(true);
    return result;
  }

  @Override
  public boolean isReadOnly(ELContext context, Object base, Object property) {
    return true;
  }

  @Override
  public void setValue(ELContext context, Object base, Object property, Object value) {
    throw new UnsupportedOperationException("what is this for?");
  }

  private final boolean isResolvable(Object base) {
    if (!(base instanceof String)) {
      return false;
    }
    String str = (String) base;
    return str != null && str.startsWith("{") && str.endsWith("}");
  }

}

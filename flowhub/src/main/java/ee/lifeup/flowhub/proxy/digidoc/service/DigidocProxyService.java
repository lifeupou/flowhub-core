/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.proxy.digidoc.service;

import ee.lifeup.flowhub.proxy.AbstractBaseProxyService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class DigidocProxyService extends AbstractBaseProxyService {

    public static final String DIGIDOC_PROXY_BASE_PATH = "/digidoc/private";

    @Value("${digidoc.proxy.scheme}")
    private String proxyScheme;

    @Value("${digidoc.proxy.server}")
    private String proxyServer;

    @Value("${digidoc.proxy.port}")
    private Integer proxyPort;

    @Override
    protected String getProxyScheme() {
        return proxyScheme;
    }

    @Override
    protected String proxyServer() {
        return proxyServer;
    }

    @Override
    protected Integer proxyPort() {
        return proxyPort;
    }
    
    public String getRequestURI(HttpServletRequest servletRequest) {
        String requestURI = super.getRequestURI(servletRequest);
        requestURI = requestURI.replaceFirst("digidoc/private", "digidoc/card");
        return requestURI;
    }
}

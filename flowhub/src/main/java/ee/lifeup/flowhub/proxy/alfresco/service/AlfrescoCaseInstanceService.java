/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.proxy.alfresco.service;

import ee.lifeup.flowhub.annotation.ProxyQualifier;
import ee.lifeup.flowhub.keis.Case;
import ee.lifeup.flowhub.valueset.ApiException;
import ee.lifeup.flowhub.valueset.ValueSetEntityService;
import ee.lifeup.flowhub.valueset.ValueSetService;
import ee.lifeup.flowhub.valueset.model.Entity;
import ee.lifeup.flowhub.valueset.model.ValueSet;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.util.*;

import static ee.lifeup.flowhub.annotation.ProxyQualifier.Proxy.ALFRESCO_PROXY;
import static ee.lifeup.flowhub.document.DocumentRegistry.DOCUMENT_PROFILE_NAME;

/**
 * Created by Aleksandr Koltakov on 10.08.2018
 */
@Service
@Slf4j
public class AlfrescoCaseInstanceService {
  private static final String URI_PREFIX = "/alfresco/wcservice/flowhub/";

  private static final String CASE_DEFINITION_KEY = "CaseDefinitionDataModel";
  private static final String DATA_SET_DEFINITION_KEY = "DatasetDefinitions";

  private final AlfrescoProxyService alfrescoProxyService;
  private final ValueSetService valueSetService;
  private final ValueSetEntityService valueSetEntityService;

  @Autowired
  public AlfrescoCaseInstanceService(@ProxyQualifier(ALFRESCO_PROXY) AlfrescoProxyService alfrescoProxyService,
                                     ValueSetService valueSetService,
                                     ValueSetEntityService valueSetEntityService) {
    this.alfrescoProxyService = alfrescoProxyService;
    this.valueSetService = valueSetService;
    this.valueSetEntityService = valueSetEntityService;
  }

  public void createCaseInstance(String id, Map<String, Object> variables) throws URISyntaxException {
    String definitionKey = (String) variables.get(Case.DEFINITION_KEY);
    log.info("Going to create instance of case {} with id {} in repository", definitionKey, id);
    createCaseInstanceRepository(id, definitionKey, variables);
    createCaseDataSetRepository(id, definitionKey, variables);
  }

  public void createDocument(String documentProfileName, Map<String, Object> variables) throws URISyntaxException {
    String uri = URI_PREFIX + "document/create";
    String query = new StringBuilder("documentProfileName=")
        .append(documentProfileName)
        .append("&caseInstanceNodeRef=")
        .append(variables.get(Case.INSTANCE_NODE_REF))
        .toString();

    ResponseEntity<?> responseEntity = alfrescoProxyService.proxy(null, HttpMethod.PUT, uri, query);
    String responseText = (String) responseEntity.getBody();
    if (responseEntity.getStatusCode() != HttpStatus.OK) {
      throw new ApiException(responseEntity.getStatusCode().value(), responseText);
    }
  }

  public void registerDocument(Map<String, Object> variables) throws URISyntaxException {
    String uri = URI_PREFIX + "document/register";
    String documentProfileName = (String) variables.get(DOCUMENT_PROFILE_NAME);
    if (documentProfileName == null) {
      throw new IllegalArgumentException("Document profile name is missing in variables");
    }
    String query = new StringBuilder("documentProfileName=")
        .append(documentProfileName)
        .append("&caseInstanceNodeRef=")
        .append(variables.get(Case.INSTANCE_NODE_REF))
        .toString();

    ResponseEntity<?> responseEntity = alfrescoProxyService.proxy(null, HttpMethod.PUT, uri, query);
    if (responseEntity.getStatusCode() != HttpStatus.OK) {
      throw new ApiException(responseEntity.getStatusCode().value(), (String) responseEntity.getBody());
    }

    variables.putAll((Map) responseEntity.getBody());
  }

  private void createCaseInstanceRepository(String id, String definitionKey, Map<String, Object> variables) throws URISyntaxException {
    String uri = URI_PREFIX + "case";
    String query = "caseDefinitionRef=" + definitionKey;

    Map<String, Object> body = new HashMap<>();
    body.put("id", id);
    body.put("variables", variables);

    ResponseEntity<?> responseEntity = alfrescoProxyService.proxy(body, HttpMethod.POST, uri, query);
    String responseText = (String) responseEntity.getBody();
    if (responseEntity.getStatusCode() != HttpStatus.OK) {
      throw new ApiException(responseEntity.getStatusCode().value(), responseText);
    }

    variables.put(Case.INSTANCE_NODE_REF, responseText);
  }

  private void createCaseDataSetRepository(String id, String definitionKey, Map<String, Object> variables) throws URISyntaxException {
    List<Entity> caseDataSetEntities = getCaseDataSets(definitionKey);
    String uri = URI_PREFIX + "dataset";
    String instanceNodeRef = (String) variables.get(Case.INSTANCE_NODE_REF);
    String query = StringUtils.isEmpty(instanceNodeRef) ? "caseInstanceRef=" + id : "caseInstanceNodeRef=" + instanceNodeRef;

    for (Entity entity : caseDataSetEntities) {
      String entityKey = entity.getCode();
      Map<String, Object> attributes = entity.getAttributes();
      attributes.put("datasetDefinitionRef", entityKey);
      attributes.put("datasetData", variables);

      ResponseEntity<?> responseEntity = alfrescoProxyService.proxy(attributes, HttpMethod.POST, uri, query);
      if (responseEntity.getStatusCode() != HttpStatus.OK) {
        throw new ApiException(responseEntity.getStatusCode().value(), (String) responseEntity.getBody());
      }

      addDataSetNodeRef(variables, entityKey, responseEntity.getBody());
    }
  }

  private void addDataSetNodeRef(Map<String, Object> variables, String dataSetKey, Object dataSetNodeRef) {
    Map<String, Object> dataSets = (Map<String, Object>) variables.get(Case.DATASET_NODE_REFS);
    if (dataSets == null) {
      dataSets = new HashMap<>();
      variables.put(Case.DATASET_NODE_REFS, dataSets);
    }

    dataSets.put(dataSetKey, dataSetNodeRef);
  }

  private List<String> getDataSetKeys(String definitionKey) {
    log.info("Looking for dataSets of caseDefinition with key {}", definitionKey);
    ValueSet caseValueSet = valueSetService.loadFlat(CASE_DEFINITION_KEY);
    for (Entity entity : caseValueSet.getEntities()) {
      if (Objects.equals(definitionKey, entity.getAttributes().get("CaseDefinitionKey"))) {
        String[] dataSets = String.valueOf(entity.getAttributes().get("Datasets")).split(";");
        return Arrays.asList(dataSets);
      }
    }
    return Collections.emptyList();
  }

  private List<Entity> getCaseDataSets(String definitionKey) {
    List<String> caseDataSetKeys = getDataSetKeys(definitionKey);
    List<Entity> entities = new ArrayList<>(caseDataSetKeys.size());

    for (String caseDataSetKey : caseDataSetKeys) {
      Entity caseDataSetEntity = valueSetEntityService.load(DATA_SET_DEFINITION_KEY, caseDataSetKey);
      entities.add(caseDataSetEntity);
    }

    return entities;
  }

}

package ee.lifeup.flowhub.proxy.alfresco.service.task;

import ee.lifeup.flowhub.proxy.alfresco.service.AlfrescoCaseInstanceService;
import ee.lifeup.flowhub.proxy.alfresco.service.task.exception.TaskExecutionException;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.cmmn.api.delegate.DelegatePlanItemInstance;

/**
 * Created by Aleksandr Koltakov on 22.08.2018
 */

@Slf4j
public class DocumentCreationServiceTask extends AbstractServiceTask {

    private CmmnRuntimeService cmmnRuntimeService = getBean(CmmnRuntimeService.class);

    private AlfrescoCaseInstanceService alfrescoCaseInstanceService = getBean(AlfrescoCaseInstanceService.class);

    @Setter
    private String documentProfileRef;

    @Override
    public void execute(DelegatePlanItemInstance planItemInstance) {
        String id = planItemInstance.getCaseInstanceId();
        log.debug("Executing document creation service for for case {} and document profile name", id, documentProfileRef);
        try {
            alfrescoCaseInstanceService.createDocument(documentProfileRef, cmmnRuntimeService.getVariables(id));
        } catch (Exception e) {
            throw new TaskExecutionException("Exception creating document", e);
        }
    }
}

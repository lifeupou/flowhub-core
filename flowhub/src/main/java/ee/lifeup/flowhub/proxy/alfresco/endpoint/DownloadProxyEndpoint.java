/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.proxy.alfresco.endpoint;


import ee.lifeup.flowhub.annotation.ProxyQualifier;
import ee.lifeup.flowhub.proxy.alfresco.service.AlfrescoFileProxyService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URISyntaxException;

import static ee.lifeup.flowhub.annotation.ProxyQualifier.Proxy.ALFRESCO_FILE_PROXY;
import static ee.lifeup.flowhub.proxy.alfresco.service.AlfrescoProxyService.ALFRESCO_PROXY_BASE_PATH;

@Api
@RestController
@RequestMapping(ALFRESCO_PROXY_BASE_PATH)
public class DownloadProxyEndpoint {

    private AlfrescoFileProxyService alfrescoFileProxyService;

    @Autowired
    public DownloadProxyEndpoint(@ProxyQualifier(ALFRESCO_FILE_PROXY) AlfrescoFileProxyService alfrescoFileProxyService) {
        this.alfrescoFileProxyService = alfrescoFileProxyService;
    }

    @GetMapping(value = "/wcservice/flowhub/download")
    public ResponseEntity<?> downloadPdfFile(HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException, IOException {
        return alfrescoFileProxyService.downloadFileProxy(method, servletRequest);
    }
}

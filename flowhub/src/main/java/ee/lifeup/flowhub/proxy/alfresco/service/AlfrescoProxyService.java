/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.proxy.alfresco.service;

import ee.lifeup.flowhub.annotation.ProxyQualifier;
import ee.lifeup.flowhub.proxy.AbstractBaseProxyService;
import ee.lifeup.flowhub.security.AuthContext;
import ee.lifeup.flowhub.valueset.ApiException;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import static ee.lifeup.flowhub.annotation.ProxyQualifier.Proxy.ALFRESCO_PROXY;

@Service
@ProxyQualifier(ALFRESCO_PROXY)
public class AlfrescoProxyService extends AbstractBaseProxyService {

    static final String X_ALFRESCO_REMOTE_USER = "X-Alfresco-Remote-User";

    public static final String ALFRESCO_PROXY_BASE_PATH = "/alfresco";

    @Value("${alfresco.proxy.scheme}")
    private String proxyScheme;

    @Value("${alfresco.proxy.server}")
    private String proxyServer;

    @Value("${alfresco.proxy.port}")
    private Integer proxyPort;

    @Override
    protected String getProxyScheme() {
        return proxyScheme;
    }

    @Override
    protected String proxyServer() {
        return proxyServer;
    }

    @Override
    protected Integer proxyPort() {
        return proxyPort;
    }

    @Override
    protected HttpHeaders getHeaders(MediaType mediaType) {
        HttpHeaders httpHeaders = super.getHeaders(mediaType);
        httpHeaders.add(X_ALFRESCO_REMOTE_USER, AuthContext.getUser().orElseThrow(() -> new ApiException(HttpStatus.SC_UNAUTHORIZED, "Authorization is failed. User is not found")));
        return httpHeaders;
    }
}

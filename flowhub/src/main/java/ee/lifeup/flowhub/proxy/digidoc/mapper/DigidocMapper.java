/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.proxy.digidoc.mapper;

import ee.lifeup.flowhub.proxy.digidoc.model.CreateSignHashRequest;
import ee.lifeup.flowhub.proxy.digidoc.model.FileResource;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Component
public class DigidocMapper {

  public MultiValueMap<String, Object> mapFromSignHashCreateRequest(CreateSignHashRequest createRequest) throws IOException {
    MultipartFile file = createRequest.getFile();
    MultipartFile container = createRequest.getContainer();

    MultiValueMap<String, Object> requestData = new LinkedMultiValueMap<>();
    requestData.add("id", createRequest.getId());
    requestData.add("cert", createRequest.getCert());
    requestData.add("file", new FileResource(file.getName(), IOUtils.toByteArray(file.getInputStream())));
    if (container != null) {
      requestData.add("container", new FileResource(container.getName(), IOUtils.toByteArray(container.getInputStream())));
    }
    return requestData;
  }

  public MultiValueMap<String, Object> mapFromMultipartFile(MultipartFile multipartFile) throws IOException {
    String multipartFileName = multipartFile.getName();
    MultiValueMap<String, Object> requestData = new LinkedMultiValueMap<>(1);
    requestData.add(multipartFileName, new FileResource(multipartFileName, IOUtils.toByteArray(multipartFile.getInputStream())));
    return requestData;
  }

}
package ee.lifeup.flowhub.proxy.alfresco.service;

import ee.lifeup.flowhub.annotation.ProxyQualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import static ee.lifeup.flowhub.annotation.ProxyQualifier.Proxy.ALFRESCO_ADMIN_PROXY;

@Service
@ProxyQualifier(ALFRESCO_ADMIN_PROXY)
public class AlfrescoAdminProxyService extends AlfrescoProxyService {

  @Override
  protected HttpHeaders getHeaders(MediaType mediaType) {
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.add(X_ALFRESCO_REMOTE_USER, "admin");
    return httpHeaders;
  }
}

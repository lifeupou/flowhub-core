/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.proxy.alfresco.endpoint;

import ee.lifeup.flowhub.annotation.ProxyQualifier;
import ee.lifeup.flowhub.proxy.alfresco.service.AlfrescoProxyService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.URISyntaxException;

import static ee.lifeup.flowhub.annotation.ProxyQualifier.Proxy.ALFRESCO_PROXY;
import static ee.lifeup.flowhub.proxy.alfresco.service.AlfrescoProxyService.ALFRESCO_PROXY_BASE_PATH;

@Api
@RestController
@RequestMapping(ALFRESCO_PROXY_BASE_PATH)
public class DatasetAccessRestrictionProxyEndpoint {

    private final AlfrescoProxyService proxyService;

    @Autowired
    public DatasetAccessRestrictionProxyEndpoint(@ProxyQualifier(ALFRESCO_PROXY) AlfrescoProxyService proxyService) {
        this.proxyService = proxyService;
    }

    @RequestMapping(value = "/wcservice/flowhub/dataset/access-data", method = {RequestMethod.GET, RequestMethod.PUT})
    public Object datasetAccessRestrictionProxy(@RequestBody(required = false) Object body, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException {
        return proxyService.proxy(body, method, servletRequest);
    }

}

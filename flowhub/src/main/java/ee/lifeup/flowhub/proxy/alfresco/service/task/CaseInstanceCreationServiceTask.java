package ee.lifeup.flowhub.proxy.alfresco.service.task;

import ee.lifeup.flowhub.keis.Case;
import ee.lifeup.flowhub.proxy.alfresco.service.AlfrescoCaseInstanceService;
import ee.lifeup.flowhub.proxy.alfresco.service.task.exception.TaskExecutionException;
import lombok.extern.slf4j.Slf4j;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.cmmn.api.delegate.DelegatePlanItemInstance;

import java.util.Map;

/**
 * Created by Aleksandr Koltakov on 23.08.2018
 */
@Slf4j
public class CaseInstanceCreationServiceTask extends AbstractServiceTask  {

    private CmmnRuntimeService cmmnRuntimeService = getBean(CmmnRuntimeService.class);

    private AlfrescoCaseInstanceService alfrescoCaseInstanceService = getBean(AlfrescoCaseInstanceService.class);


    @Override
    public void execute(DelegatePlanItemInstance planItemInstance) {
        String id = planItemInstance.getCaseInstanceId();
        log.debug("Executing case instance creation service for for case {}", id);
        try {
            Map<String, Object> variables = cmmnRuntimeService.getVariables(id);

            alfrescoCaseInstanceService.createCaseInstance(id, variables);
            cmmnRuntimeService.setVariable(id, Case.INSTANCE_NODE_REF, variables.get(Case.INSTANCE_NODE_REF));
            cmmnRuntimeService.setVariable(id, Case.DATASET_NODE_REFS, variables.get(Case.DATASET_NODE_REFS));
        } catch (Exception e) {
            throw new TaskExecutionException("Exception creating case instance in repository", e);
        }
    }
}

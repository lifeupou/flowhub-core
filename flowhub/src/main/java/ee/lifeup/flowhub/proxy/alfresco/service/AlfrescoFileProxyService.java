/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.proxy.alfresco.service;

import com.google.common.base.Preconditions;
import ee.lifeup.flowhub.annotation.ProxyQualifier;
import ee.lifeup.flowhub.valueset.ApiException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import static ee.lifeup.flowhub.annotation.ProxyQualifier.Proxy.ALFRESCO_FILE_PROXY;

@Service
@ProxyQualifier(ALFRESCO_FILE_PROXY)
public class AlfrescoFileProxyService extends AlfrescoProxyService {

    /**
     * Get file content from Alfresco.
     *
     * @param nodeRef File node reference.
     * @return Input stream of file.
     */
    public InputStream getFileContent(String nodeRef) throws URISyntaxException, IOException {
        Preconditions.checkArgument(StringUtils.isNotEmpty(nodeRef), "File node reference must not be null or empty");
        ResponseEntity<?> responseEntity = proxy(null, HttpMethod.GET, getFileRequestURI(nodeRef), Resource.class);
        Resource fileContent = (Resource) responseEntity.getBody();
        if (fileContent == null || fileContent.contentLength() == 0) {
            throw new ApiException(org.apache.http.HttpStatus.SC_NO_CONTENT, "File content is empty");
        }
        return fileContent.getInputStream();
    }
    
    public ResponseEntity<?> downloadFileProxy(HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException, IOException {
        ResponseEntity<?> responseEntity = proxy(null, method, servletRequest, null, Resource.class);
        Resource body = (Resource) responseEntity.getBody();
        if (body != null) {
            InputStream responseInputStream = body.getInputStream();
            // headers.setContentType(MediaType.APPLICATION_PDF);
            return new ResponseEntity<>(IOUtils.toByteArray(responseInputStream), responseEntity.getHeaders(), org.springframework.http.HttpStatus.OK);
        }
        return ResponseEntity.noContent().build();
    }

    private URI getFileRequestURI(String nodeRef) throws URISyntaxException {
        nodeRef = nodeRef.replaceFirst(":/", StringUtils.EMPTY);
        String requestURI = "/alfresco/wcservice/api/node/content/" + nodeRef;
        return new URI(getProxyScheme(), null, proxyServer(), proxyPort(), requestURI, null, null);
    }

    public String getRequestURI(HttpServletRequest servletRequest) {
        String nodeRef = servletRequest.getParameter(NODE_REF_PARAMETER);
        if (StringUtils.isEmpty(nodeRef)) {
            throw new ApiException(HttpStatus.SC_PRECONDITION_FAILED, "Parameter nodeRef is missing");
        }
        nodeRef = nodeRef.replaceFirst(":/", StringUtils.EMPTY);
        String requestURI = super.getRequestURI(servletRequest);
        requestURI = requestURI.replaceFirst("flowhub/download", "api/node/content/" + nodeRef);
        return requestURI;
    }
    
}

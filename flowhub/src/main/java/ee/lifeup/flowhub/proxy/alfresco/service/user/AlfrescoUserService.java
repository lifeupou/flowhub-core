package ee.lifeup.flowhub.proxy.alfresco.service.user;

import ee.lifeup.flowhub.annotation.ProxyQualifier;
import ee.lifeup.flowhub.proxy.AbstractBaseProxyService;
import ee.lifeup.flowhub.proxy.alfresco.model.AlfrescoUser;
import ee.lifeup.flowhub.user.UserSync.UserSynchronizer;
import ee.lifeup.flowhub.valueset.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import static ee.lifeup.flowhub.annotation.ProxyQualifier.Proxy.ALFRESCO_ADMIN_PROXY;

@Slf4j
@Component
public class AlfrescoUserService {

  private static final String URI_PREFIX = "/alfresco/wcservice/";
  private static final String MAIN_USER_GROUP = "SEC_Alfresco_Kasutaja";

  private final AbstractBaseProxyService proxyService;

  @Autowired
  public AlfrescoUserService(@ProxyQualifier(ALFRESCO_ADMIN_PROXY) AbstractBaseProxyService proxyService) {
    this.proxyService = proxyService;
  }

  public void syncUsers(UserSynchronizer synchronizer) {
    //TODO: when becomes performance problem paging should be used instead
    int maxItems = 10_000;
    int skip = 0;

    try {
      String requestUri = URI_PREFIX + "api/groups/" + MAIN_USER_GROUP + "/children";
      String query = "authorityType=USER&maxItems=" + maxItems + "&skipCount=" + skip;
      ResponseEntity<?> entity = proxyService.proxy(null, HttpMethod.GET, requestUri, query);

      if (entity.getStatusCodeValue() >= 400) {
        throw new ApiException(entity.getStatusCodeValue(), (String) entity.getBody());
      }

      Map<String, Object> map = (Map<String, Object>) entity.getBody();
      for (Map userData : (List<Map>) map.get("data")) {
        try {
          String username = (String) userData.get("shortName");
          AlfrescoUser user = getUser(username);
          synchronizer.sync(user);
        } catch (Exception e) {
          log.error("Failed to sync user: {}", userData, e);
        }
      }

    } catch (URISyntaxException e) {
      throw new RuntimeException("Failed to get alfresco users", e);
    }

  }

  private AlfrescoUser getUser(String username) throws URISyntaxException {
    String requestUri = URI_PREFIX + "api/people/" + username;
    ResponseEntity<?> entity = proxyService.proxy(null, HttpMethod.GET, requestUri, AlfrescoUser.class);
    return (AlfrescoUser) entity.getBody();
  }
}

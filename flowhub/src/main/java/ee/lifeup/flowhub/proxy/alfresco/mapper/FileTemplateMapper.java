/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.proxy.alfresco.mapper;


import ee.lifeup.flowhub.proxy.alfresco.model.filetemplate.FileTemplateBaseRequest;
import ee.lifeup.flowhub.proxy.alfresco.model.filetemplate.FileTemplateCreateRequest;
import ee.lifeup.flowhub.proxy.alfresco.model.filetemplate.FileTemplateUpdateRequest;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;

@Component
public class FileTemplateMapper {

    public MultiValueMap<String, Object> mapFromCreateRequest(FileTemplateCreateRequest createRequest) throws IOException {
        MultiValueMap<String, Object> requestData = mapFromBaseRequest(createRequest);
        requestData.add("organisationName", createRequest.getOrganisationName());
        return requestData;
    }

    public MultiValueMap<String, Object> mapFromUpdateRequest(FileTemplateUpdateRequest updateRequest) throws IOException {
        MultiValueMap<String, Object> requestData = mapFromBaseRequest(updateRequest);
        requestData.add("nodeRef", updateRequest.getNodeRef());
        return requestData;
    }

    private MultiValueMap<String, Object> mapFromBaseRequest(FileTemplateBaseRequest baseRequest) throws IOException {
        MultiValueMap<String, Object> requestData = new LinkedMultiValueMap<>();
        requestData.add("fileData", new InputStreamResource(baseRequest.getFileData().getInputStream()));
        requestData.add("name", baseRequest.getName());
        requestData.add("title", baseRequest.getTitle());
        requestData.add("description", baseRequest.getDescription());
        requestData.add("fileTemplateType", baseRequest.getFileTemplateType());
        requestData.add("fileTemplateFields", baseRequest.getFileTemplateFields());
        return requestData;
    }
}

/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.proxy.alfresco.endpoint;

import ee.lifeup.flowhub.annotation.ProxyQualifier;
import ee.lifeup.flowhub.proxy.alfresco.mapper.FileTemplateMapper;
import ee.lifeup.flowhub.proxy.alfresco.model.filetemplate.FileTemplateCreateRequest;
import ee.lifeup.flowhub.proxy.alfresco.model.filetemplate.FileTemplateUpdateRequest;
import ee.lifeup.flowhub.proxy.alfresco.service.AlfrescoProxyService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URISyntaxException;

import static ee.lifeup.flowhub.annotation.ProxyQualifier.Proxy.ALFRESCO_PROXY;
import static ee.lifeup.flowhub.proxy.alfresco.service.AlfrescoProxyService.ALFRESCO_PROXY_BASE_PATH;

@Api
@RestController
@RequestMapping(ALFRESCO_PROXY_BASE_PATH)
public class FileTemplateProxyEndpoint {

    private final AlfrescoProxyService proxyService;
    private final FileTemplateMapper fileTemplateMapper;

    @Autowired
    public FileTemplateProxyEndpoint(@ProxyQualifier(ALFRESCO_PROXY) AlfrescoProxyService proxyService,
                                     FileTemplateMapper fileTemplateMapper) {
        this.proxyService = proxyService;
        this.fileTemplateMapper = fileTemplateMapper;
    }

    @PostMapping(value = "/wcservice/flowhub/file-template")
    public Object createFileTemplate(FileTemplateCreateRequest body, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException, IOException {
        MultiValueMap<String, Object> requestData = fileTemplateMapper.mapFromCreateRequest(body);
        return proxyService.proxy(requestData, method, servletRequest, MediaType.MULTIPART_FORM_DATA);
    }

    @PutMapping(value = "/wcservice/flowhub/file-template")
    public Object updateFileTemplate(FileTemplateUpdateRequest body, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException, IOException {
        MultiValueMap<String, Object> requestData = fileTemplateMapper.mapFromUpdateRequest(body);
        return proxyService.proxy(requestData, method, servletRequest, MediaType.MULTIPART_FORM_DATA);
    }

    @GetMapping(value = "/wcservice/flowhub/file-template")
    public Object getFileTemplate(@RequestBody(required = false) Object body, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException {
        return proxyService.proxy(body, method, servletRequest);
    }

    @DeleteMapping(value = "/wcservice/flowhub/file-template")
    public Object deleteFileTemplate(@RequestBody(required = false) Object body, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException {
        return proxyService.proxy(body, method, servletRequest);
    }

    @GetMapping(value = "/wcservice/flowhub/file-templates")
    public Object listFileTemplates(@RequestBody(required = false) Object body, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException {
        return proxyService.proxy(body, method, servletRequest);
    }
}
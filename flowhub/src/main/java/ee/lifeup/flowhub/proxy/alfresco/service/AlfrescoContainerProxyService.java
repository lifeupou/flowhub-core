/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.proxy.alfresco.service;

import ee.lifeup.flowhub.annotation.ProxyQualifier;
import ee.lifeup.flowhub.digitalsign.model.ContainerMetadata;
import ee.lifeup.flowhub.digitalsign.model.SignContainerAware;
import ee.lifeup.flowhub.digitalsign.model.SignFileType;
import ee.lifeup.flowhub.proxy.alfresco.mapper.DatasetFileMapper;
import ee.lifeup.flowhub.valueset.ApiException;
import lombok.AllArgsConstructor;
import org.apache.http.HttpStatus;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.net.URISyntaxException;

import static ee.lifeup.flowhub.annotation.ProxyQualifier.Proxy.ALFRESCO_CONTAINER_PROXY;

@Service
@ProxyQualifier(ALFRESCO_CONTAINER_PROXY)
@AllArgsConstructor
public class AlfrescoContainerProxyService extends AlfrescoProxyService {

    private static final String CASE_FILE_CONTAINER_STORE_URI = "/alfresco/wcservice/flowhub/case/file";
    private static final String DATASET_FILE_CONTAINER_STORE_URI = "/alfresco/wcservice/flowhub/dataset/file";

    private final DatasetFileMapper datasetFileMapper;

    /**
     * Store container to the Alfresco.
     *
     * @param container         BDOC container.
     * @param containerMetadata container metadata.
     * @param signRequest       Request for sign document.
     * @return signed file reference.
     */
    public String storeContainer(SignContainerAware signRequest, Resource container, ContainerMetadata containerMetadata) throws IOException, URISyntaxException {
        SignFileType signFileType = signRequest.getFileType();
        MultiValueMap<String, Object> requestDat = getRequestData(signRequest, container, containerMetadata);
        ResponseEntity<?> responseEntity = proxy(requestDat, HttpMethod.POST, getRequestURI(signFileType), null, MediaType.MULTIPART_FORM_DATA);
        return (String) responseEntity.getBody();
    }

    private String getRequestURI(SignFileType signFileType) {
        switch (signFileType) {
            case CASE:
                return CASE_FILE_CONTAINER_STORE_URI;
            case DATASET:
                return DATASET_FILE_CONTAINER_STORE_URI;
            default:
                throw new ApiException(HttpStatus.SC_BAD_REQUEST, "File type is not correct: " + signFileType);
        }
    }

    private MultiValueMap<String, Object> getRequestData(SignContainerAware signRequest, Resource container, ContainerMetadata containerMetadata) throws IOException {
        SignFileType signFileType = signRequest.getFileType();
        switch (signFileType) {
            case CASE:
                return createCaseRequest();
            case DATASET:
                return datasetFileMapper.mapFromSignCardRequest(signRequest, container, containerMetadata);
            default:
                throw new ApiException(HttpStatus.SC_BAD_REQUEST, "File type is not correct: " + signFileType);
        }
    }


    private MultiValueMap<String, Object> createCaseRequest() {
        //TODO implement later
        return new LinkedMultiValueMap<>();
    }
}

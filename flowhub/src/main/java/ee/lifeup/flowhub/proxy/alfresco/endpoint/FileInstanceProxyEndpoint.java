package ee.lifeup.flowhub.proxy.alfresco.endpoint;

import ee.lifeup.flowhub.annotation.ProxyQualifier;
import ee.lifeup.flowhub.proxy.alfresco.service.AlfrescoProxyService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.URISyntaxException;

import static ee.lifeup.flowhub.annotation.ProxyQualifier.Proxy.ALFRESCO_PROXY;
import static ee.lifeup.flowhub.proxy.alfresco.service.AlfrescoProxyService.ALFRESCO_PROXY_BASE_PATH;

/**
 * Created by Aleksandr Koltakov on 30.08.2018
 */
@Api
@RestController
@RequestMapping(ALFRESCO_PROXY_BASE_PATH)
public class FileInstanceProxyEndpoint {

    private final AlfrescoProxyService proxyService;

    @Autowired
    public FileInstanceProxyEndpoint(@ProxyQualifier(ALFRESCO_PROXY) AlfrescoProxyService proxyService) {
        this.proxyService = proxyService;
    }

    @RequestMapping(value = "/wcservice/flowhub/file-instance", method = {RequestMethod.POST, RequestMethod.PUT})
    public Object deleteFileTemplate(@RequestBody(required = false) Object body, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException {
        return proxyService.proxy(body, method, servletRequest);
    }
}

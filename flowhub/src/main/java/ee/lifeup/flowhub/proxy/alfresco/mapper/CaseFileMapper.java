/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.proxy.alfresco.mapper;

import ee.lifeup.flowhub.proxy.alfresco.model.casefile.CaseFileBaseRequest;
import ee.lifeup.flowhub.proxy.alfresco.model.casefile.CaseFileCreateRequest;
import ee.lifeup.flowhub.proxy.alfresco.model.casefile.CaseFileUpdateRequest;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;

@Component
public class CaseFileMapper {

    public MultiValueMap<String, Object> mapFromCreateRequest(CaseFileCreateRequest createRequest) throws IOException {
        MultiValueMap<String, Object> requestData = mapFromBaseRequest(createRequest);
        requestData.add("caseRef", createRequest.getCaseRef());
        return requestData;
    }

    public MultiValueMap<String, Object> mapFromUpdateRequest(CaseFileUpdateRequest updateRequest) throws IOException {
        MultiValueMap<String, Object> requestData = mapFromBaseRequest(updateRequest);
        requestData.add("fileRef", updateRequest.getFileRef());
        return requestData;
    }

    private MultiValueMap<String, Object> mapFromBaseRequest(CaseFileBaseRequest baseRequest) throws IOException {
        MultiValueMap<String, Object> requestData = new LinkedMultiValueMap<>();
        requestData.add("fileData", new InputStreamResource(baseRequest.getFileData().getInputStream()));
        requestData.add("fileType", baseRequest.getFileType());
        requestData.add("fileTitle", baseRequest.getFileTitle());
        requestData.add("fileName", baseRequest.getFileName());
        requestData.add("fileDescription", baseRequest.getFileDescription());
        return requestData;
    }
}

/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.proxy.alfresco.mapper;

import ee.lifeup.flowhub.digitalsign.model.ContainerMetadata;
import ee.lifeup.flowhub.digitalsign.model.SignContainerAware;
import ee.lifeup.flowhub.proxy.alfresco.model.datasetfile.DatasetFileBaseRequest;
import ee.lifeup.flowhub.proxy.alfresco.model.datasetfile.DatasetFileCreateRequest;
import ee.lifeup.flowhub.proxy.alfresco.model.datasetfile.DatasetFileUpdateRequest;
import ee.lifeup.flowhub.proxy.digidoc.model.FileResource;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.util.Date;

/**
 * Mapper for dataset files.
 */
@Component
public class DatasetFileMapper {

    private static final String CONTAINER_TYPE = "bdoc";

    private static final String DATASET_FILE_DATA = "datasetFileData";
    private static final String DATASET_FILE_TYPE = "datasetFileType";
    private static final String DATASET_FILE_NAME = "datasetFileName";
    private static final String DATASET_FILE_TITLE = "datasetFileTitle";
    private static final String DATASET_NODE_REF = "datasetNodeRef";
    private static final String DATASET_FILE_METADATA = "datasetFileMetadata";
    private static final String DATASET_FILE_REF = "datasetFileRef";
    private static final String DATASET_FILE_DESCRIPTION = "datasetFileDescription";

    public MultiValueMap<String, Object> mapFromSignCardRequest(SignContainerAware signRequest, Resource container, ContainerMetadata containerMetadata) throws IOException {
        String containerName = signRequest.getContainerName();

        MultiValueMap<String, Object> requestData = new LinkedMultiValueMap<>();
        requestData.add(DATASET_FILE_DATA, new FileResource(containerName, IOUtils.toByteArray(container.getInputStream())));
        requestData.add(DATASET_FILE_TYPE, CONTAINER_TYPE);
        requestData.add(DATASET_FILE_NAME, containerName + "_" + new Date().getTime() + "." + CONTAINER_TYPE);
        requestData.add(DATASET_FILE_TITLE, containerName);
        requestData.add(DATASET_FILE_DESCRIPTION, containerName);
        requestData.add(DATASET_NODE_REF, signRequest.getNodeRef());
        requestData.add(DATASET_FILE_METADATA, containerMetadata);
        return requestData;
    }

    public MultiValueMap<String, Object> mapFromCreateRequest(DatasetFileCreateRequest createRequest) throws IOException {
        MultiValueMap<String, Object> requestData = mapFromBaseRequest(createRequest);
        requestData.add(DATASET_NODE_REF, createRequest.getDatasetNodeRef());
        return requestData;
    }

    public MultiValueMap<String, Object> mapFromUpdateRequest(DatasetFileUpdateRequest updateRequest) throws IOException {
        MultiValueMap<String, Object> requestData = mapFromBaseRequest(updateRequest);
        requestData.add(DATASET_FILE_REF, updateRequest.getDatasetFileRef());
        return requestData;
    }

    private MultiValueMap<String, Object> mapFromBaseRequest(DatasetFileBaseRequest baseRequest) throws IOException {
        MultiValueMap<String, Object> requestData = new LinkedMultiValueMap<>();
        requestData.add(DATASET_FILE_DATA, new InputStreamResource(baseRequest.getDatasetFileData().getInputStream()));
        requestData.add(DATASET_FILE_TYPE, baseRequest.getDatasetFileType());
        requestData.add(DATASET_FILE_TITLE, baseRequest.getDatasetFileTitle());
        requestData.add(DATASET_FILE_NAME, baseRequest.getDatasetFileName());
        requestData.add(DATASET_FILE_DESCRIPTION, baseRequest.getDatasetFileDescription());
        return requestData;
    }
}

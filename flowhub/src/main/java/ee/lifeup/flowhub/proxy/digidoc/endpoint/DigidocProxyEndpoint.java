/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.proxy.digidoc.endpoint;

import ee.lifeup.flowhub.proxy.digidoc.mapper.DigidocMapper;
import ee.lifeup.flowhub.proxy.digidoc.model.CreateSignHashRequest;
import ee.lifeup.flowhub.proxy.digidoc.model.SignDocumentRequest;
import ee.lifeup.flowhub.proxy.digidoc.service.DigidocProxyService;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URISyntaxException;

import static ee.lifeup.flowhub.proxy.digidoc.service.DigidocProxyService.DIGIDOC_PROXY_BASE_PATH;

@Api
@RestController
@AllArgsConstructor
@RequestMapping(DIGIDOC_PROXY_BASE_PATH)
public class DigidocProxyEndpoint {

    private final DigidocProxyService digidocProxyService;
    private final DigidocMapper digidocMapper;

    @PostMapping(value = "/signature/hash/create")
    public ResponseEntity<?> createSignatureHash(CreateSignHashRequest createSignHashRequest, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException, IOException {
        MultiValueMap<String, Object> requestData = digidocMapper.mapFromSignHashCreateRequest(createSignHashRequest);
        return digidocProxyService.proxy(requestData, method, servletRequest, MediaType.MULTIPART_FORM_DATA, String.class);
    }

    @PostMapping(value = "/document/sign")
    public ResponseEntity<?> signDocument(@RequestBody SignDocumentRequest signDocumentRequest, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException {
        return digidocProxyService.proxy(signDocumentRequest, method, servletRequest, MediaType.APPLICATION_JSON, Resource.class);
    }

    @PostMapping(value = "/container/metadata/extract")
    public ResponseEntity<?> extractContainerMetadata(@RequestPart MultipartFile containerFile, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException, IOException {
        MultiValueMap<String, Object> requestData = digidocMapper.mapFromMultipartFile(containerFile);
        return digidocProxyService.proxy(requestData, method, servletRequest, MediaType.MULTIPART_FORM_DATA);
    }

    @PostMapping(value = "/container/files/extract")
    public ResponseEntity<?> extractContainerFiles(@RequestPart MultipartFile containerFile, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException, IOException {
        MultiValueMap<String, Object> requestData = digidocMapper.mapFromMultipartFile(containerFile);
        return digidocProxyService.proxy(requestData, method, servletRequest, MediaType.MULTIPART_FORM_DATA, Resource.class);
    }

    @PostMapping(value = "/container/validate")
    public ResponseEntity<?> validateContainer(@RequestPart MultipartFile containerFile, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException, IOException {
        MultiValueMap<String, Object> requestData = digidocMapper.mapFromMultipartFile(containerFile);
        return digidocProxyService.proxy(requestData, method, servletRequest, MediaType.MULTIPART_FORM_DATA);
    }
}



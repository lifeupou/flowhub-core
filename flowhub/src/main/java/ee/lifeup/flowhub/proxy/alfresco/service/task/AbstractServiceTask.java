package ee.lifeup.flowhub.proxy.alfresco.service.task;

import ee.lifeup.flowhub.proxy.alfresco.service.task.exception.TaskExecutionException;
import lombok.extern.slf4j.Slf4j;
import org.flowable.cmmn.api.delegate.DelegatePlanItemInstance;
import org.flowable.cmmn.api.delegate.PlanItemJavaDelegate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Created by Aleksandr Koltakov on 23.08.2018
 */
@Component
@Slf4j
public class AbstractServiceTask implements ApplicationContextAware, PlanItemJavaDelegate {

    protected static ApplicationContext applicationContext;

    protected static  <T> T getBean(Class<T> clazz)  {
        return applicationContext.getBean(clazz);
    }

    @Override
    @SuppressWarnings("squid:S2696")
    public void setApplicationContext(ApplicationContext applicationContext) {
        AbstractServiceTask.applicationContext = applicationContext;
    }

    @Override
    public void execute(DelegatePlanItemInstance planItemInstance) {
        throw new TaskExecutionException("'execute' method has not been overridden");
    }
}

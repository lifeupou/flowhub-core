/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.proxy;

import ee.lifeup.flowhub.valueset.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

@Slf4j
public abstract class AbstractBaseProxyService {

  public static final String NODE_REF_PARAMETER = "nodeRef";

  @Autowired
  protected RestTemplate restTemplate;

  protected abstract String getProxyScheme();

  protected abstract String proxyServer();

  protected abstract Integer proxyPort();

  protected String getRequestURI(HttpServletRequest servletRequest) {
    String requestUri = servletRequest.getRequestURI();
    return requestUri.replaceFirst(servletRequest.getContextPath(), StringUtils.EMPTY);
  }

  protected HttpHeaders getHeaders(MediaType mediaType) {
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(mediaType);
    return httpHeaders;
  }

  public ResponseEntity<?> proxy(Object body, HttpMethod method, HttpServletRequest servletRequest) throws URISyntaxException {
    return proxy(body, method, getRequestURI(servletRequest), servletRequest.getQueryString());
  }

  public ResponseEntity<?> proxy(Object body, HttpMethod method, HttpServletRequest servletRequest, MediaType mediaType) throws URISyntaxException {
    return proxy(body, method, getRequestURI(servletRequest), servletRequest.getQueryString(), mediaType);
  }

  public <T> ResponseEntity<?> proxy(Object body, HttpMethod method, HttpServletRequest servletRequest, MediaType mediaType, Class<T> responseType) throws URISyntaxException {
    URI uri = new URI(getProxyScheme(), null, proxyServer(), proxyPort(), getRequestURI(servletRequest), servletRequest.getQueryString(), null);
    return proxy(body, method, uri, mediaType, responseType);
  }

    public <T> ResponseEntity<?> proxy(Object body, HttpMethod method, HttpServletRequest servletRequest, String queryString, MediaType mediaType, Class<T> responseType) throws URISyntaxException {
        URI uri = new URI(getProxyScheme(), null, proxyServer(), proxyPort(), getRequestURI(servletRequest), queryString, null);
        return proxy(body, method, uri, mediaType, responseType);
    }

    public <T> ResponseEntity<?> proxy(Object body, HttpMethod method, String requestUri, String queryString, MediaType mediaType, Class<T> responseType) throws URISyntaxException {
        URI uri = new URI(getProxyScheme(), null, proxyServer(), proxyPort(), requestUri, queryString, null);
        return proxy(body, method, uri, mediaType, responseType);
    }

  public ResponseEntity<?> proxy(Object body, HttpMethod method, String requestUri, String queryString) throws URISyntaxException {
    URI uri = new URI(getProxyScheme(), null, proxyServer(), proxyPort(), requestUri, queryString, null);
    return proxy(body, method, uri);
  }

  public ResponseEntity<?> proxy(Object body, HttpMethod method, String requestUri, String queryString, MediaType mediaType) throws URISyntaxException {
    URI uri = new URI(getProxyScheme(), null, proxyServer(), proxyPort(), requestUri, queryString, null);
    return proxy(body, method, uri, mediaType, Object.class);
  }

  public ResponseEntity<?> proxy(Object body, HttpMethod method, String requestUri, MediaType mediaType, Class<?> responseType) throws URISyntaxException {
    URI uri = new URI(getProxyScheme(), null, proxyServer(), proxyPort(), requestUri, null, null);
    return proxy(body, method, uri, mediaType, responseType);
  }

  public ResponseEntity<?> proxy(Object body, HttpMethod method, String requestUri, MediaType mediaType) throws URISyntaxException {
    URI uri = new URI(getProxyScheme(), null, proxyServer(), proxyPort(), requestUri, null, null);
    return proxy(body, method, uri, mediaType, Object.class);
  }

  public ResponseEntity<?> proxy(Object body, HttpMethod method, String requestUri) throws URISyntaxException {
    URI uri = new URI(getProxyScheme(), null, proxyServer(), proxyPort(), requestUri, null, null);
    return proxy(body, method, uri);
  }

  public ResponseEntity<?> proxy(Object body, HttpMethod method, String requestUri, Class<?> responseType) throws URISyntaxException {
    URI uri = new URI(getProxyScheme(), null, proxyServer(), proxyPort(), requestUri, null, null);
    return proxy(body, method, uri, MediaType.APPLICATION_JSON, responseType);
  }

  public <T> ResponseEntity<?> proxy(Object body, HttpMethod method, URI uri, Class<T> responseType) {
    return proxy(body, method, uri, MediaType.APPLICATION_JSON, responseType);
  }

  private ResponseEntity<?> proxy(Object body, HttpMethod method, URI uri) {
    return proxy(body, method, uri, MediaType.APPLICATION_JSON, Object.class);
  }

  private <T> ResponseEntity<?> proxy(Object body, HttpMethod method, URI uri, MediaType mediaType, Class<T> responseType) {
    HttpEntity<?> entity = new HttpEntity<>(body, getHeaders(mediaType));
    ResponseEntity<?> resp;
    try {
      resp = restTemplate.exchange(uri, method, entity, responseType);
    } catch (RestClientResponseException exc) {
      log.error("Proxy request {} {} has failed: \n {}", method, uri, exc.getResponseBodyAsString());
      throw new ApiException(exc.getRawStatusCode(), exc.getResponseBodyAsString());
    }
    if (resp.getBody() != null) {
      return ResponseEntity.ok(resp.getBody());
    }
    return ResponseEntity.noContent().build();
  }
}

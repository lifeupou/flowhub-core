package ee.lifeup.flowhub.proxy.alfresco.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AlfrescoUser {

  private String userName;
  private String firstName;
  private String lastName;
  @JsonProperty("mobile")
  private String phoneNumber;
  private String email;
  @JsonProperty("instantmsg")
  private String personalCode;
}

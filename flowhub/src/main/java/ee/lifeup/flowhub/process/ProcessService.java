/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.process;

import lombok.AllArgsConstructor;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.engine.runtime.ProcessInstanceQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class ProcessService {
  private final RuntimeService processRuntimeService;

  @Transactional
  public void create(Process proc) {
    processRuntimeService.createProcessInstanceBuilder()
        .processDefinitionKey(proc.getProcessDefinitionKey())
        .variables(proc.getVariables())
        .start();
  }

  public List<Process> search(ProcessQuery query) {
    ProcessInstanceQuery q = processRuntimeService.createProcessInstanceQuery();
    q.includeProcessVariables();
    if (query.getId() != null) {
      q.processInstanceId(query.getId());
    }
    return q.list().stream().map(ci -> decorate(ci)).collect(toList());
  }

  public Process get(String id) {
    return decorate(processRuntimeService.createProcessInstanceQuery().processInstanceId(id).singleResult());
  }

  private Process decorate(ProcessInstance pi) {
    Process bc = new Process(pi);
    return bc;
  }

}

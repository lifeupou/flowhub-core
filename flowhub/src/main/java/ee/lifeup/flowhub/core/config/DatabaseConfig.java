/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.core.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
public class DatabaseConfig {

  @Autowired
  private Environment environment;

  @Bean
  @Primary
  @ConfigurationProperties(prefix = "spring.datasource.hikari")
  public HikariConfig dataSourceConfig() {
    return new HikariConfig();
  }

  @Bean
  @DependsOn("liquibase")
  public DataSource pgDataSource() {
    return new HikariDataSource(dataSourceConfig());
  }

  @Lazy
  @Bean
  @ConfigurationProperties(prefix = "spring.datasource.hikari")
  public HikariConfig lqDataSourceConfig() {
    HikariConfig config = new HikariConfig();
    config.setMaximumPoolSize(1);
    return config;
  }

  /**
   * @see LiquibaseAutoConfiguration
   */
  @Bean
  @ConfigurationProperties(prefix = "liquibase")
  public SpringLiquibase liquibase() {
    boolean enabled = Boolean.parseBoolean(environment.getProperty("liquibase.enabled", "false"));

    SpringLiquibase l = new SpringLiquibase();
    l.setShouldRun(enabled);
    if (enabled) {
      l.setDataSource(new HikariDataSource(lqDataSourceConfig()));
    }
    return l;
  }

}

/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.core.health;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class AlfrescoStatus implements HealthIndicator {

  private RestTemplate restTemplate;

  @Value("${alfresco.proxy.scheme}://${alfresco.proxy.server}:${alfresco.proxy.port}/alfresco/wcs/api/server")
  private String alfrescoUrl;

  public AlfrescoStatus() {
    this.restTemplate = new RestTemplate();
  }

  @Override
  public Health health() {
    try {
      String resp = restTemplate.getForObject(alfrescoUrl, String.class);
      log.info("Alfresco status: {}", resp);
    } catch (RestClientException e) {
      log.error("Alfresco status check failed", e);
      return Health.down().status(e.toString()).build();
    }
    return Health.up().build();
  }

}

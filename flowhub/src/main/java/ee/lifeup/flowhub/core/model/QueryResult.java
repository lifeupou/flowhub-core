/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class QueryResult<T> {
  private List<T> data;
  private SearchResultMeta meta = new SearchResultMeta();
  private Map<String, String> links;
  @JsonIgnore
  private QueryParams queryParams;

  public QueryResult() {
  }

  public QueryResult(List<T> data) {
    this.data = data;
    this.meta.setTotal(data.size());
  }

  public QueryResult(Integer total, QueryParams params) {
    this.queryParams = params;
    this.meta.setTotal(total);
    this.meta.setPages((int) Math.ceil((double) total / params.getLimit()));
    this.meta.setItemsPerPage(params.getLimit());
    this.meta.setOffset(params.getOffset());
  }

  public void buildPagingLinks(String pathInfo, String queryString) {
    if (links == null) {
      links = new HashMap<>();
    }
    if (queryParams.getLimit() == 0 || meta.getTotal() == 0) {
      return;
    }
    String pageUrl = pathInfo;
    queryString = StringUtils.isEmpty(queryString) ? "" : StringUtils.removePattern(queryString, "[&?]offset=[0-9]+");
    pageUrl += queryString + (StringUtils.contains(queryString, "?") ? "&" : "?") + "offset=";
    Integer offset = queryParams.getOffset();
    Integer count = queryParams.getLimit();

    links.put("self", pageUrl + offset);
    links.put("first", pageUrl + 1);
    links.put("last", pageUrl + getMeta().getPages());
    if (offset > 1) {
      links.put("prev", pageUrl + (offset - 1));
    }
    if (offset * count < getMeta().getTotal()) {
      links.put("next", pageUrl + (offset + 1));
    }
  }

  @Getter
  @Setter
  public static class SearchResultMeta {
    private Integer total;
    private Integer pages;
    private Integer offset;
    private Integer itemsPerPage;
  }

}

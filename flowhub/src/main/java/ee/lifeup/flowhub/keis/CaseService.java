/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.keis;

import ee.lifeup.flowhub.casedefinition.CaseDefinitionService;
import ee.lifeup.flowhub.task.Task.TaskCase;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.flowable.cmmn.api.CmmnHistoryService;
import org.flowable.cmmn.api.CmmnRepositoryService;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.cmmn.api.history.HistoricCaseInstance;
import org.flowable.cmmn.api.repository.CaseDefinition;
import org.flowable.cmmn.api.runtime.CaseInstance;
import org.flowable.cmmn.api.runtime.CaseInstanceQuery;
import org.flowable.cmmn.api.runtime.MilestoneInstanceQuery;
import org.flowable.cmmn.api.runtime.PlanItemInstance;
import org.flowable.engine.HistoryService;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.flowable.task.api.history.HistoricTaskInstanceQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.comparator.Comparators;

import java.util.*;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class CaseService {
  private final CmmnHistoryService cmmnHistoryService;
  private final CmmnRuntimeService cmmnRuntimeService;
  private final CmmnRepositoryService cmmnRepositoryService;
  private final CaseDefinitionService caseDefinitionService;
  private final HistoryService historyService;

  @Transactional
  public Case create(Case keis) {
    Map<String, Object> variables = keis.getVariables() == null ? new HashMap<>() : new HashMap<>(keis.getVariables());
    variables.put(Case.INSTANCE_NODE_REF, null);
    variables.put(Case.DEFINITION_KEY, keis.getCaseDefinitionKey());

    CaseInstance inst = cmmnRuntimeService.createCaseInstanceBuilder()
        .caseDefinitionKey(keis.getCaseDefinitionKey())
        .variables(variables)
        .start();
    Case c = new Case();
    c.setId(inst.getId());
    c.setState(inst.getState());
    c.setCaseDefinitionKey(keis.getCaseDefinitionKey());
    return c;
  }

  public List<Case> search(CaseQuery query) {
    CaseInstanceQuery q = cmmnRuntimeService.createCaseInstanceQuery();
    if (query.getNumber() != null) {
      q.variableValueEquals("name", query.getNumber());
    }
    if (query.getId() != null) {
      q.caseInstanceId(query.getId());
    }
    if (query.getVariable() != null) {
      query.getVariable().forEach(v -> {
        String[] t = StringUtils.split(v, "|");
        q.variableValueEquals(t[0], t[1]);
      });
    }
    q.orderByStartTime().desc();

    List<Case> cases = q.list().stream().map(this::decorate).filter(c -> {
      if (query.getMilestone() == null) {
        return true;
      }

      if (c.getLatestMilestone() != null) {
        return c.getLatestMilestone().getName().equals(query.getMilestone());
      }
      return false;
    }).collect(toList());

    // very fishy
    if (query.getSort() != null) {
      query.getSort().forEach(sortField -> {
        cases.sort((o1, o2) -> {
          Object val1 = o1.variables.get(sortField);
          Object val2 = o2.variables.get(sortField);
          if (val1 == null || val2 == null) {
            return 0;
          }
          return Comparators.comparable().compare(val1, val2);
        });
      });
    }
    return cases;
  }

  public Case get(String id) {
    return decorate(cmmnRuntimeService.createCaseInstanceQuery().caseInstanceId(id).singleResult());
//    return decorate(cmmnHistoryService.createHistoricCaseInstanceQuery().caseInstanceId(id).singleResult());
  }

  public TaskCase getTaskCase(String id) {
    CaseInstance caseInstance = cmmnRuntimeService.createCaseInstanceQuery().caseInstanceId(id).singleResult();

    TaskCase taskCase = new TaskCase();
    taskCase.setId(id);

    Map<String, Object> variables = cmmnRuntimeService.getVariables(id);
    taskCase.setVariables(variables);

    CaseDefinition caseDefinition = cmmnRepositoryService.getCaseDefinition(caseInstance.getCaseDefinitionId());
    String caseDefinitionKey = caseDefinition.getKey();
    taskCase.setNames(caseDefinitionService.getNames(caseDefinitionKey));

    Optional<Milestone> latestMilestone = getLatestMilestone(caseInstance);
    latestMilestone.ifPresent(taskCase::setLatestMilestone);

    return taskCase;
  }

  public List<Map<String, String>> getEnabledPlans(String caseId) {
    List<PlanItemInstance> plans =
        cmmnRuntimeService.createPlanItemInstanceQuery().caseInstanceId(caseId).planItemInstanceStateEnabled().list();
    return plans.stream().map(plan -> {
      Map<String, String> obj = new HashMap<>();
      obj.put("id", plan.getId());
      obj.put("name", plan.getName());
      //      obj.put("state", plan.getState());
      return obj;
    }).collect(toList());
  }

  @Transactional
  public void activatePlan(String caseId, String planId) {
    PlanItemInstance plan = cmmnRuntimeService.createPlanItemInstanceQuery()
        .caseInstanceId(caseId)
        .planItemInstanceId(planId)
        .planItemInstanceStateEnabled()
        .singleResult();
    if (plan != null) {
      cmmnRuntimeService.startPlanItemInstance(plan.getId());
    }
  }

  public void terminate(String caseId) {
    cmmnRuntimeService.terminateCaseInstance(caseId);
  }

  public void complete(String caseId) {
    cmmnRuntimeService.completeCaseInstance(caseId);
  }

  private Case decorate(CaseInstance ci) {
    if (ci == null) {
      return null;
    }
    Case bc = new Case();
    bc.setId(ci.getId());
    bc.setState(ci.getState());
    CaseDefinition caseDefinition = cmmnRepositoryService.getCaseDefinition(ci.getCaseDefinitionId());

    String caseDefinitionKey = caseDefinition.getKey();

    bc.setCaseDefinitionKey(caseDefinitionKey);
    bc.setNames(caseDefinitionService.getNames(caseDefinitionKey));
    bc.setStartTime(ci.getStartTime().getTime());

    HistoricTaskInstanceQuery taskqQuery = historyService.createHistoricTaskInstanceQuery();
    taskqQuery.caseInstanceId(ci.getId());
    taskqQuery.finished();
    taskqQuery.orderByHistoricTaskInstanceEndTime().desc();
    List<HistoricTaskInstance> taskInstances = taskqQuery.list();

    if (taskInstances != null && !taskInstances.isEmpty()) {
      bc.setModifyUser(taskInstances.get(0).getAssignee());
      bc.setModifyTime(taskInstances.get(0).getEndTime().getTime());
    } else {
      bc.setModifyUser("N/A");
      bc.setModifyTime(ci.getStartTime().getTime());
    }

    bc.setVariables(cmmnRuntimeService.getVariables(ci.getId()));

    Optional<Milestone> latest = getLatestMilestone(ci);
    latest.ifPresent(bc::setLatestMilestone);

    return bc;
  }

  private Optional<Milestone> getLatestMilestone(CaseInstance ci) {
    MilestoneInstanceQuery query = cmmnRuntimeService.createMilestoneInstanceQuery();
    query.milestoneInstanceCaseInstanceId(ci.getId());

    return query.list().stream().map(Milestone::new).max(Comparator.comparing(Milestone::getStartDate));
  }

  private Case decorate(HistoricCaseInstance ci) {
    if (ci == null) {
      return null;
    }
    Case bc = new Case();
    bc.setId(ci.getId());
    bc.setState(ci.getState());
    bc.setCaseDefinitionKey(cmmnRepositoryService.getCaseDefinition(ci.getCaseDefinitionId()).getKey());
    bc.setVariables(cmmnRuntimeService.getVariables(ci.getId()));
    return bc;
  }

}

/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.keis;

import ee.lifeup.flowhub.document.DocumentRegistry;
import ee.lifeup.flowhub.proxy.alfresco.service.AlfrescoCaseInstanceService;
import ee.lifeup.flowhub.task.Task;
import ee.lifeup.flowhub.task.TaskService;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api
@RestController
@RequestMapping({ "/private/cases", "/internal/cases" })
@AllArgsConstructor
public class CaseEndpoint {
  private final CaseService caseService;
  private final TaskService taskService;
  private final CmmnRuntimeService cmmnRuntimeService;
  private final AlfrescoCaseInstanceService alfrescoCaseInstanceService;
  private final DocumentRegistry documentRegistry;

  @GetMapping("{id}")
  public Case get(@PathVariable String id) {
    return caseService.get(id);
  }

  @GetMapping("{id}/enabled-plans")
  public List<Map<String, String>> getPlans(@PathVariable String id) {
    return caseService.getEnabledPlans(id);
  }

  @PostMapping("{id}/terminate")
  public ResponseEntity<?> terminate(@PathVariable String id){
    caseService.terminate(id);
    return ResponseEntity.noContent().build();
  }

  @PostMapping("{id}/complete")
  public ResponseEntity<?> complete(@PathVariable String id){
    caseService.complete(id);
    return ResponseEntity.noContent().build();
  }

  //XXX move to PlanEndpoint?
  @PostMapping("{id}/plans/{planId}/activate")
  public ResponseEntity<?> activatePlan(@PathVariable String id, @PathVariable String planId) {
    caseService.activatePlan(id, planId);
    return ResponseEntity.noContent().build();
  }
  @GetMapping("{id}/historic-tasks")
  public List<Task> getHistoricTasks(@PathVariable String id){
    return taskService.getHistoricTasksForCase(id);
  }

  @GetMapping()
  public List<Case> search(CaseQuery query) {
    return caseService.search(query);
  }

  @PostMapping()
  public Case create(@RequestBody Case keis) {
    Case createdCase = caseService.create(keis);
    createdCase.setVariables(cmmnRuntimeService.getVariables(createdCase.getId()));
    return createdCase;
  }

  @PutMapping("{id}/variables")
  public Case setVariables(@PathVariable String id, @RequestBody Map<String, Object> body) {
    cmmnRuntimeService.setVariables(id, body);
    return caseService.get(id);
  }

  @PutMapping("{id}/document")
  public Case createDocument(@PathVariable String id, @RequestParam(name = "documentProfileName") String documentProfileName)  {
    documentRegistry.createDocument(id, documentProfileName);
    return caseService.get(id);
  }

  @PutMapping("{id}/document/register")
  public Case registerDocument(@PathVariable String id, @RequestParam(name = "documentProfileName") String documentProfileName) {
    documentRegistry.registerDocument(id, documentProfileName);
    return caseService.get(id);
  }
}

/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.keis;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class Case {
  public static final String INSTANCE_NODE_REF = "instanceNodeRef";
  public static final String DATASET_NODE_REFS = "datasetNodeRefs";
  public static final String DEFINITION_KEY = "definitionKey";

  protected String id;
  protected Map<String, String> names;
  protected Long startTime;
  protected Long modifyTime;
  protected String modifyUser;
  protected String state;
  protected Map<String, Object> variables;
  protected String caseDefinitionKey;
  protected Milestone latestMilestone;
}

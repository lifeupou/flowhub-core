/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.security;

import com.microsoft.azure.spring.autoconfigure.aad.UserPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import java.util.Map;
import java.util.Optional;

public class AuthContext {

  public static Optional<String> getUser() {
    return getClaims().flatMap(claims -> Optional.ofNullable((String) claims.get("unique_name")));
  }

  public static Optional<Map<String, Object>> getClaims() {
    return getUserPrincipal().flatMap(userPrincipal -> Optional.of(userPrincipal.getClaims()));
  }

  public static Optional<UserPrincipal> getUserPrincipal() {
    return getAuthentication().flatMap(auth -> Optional.ofNullable((UserPrincipal) auth.getPrincipal()));
  }

  public static Optional<PreAuthenticatedAuthenticationToken> getAuthentication() {
    if (SecurityContextHolder.getContext().getAuthentication() == null) {
      return Optional.empty();
    }
    if (SecurityContextHolder.getContext().getAuthentication() instanceof PreAuthenticatedAuthenticationToken) {
      return Optional.ofNullable((PreAuthenticatedAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());
    }
    return Optional.empty();
  }

}

/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.security;

import com.microsoft.azure.spring.autoconfigure.aad.AADAuthenticationFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  private final AADAuthenticationFilter aadAuthFilter;
  private final boolean azureAuthDisabled;

  public WebSecurityConfig(AADAuthenticationFilter aadAuthFilter, @Value("${azure.auth.disabled}") boolean azureAuthDisabled) {
    this.aadAuthFilter = aadAuthFilter;
    this.azureAuthDisabled = azureAuthDisabled;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry = http.authorizeRequests();
    registry.antMatchers("/swagger-ui.html", "/webjars/**", "/swagger-resources/**", "/v2/**", "/actuator/health").permitAll();

    if (azureAuthDisabled) {
      registry.anyRequest().permitAll();
    } else {
      // required for CORS auth
      registry.antMatchers(HttpMethod.OPTIONS, "/**").permitAll();

      registry.anyRequest().authenticated();
    }
    http.csrf().disable();


    if (!azureAuthDisabled) {
      http.addFilterBefore(aadAuthFilter, UsernamePasswordAuthenticationFilter.class);
    }
    http.addFilterAfter(new FlowableAuthFilter(), UsernamePasswordAuthenticationFilter.class);


  }

}

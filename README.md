# flowhub

## running


### run postgres db:
```
etc/docker/pg/run.sh
#careful. will remove existing 'flowhub-postgres' container
```

### run flowhub:
```
./gradlew bootRun
#OR
./gradlew jar && java -jar build/libs/*.jar
#OR 
etc/docker/app/run.sh
```


### see also script for running flowable stack
```
etc/docker/flowable.sh
```
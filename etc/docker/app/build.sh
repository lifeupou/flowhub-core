#!/bin/bash
cd `dirname $0`

repo="kodality"
name="flowhub"

gradle build -p ../../../flowhub && cp ../../../flowhub/build/libs/flowhub-1.0.0-SNAPSHOT.jar flowhub.jar
docker build -t $repo/$name .
rm flowhub.jar

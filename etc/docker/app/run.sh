#!/bin/bash
cd `dirname $0`

repo="kodality"
name="flowhub"

docker rm -vf $name
./build.sh || exit 1

docker run -d --name=$name \
  -p 8080:8080 \
  --restart=unless-stopped \
  --link flowhub-postgres \
  $repo/$name
  
docker exec -ti $name sh -c 'echo "
spring:
  datasource:
    hikari:
      jdbc-url: jdbc:postgresql://flowhub-postgres:5432/flowhub
      username: flowhub
      password: test
" > /config/application-default.yml'



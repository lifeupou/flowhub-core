
docker rm -vf flowable
docker run -d \
  --name=flowable \
  -p 9080:8080 \
  --link flowhub-postgres \
  -e SPRING_DATASOURCE_DRIVER-CLASS-NAME='org.postgresql.Driver' \
  -e SPRING_DATASOURCE_URL='jdbc:postgresql://flowhub-postgres:5432/flowhub' \
  -e SPRING_DATASOURCE_USERNAME='flowhub' \
  -e SPRING_DATASOURCE_PASSWORD='test' \
  -e FLOWABLE_COMMON_APP_IDM-URL='http://localhost:8080/flowable-idm' \
  -e FLOWABLE_COMMON_APP_IDM-REDIRECT-URL='http://localhost:9080/flowable-idm' \
  -e FLOWABLE_COMMON_APP_IDM-ADMIN.USER=admin \
  -e FLOWABLE_COMMON_APP_IDM-ADMIN.PASSOWRD=test \
  -e FLOWABLE_MODELER_APP_DEPLOYMENT-API-URL='http://localhost:8080/flowable-task/process-api' \
  --restart=unless-stopped \
  kodality/flowable

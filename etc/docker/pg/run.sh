#!/bin/bash 
cd `dirname $0`
./build.sh

repo="flowhub"
name="postgres"

docker rm -vf $repo-$name
docker run -d \
  -e TZ=Europe/Tallinn \
  -e POSTGRES_USER=postgres \
  -e POSTGRES_PASSWORD=postgres \
  --restart=unless-stopped \
  --name $repo-$name \
  -p 5432:5432 \
  $repo/$name


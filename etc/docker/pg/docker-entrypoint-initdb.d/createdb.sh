#!/bin/bash
psql -d "$POSTGRES_DB" -U "$POSTGRES_USER" -w -f /docker-entrypoint-initdb.d/create_db_flowhub.psql
psql -d "flowhub" -U "$POSTGRES_USER" -w -f /docker-entrypoint-initdb.d/init_db_flowhub.psql


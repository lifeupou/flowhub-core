--liquibase formatted sql

--changeset daniel:valueset_entity
CREATE SEQUENCE s_valueset_entity;
create table valueset_entity (
  id serial primary key,
  valueset text not null references valueset(id),
  code text not null,
  valid tstzrange,
  parent text,
  attributes jsonb,
  sys_created jsonb,
  sys_modified jsonb
);

create index on valueset_entity(valueset);
--rollback drop table valueset_entity;




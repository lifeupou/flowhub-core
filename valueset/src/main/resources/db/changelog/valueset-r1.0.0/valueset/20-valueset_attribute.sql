--liquibase formatted sql

--changeset daniel:valueset_attribute
create table valueset_attribute (
  id text not null,
  valueset text not null references valueset(id),
  names jsonb not null default '{}'::jsonb,
  datatype text,
  parameters jsonb,
  default_value jsonb,
  ordering smallint not null default 300,
  editable boolean,
  required boolean,
  constraint valueset_attribute_unique unique (valueset, id)
);

create index on valueset_attribute(valueset);
--rollback drop table valueset_attribute;




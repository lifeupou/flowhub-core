--liquibase formatted sql

--changeset daniel:valueset
create table valueset (
  id text not null primary key,
  names jsonb not null default '{}'::jsonb,
  title jsonb not null default '{}'::jsonb,
  behavior text,
  notes jsonb,
  sys_created jsonb,
  sys_modified jsonb
);

create index on valueset(id);
--rollback drop table valueset;





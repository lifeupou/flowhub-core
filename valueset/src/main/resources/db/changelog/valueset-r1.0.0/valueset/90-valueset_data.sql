--liquibase formatted sql

--changeset daniel:valueset_bpm-form
INSERT INTO valueset(id, names, title, behavior, notes)
	VALUES ('bpm-form','{}'::jsonb,'{}'::jsonb,'unique','{"hidden":["valid_from","valid_to"]}');
INSERT INTO valueset_attribute(id, valueset, names, datatype, parameters, default_value, ordering, editable, required)
	VALUES ('names','bpm-form','{}'::jsonb,'translations','{}'::jsonb,'{}'::jsonb,0,false,false);
INSERT INTO valueset_attribute(id, valueset, names, datatype, parameters, default_value, ordering, editable, required)
	VALUES ('form-url','bpm-form','{"en":"form url"}'::jsonb,'text','{}'::jsonb,'{}'::jsonb,1,false,false);
--rollback 

--changeset daniel:valueset_entities_bpm-form
INSERT INTO valueset_entity(valueset, code, valid, parent, attributes)
	VALUES ('bpm-form', 'dynform', null, null, '{"names" : { "en" : "dynamic form"}, "form-url" : "flowhub/task/form/{task_id}"}'::jsonb);
--rollback 



/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonUtil {

  private static ObjectMapper mapper;

  public static synchronized ObjectMapper getObjectMapper() {
    if (mapper == null) {
      mapper = new ObjectMapper();
      mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
      mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
      mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
      JavaTimeModule javaTimeModule = new JavaTimeModule();
      javaTimeModule.addDeserializer(LocalDateTime.class,
                                     new LocalDateTimeDeserializer(DateTimeFormatter.ISO_DATE_TIME));
      javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(DateTimeFormatter.ISO_DATE_TIME));
      mapper.registerModule(javaTimeModule);
    }

    return mapper;
  }

  public static String toJson(Object object) {
    try {
      return object == null ? null : getObjectMapper().writeValueAsString(object);
    } catch (JsonProcessingException e) {
      throw new RuntimeException("error parsing json: " + e.getMessage());
    }
  }

  public static <T> T fromJson(String json, Class<T> clazz) {
    try {
      return json == null ? null : getObjectMapper().readValue(json, clazz);
    } catch (JsonProcessingException e) {
      throw new RuntimeException("error parsing json: " + e.getMessage());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static <T> T fromJson(String json, JavaType type) {
    try {
      return json == null ? null : getObjectMapper().readValue(json, type);
    } catch (JsonProcessingException e) {
      throw new RuntimeException("error parsing json: " + e.getMessage());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @SuppressWarnings("unchecked")
  public static Map<String, Object> toMap(String json) {
    if (json == null) {
      return null;
    }
    try {
      return new ObjectMapper().readValue(json, HashMap.class);
    } catch (JsonProcessingException e) {
      throw new RuntimeException("error parsing json: " + e.getMessage());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static JavaType getListType(Class<?> clazz) {
    return getObjectMapper().getTypeFactory().constructCollectionType(List.class, clazz);
  }

  public static JavaType getMapType(Class<?> valueClass) {
    return getObjectMapper().getTypeFactory().constructMapType(Map.class, Object.class, valueClass);
  }
}

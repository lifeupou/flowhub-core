/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateUtil {
  public static final String DATE = "yyyy-MM-dd";
  public static final String ISO_DATETIME = "yyyy-MM-dd'T'HH:mm:ssX";
  public static final String RFC_DATETIME = "yyyy-MM-dd'T'HH:mm:ssZ";
  public static final String ISO_DATETIME_MILLIS = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
  public static final String DATETIME_FOR_FILTER = "yyyy-MM-dd'T'HH:mm:ssXXX";
  public static final String DATETIME_DISPALY_FORMAT = "dd.MM.YYYY - HH:mm";
  public static final String TIMESTAMP_PG = "yyyy-MM-dd HH:mm:ssX";

  public static String reformat(String dateString, String pattern) {
    return new SimpleDateFormat(pattern).format(parse(dateString));
  }

  public static Date parse(String date) {
    return parse(date, TIMESTAMP_PG, ISO_DATETIME_MILLIS, ISO_DATETIME, DATE)
        .orElseThrow(() -> new IllegalArgumentException("Cannot parse date: " + date));
  }

  public static Optional<Date> parse(String date, String... formats) {
    if (date == null) {
      return null;
    }
    for (String format : formats) {
      try {
        return Optional.of(new SimpleDateFormat(format).parse(date));
      } catch (ParseException e) {
        // next try
      }
    }
    return Optional.empty();
  }

  public static LocalDateTime toLocalDateTime(Date date) {
    return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
  }

}

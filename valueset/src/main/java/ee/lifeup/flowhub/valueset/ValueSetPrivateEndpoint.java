/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset;

import com.fasterxml.jackson.core.JsonProcessingException;
import ee.lifeup.flowhub.util.JsonUtil;
import ee.lifeup.flowhub.valueset.model.Entity;
import ee.lifeup.flowhub.valueset.model.EntityExport;
import ee.lifeup.flowhub.valueset.model.ValueSet;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import java.io.IOException;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Validated
@RestController
@RequestMapping("/private/valueset")
@AllArgsConstructor
public class ValueSetPrivateEndpoint {
  private final ValueSetService valueSetService;
  private final ValueSetEntityService entityService;
  private final ValueSetCsvService csvService;

  @GetMapping()
  public List<ValueSet> search() {
    return valueSetService.search();
  }

  @GetMapping("/{valueSetId:.+}")
  public ValueSet get(@PathVariable String valueSetId,
                      @RequestParam(name = "entities", defaultValue = "tree") String repr) {
    if (repr.equals("flat")) {
      return valueSetService.loadFlat(valueSetId);
    }
    return valueSetService.loadTree(valueSetId);
  }

  @PutMapping("/{valueSetId:.+}")
  public ResponseEntity<?> save(@PathVariable @NotNull String valueSetId, @RequestBody @Valid ValueSet vs) {
    if (vs.getId() == null) {
      vs.setId(valueSetId);
    }
    if (!vs.getId().equals(valueSetId)) {
      throw new ApiException(400, "id mismatch");
    }
    valueSetService.save(vs);
    return ResponseEntity.noContent().build();
  }

  @PostMapping("/{valueSetId}/entities")
  public ResponseEntity<?> createEntity(@PathVariable String valueSetId, @RequestBody @Valid Entity entity) {
    entity.setId(null);
    entityService.save(valueSetId, entity);
    return ResponseEntity.noContent().build();
  }

  @PutMapping("/{valueSetId}/entities/{id:.+}")
  public ResponseEntity<?> updateEntity(@PathVariable String valueSetId,
                                        @PathVariable Long id,
                                        @RequestBody @Valid Entity entity) {
    if (entity.getId() == null) {
      entity.setId(id);
    }
    if (!entity.getId().equals(id)) {
      throw new ApiException(400, "id mismatch");
    }
    entityService.save(valueSetId, entity);
    return ResponseEntity.noContent().build();
  }

  @DeleteMapping("/{valueSetId}")
  public ResponseEntity<?> deleteValueSet(@PathVariable String valueSetId) {
    entityService.delete(valueSetId);
    return ResponseEntity.noContent().build();
  }

  @DeleteMapping("/{valueSetId}/entities/{id:.+}")
  public ResponseEntity<?> deleteEntity(@PathVariable String valueSetId, @PathVariable Long id) {
    entityService.delete(valueSetId, id);
    return ResponseEntity.noContent().build();
  }

  @GetMapping("/{valueSetId}/export")
  public ResponseEntity<?> exportValueSet(@PathVariable String valueSetId) {
    ValueSet valueSet = valueSetService.loadFlat(valueSetId);
    //valueSet.setEntities(null);
    String json = toPrettyJson(valueSet);
    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + valueSet.getId() + ".json")
        .contentType(MediaType.APPLICATION_JSON)
        .contentLength(json.getBytes().length)
        .body(json);
  }

  @GetMapping("/{valueSetId}/entities/export")
  public ResponseEntity<?> exportEntities(@PathVariable String valueSetId) {
    ValueSet valueSet = valueSetService.loadFlat(valueSetId);
    List<EntityExport> entities =
        valueSet.getEntities().stream().map(e -> new EntityExport(valueSetId, e)).collect(toList());
    String json = toPrettyJson(entities);
    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + valueSet.getId() + ".json")
        .contentType(MediaType.APPLICATION_JSON)
        .contentLength(json.getBytes().length)
        .body(json);
  }

  @GetMapping(path = "/{valueSetId}/entities/export", produces = "application/csv")
  public ResponseEntity<?> exportEntitiesCsv(@PathVariable String valueSetId) {
    ValueSet valueSet = valueSetService.loadFlat(valueSetId);
    List<Entity> entities = valueSet.getEntities();
    String csv = csvService.compose(entities, valueSet.getAttributes()).toString();

    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + valueSet.getId() + ".csv")
        .contentType(MediaType.parseMediaTypes("application/csv").get(0))
        .contentLength(csv.getBytes().length)
        .body(csv);
  }

  @GetMapping("/{valueSetId}/entities/{id}/export")
  public ResponseEntity<?> exportEntities(@PathVariable String valueSetId, @PathVariable Long id) {
    ValueSet valueSet = valueSetService.loadFlat(valueSetId);
    EntityExport entity = valueSet.getEntities()
        .stream()
        .filter(e -> id.equals(e.getId()))
        .findFirst()
        .map(e -> new EntityExport(valueSetId, e))
        .orElse(null);
    String json = toPrettyJson(entity);
    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + valueSet.getId() + "." + id + ".json")
        .contentType(MediaType.APPLICATION_JSON)
        .contentLength(json.getBytes().length)
        .body(json);
  }

  @PostMapping(path = "/{valueSetId}/entities/import")
  public ResponseEntity<?> importEntities(@PathVariable String valueSetId, @RequestPart("file") MultipartFile csv)
      throws IOException {
    ValueSet valueSet = valueSetService.loadFlat(valueSetId);
    List<Entity> entities = csvService.parse(valueSet.getAttributes(), csv.getInputStream());
    entityService.merge(valueSetId, entities);
    return ResponseEntity.noContent().build();
  }

  private String toPrettyJson(Object o) {
    try {
      return JsonUtil.getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(o);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }
}

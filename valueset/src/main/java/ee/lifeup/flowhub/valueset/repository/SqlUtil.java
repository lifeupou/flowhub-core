/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset.repository;

import ee.lifeup.flowhub.valueset.ApiException;
import ee.lifeup.flowhub.valueset.util.range.DateRange;
import org.postgresql.util.PGobject;
import org.postgresql.util.PSQLException;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;

public class SqlUtil {

  public static PGobject tstzrange(DateRange dr) {
    PGobject gobject = new PGobject();
    gobject.setType("tstzrange");
    try {
      gobject.setValue(dr == null ? null : dr.asString());
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
    return gobject;
  }

  public static void handleException(DataAccessException e) {
    if (e.getCause() instanceof PSQLException) {
      PSQLException exception = (PSQLException) e.getCause();
      switch (exception.getSQLState()) {
      case "23505":
        throw new ApiException(409, e.getCause().getMessage());
      case "23P01":
        throw new ApiException(409, e.getCause().getMessage());
      }
    }
    throw e;
  }
}

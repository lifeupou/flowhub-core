/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset.repository;

import ee.lifeup.flowhub.util.JsonUtil;
import ee.lifeup.flowhub.valueset.model.ValueSetAttribute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Repository
public class ValueSetAttributeRepository {
  private final ValueSetAttributeRowMapper mapper = new ValueSetAttributeRowMapper();
  private final JdbcTemplate jdbcTemplate;

  @Autowired
  public ValueSetAttributeRepository(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public List<ValueSetAttribute> load(String valueSetId) {
    String sql = "SELECT * FROM valueset_attribute WHERE valueset = ? order by ordering";
    return jdbcTemplate.query(sql, new Object[] { valueSetId }, mapper);
  }

  public void merge(String valueSetId, List<ValueSetAttribute> attributes) {
    // too lazy to merge. should not be a problem.
    jdbcTemplate.update("DELETE FROM valueset_attribute WHERE valueset = ?", valueSetId);
    if (attributes == null || attributes.isEmpty()) {
      return;
    }
    attributes.forEach(a -> a.setValueset(valueSetId));
    String sql =
        "INSERT INTO valueset_attribute (id, valueset, names, datatype, parameters, default_value, editable, required, ordering)"
            + " VALUES (?,?,?::jsonb,?,?::jsonb,?::jsonb,?,?,?)";

    AtomicInteger order = new AtomicInteger(0);
    List<Object[]> args = attributes.stream()
        .map(a -> new Object[] { a.getId(), a.getValueset(), JsonUtil.toJson(a.getNames()), a.getDatatype(),
                                 JsonUtil.toJson(a.getParameters()),
                                 JsonUtil.toJson(Collections.singletonMap("value", a.getDefaultValue())),
                                 a.isEditable(), a.isRequired(), order.getAndIncrement() })
        .collect(toList());
    jdbcTemplate.batchUpdate(sql, args);
  }

  private static class ValueSetAttributeRowMapper
      implements org.springframework.jdbc.core.RowMapper<ValueSetAttribute> {
    @Override
    public ValueSetAttribute mapRow(ResultSet rs, int rowNum) throws SQLException {
      ValueSetAttribute a = new ValueSetAttribute();
      a.setId(rs.getString("id"));
      a.setValueset(rs.getString("valueset"));
      a.setNames(toMap(rs.getString("names")));
      a.setDatatype(rs.getString("datatype"));
      a.setParameters(JsonUtil.toMap(rs.getString("parameters")));
      a.setDefaultValue(JsonUtil.toMap(rs.getString("default_value")).get("value"));
      a.setEditable(rs.getBoolean("editable"));
      a.setRequired(rs.getBoolean("required"));
      return a;
    }

    private Map<String, String> toMap(String json) {
      if (json == null) {
        return null;
      }
      Map<String, Object> map = JsonUtil.toMap(json);
      return map.keySet().stream().collect(Collectors.toMap(k -> k, k -> (String) map.get(k)));
    }
  }

}

/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset;

import ee.lifeup.flowhub.valueset.model.*;
import ee.lifeup.flowhub.valueset.repository.ValueSetAttributeRepository;
import ee.lifeup.flowhub.valueset.repository.ValueSetEntityRepository;
import ee.lifeup.flowhub.valueset.repository.ValueSetRepository;
import ee.lifeup.flowhub.valueset.rules.AttributeValidationService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.*;

@Service
@AllArgsConstructor
public class ValueSetEntityService {
  private final ValueSetRepository valueSetRepository;
  private final ValueSetEntityRepository entityRepository;
  private final ValueSetAttributeRepository attributeRepository;
  private final AttributeValidationService validationService;

  public List<Entity> load(String vsId) {
    return entityRepository.load(vsId);
  }

  public Entity load(String vsId, String entity) {
    return entityRepository.load(vsId, entity);
  }

  public boolean exists(String valueSetId, String entity) {
    return entityRepository.exists(valueSetId, entity);
  }

  @Transactional
  public void save(String valueSetId, List<Entity> entities) {
    ValueSet vs = valueSetRepository.load(valueSetId);
    List<ValueSetAttribute> attributes = attributeRepository.load(valueSetId);
    entities.forEach(d -> {
      if (StringUtils.isEmpty(d.getCode())) {
        throw new ApiException(400, "entity code required");
      }
      validateAttributes(d, attributes);
      validateCodeOverlaps(vs, d);
      if (d.getId() != null) {
        if (entityRepository.update(valueSetId, d)) {
          return;
        }
      }
      entityRepository.create(valueSetId, d);
    });
  }

  /**
   * will remove all existing entities by code in list. then create
   */
  @Transactional
  public void merge(String valueSetId, List<Entity> entities) {
    entities.stream().map(e -> e.getCode()).distinct().forEach(code -> {
      entityRepository.deleteAll(valueSetId, code);
    });
    save(valueSetId, entities);
  }

  @Transactional
  public void merge(List<EntityExport> entities) {
    Map<String, List<EntityExport>> vsEntities = entities.stream().collect(groupingBy(e -> e.getValueSetId()));
    vsEntities.forEach((vs, e) -> merge(vs, e.stream().map(ee -> (Entity) ee).collect(toList())));
  }

  @Transactional
  public void save(String valueSetId, Entity entity) {
    save(valueSetId, Collections.singletonList(entity));
  }

  private void validateCodeOverlaps(ValueSet vs, Entity entity) {
    switch (vs.getBehavior()) {
      case ValueSetBehavior.unique:
        if (entityRepository.overlaps(vs.getId(), entity.getCode(), entity.getId(), null)) {
          throw new ApiException(400, "code already exists");
        }
        return;
      case ValueSetBehavior.temporal:
        if (entityRepository.overlaps(vs.getId(), entity.getCode(), entity.getId(), entity.getValid())) {
          throw new ApiException(400, "periods overlap");
        }
        return;
      default:
        throw new ApiException(400, "unsupported behavior");
    }
  }

  @Transactional
  public void delete(String valueSetId, Long id) {
    entityRepository.load(valueSetId).stream().filter(d -> d.getId().equals(id)).findFirst().ifPresent(entity -> {
      if (entity.getValid().getLower() != null && entity.getValid().getLower().before(new Date())) {
        //XXX is this check needed?
        throw new ApiException(400, "entity is active");
      }
      entityRepository.delete(valueSetId, id);
    });
  }

  private void validateAttributes(Entity entity, List<ValueSetAttribute> attributes) {
    if (entity.getAttributes() == null) {
      return;
    }
    Map<String, ValueSetAttribute> defs = attributes.stream().collect(toMap(a -> a.getId(), a -> a));
    entity.getAttributes().forEach((key, attr) -> {
      if (!defs.containsKey(key)) {
        throw new ApiException(400, "attribute '" + key + "' not defined");
      }
      validationService.validate(attr, defs.get(key));
    });
  }

  public void delete(String valueSetId) {
    valueSetRepository.delete(valueSetId);
  }
}

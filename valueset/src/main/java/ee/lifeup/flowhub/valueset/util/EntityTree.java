/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset.util;

import ee.lifeup.flowhub.valueset.model.Entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EntityTree {

  public static List<Entity> buildTree(List<Entity> flat) {
    return buildTree(flat.stream());
  }

  public static List<Entity> buildTree(Stream<Entity> flat) {
    Map<String, List<Entity>> map = group(flat);
    List<Entity> root = map.getOrDefault(null, new ArrayList<>());
    root.forEach(c -> expand(c, map));
    return root;
  }

  public static void buildTree(Entity root, List<Entity> all) {
    expand(root, group(all.stream()));
  }

  public static Stream<Entity> flat(Stream<Entity> entities) {
    return entities.flatMap(e -> {
      Stream<Entity> result = Stream.of(e);
      if (e.getChildren() != null) {
        Stream<Entity> children = flat(e.getChildren().stream().map(ee -> {
          ee.setParent(e.getCode());
          return ee;
        }));
        result = Stream.concat(result, children);
        e.setChildren(null);
      }
      return result;
    });
  }

  private static void expand(Entity mi, Map<String, List<Entity>> map) {
    List<Entity> children = map.get(mi.getCode());
    if (children == null) {
      return;
    }
    mi.setChildren(children);
    children.forEach(c -> expand(c, map));
  }

  private static Map<String, List<Entity>> group(Stream<Entity> flat) {
    Map<String, List<Entity>> r = flat.collect(groupingBy(Entity::getParent));
    r.values().forEach(rr -> rr.forEach(rrr -> rrr.setParent(null)));
    return r;
  }

  /**
   * because #Collectors.groupingBy does not eat null keys
   */
  private static Collector<Entity, ?, Map<String, List<Entity>>> groupingBy(Function<Entity, String> keyFn) {
    return Collectors.toMap(keyFn, x -> {
      List<Entity> list = new ArrayList<>();
      list.add(x);
      return list;
    }, (left, right) -> {
      left.addAll(right);
      return left;
    }, HashMap::new);
  }

}

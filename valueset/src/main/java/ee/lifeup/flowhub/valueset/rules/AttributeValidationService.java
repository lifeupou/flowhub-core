/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset.rules;

import ee.lifeup.flowhub.valueset.ApiException;
import ee.lifeup.flowhub.valueset.model.AttributeDatatype;
import ee.lifeup.flowhub.valueset.model.ValueSetAttribute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
@SuppressWarnings(value = { "rawtypes", "unchecked" })
public class AttributeValidationService {
  private final Map<String, RuleValidator> validators;

  @Autowired
  public AttributeValidationService(Map<String, RuleValidator> validators) {
    this.validators = validators;
  }

  private static final Map<String, Function<Object, Boolean>> types;
  static {
    types = new HashMap<>();
    types.put(AttributeDatatype.text, (o) -> o instanceof String);
    types.put(AttributeDatatype.date, (o) -> o instanceof Date);
    types.put(AttributeDatatype.number, (o) -> o instanceof Integer || o instanceof Double);
    types.put(AttributeDatatype.booljan, (o) -> o instanceof Boolean);
    types.put(AttributeDatatype.translations, (o) -> o instanceof Map);
    types.put(AttributeDatatype.valueset, (o) -> o instanceof String);
  }

  public void validate(Object entityValue, ValueSetAttribute attribute) {
    if (attribute.isRequired() && entityValue == null) {
      throw new ApiException(400, attribute.getId() + " is required");
    }
    if (!validateType(entityValue, attribute.getDatatype())) {
      throw new ApiException(400, entityValue.getClass() + " is not allowed for datatype " + attribute.getDatatype());
    }
    if (attribute.getParameters() != null) {
      attribute.getParameters().forEach((param, args) -> {
        if (!validateRule(entityValue, param, args)) {
          throw new ApiException(400, "rule failed: " + param);
        }
      });
    }
  }

  private boolean validateType(Object value, String datatype) {
    return types.get(datatype).apply(value);
  }

  private boolean validateRule(Object value, String rule, Object args) {
    String key = rule + "Validator";
    if (!validators.containsKey(key)) {
      return true;
    }
    return validators.get(key).validate(value, args);
  }

}

/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset.repository;

import ee.lifeup.flowhub.util.JsonUtil;
import ee.lifeup.flowhub.valueset.model.ValueSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class ValueSetRepository {
  private final ValueSetRowMapper mapper = new ValueSetRowMapper();
  private final JdbcTemplate jdbcTemplate;

  @Autowired
  public ValueSetRepository(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public List<ValueSet> search() {
    String sql = "SELECT * FROM valueset";
    return jdbcTemplate.query(sql, mapper);
  }

  public ValueSet load(String id) {
    String sql = "SELECT * FROM valueset WHERE id = ?";
    return jdbcTemplate.queryForObject(sql, new Object[] { id }, mapper);
  }

  public void save(ValueSet vs) {
    if (exists(vs.getId())) {
      update(vs);
    } else {
      insert(vs);
    }
  }

  private void insert(ValueSet vs) {
    String sql = "INSERT INTO valueset (names, title, behavior, notes, id) VALUES (?::jsonb,?::jsonb,?,?::jsonb,?)";
    Object[] p = { JsonUtil.toJson(vs.getNames()), JsonUtil.toJson(vs.getTitle()), vs.getBehavior(),
                   JsonUtil.toJson(vs.getNotes()), vs.getId() };
    jdbcTemplate.update(sql, p);
  }

  private void update(ValueSet vs) {
    String sql = "UPDATE valueset SET names = ?::jsonb, title = ?::jsonb, notes = ?::jsonb WHERE id = ?";
    Object[] p =
        { JsonUtil.toJson(vs.getNames()), JsonUtil.toJson(vs.getTitle()), JsonUtil.toJson(vs.getNotes()), vs.getId() };
    jdbcTemplate.update(sql, p);
  }

  private boolean exists(String id) {
    String sql = "SELECT EXISTS (SELECT 1 FROM valueset WHERE id = ?)";
    return jdbcTemplate.queryForObject(sql, new Object[] { id }, Boolean.class);
  }

  public void delete(String valueSetId) {
    jdbcTemplate.update("DELETE FROM valueset_entity WHERE valueset = ?", valueSetId);
    jdbcTemplate.update("DELETE FROM valueset_attribute WHERE valueset = ?", valueSetId);
    jdbcTemplate.update("DELETE FROM valueset WHERE id = ?", valueSetId);
  }

  private static class ValueSetRowMapper implements RowMapper<ValueSet> {
    @Override
    public ValueSet mapRow(ResultSet rs, int rowNum) throws SQLException {
      ValueSet vs = new ValueSet();
      vs.setId(rs.getString("id"));
      vs.setNames(toMap(rs.getString("names")));
      vs.setTitle(toMap(rs.getString("title")));
      vs.setBehavior(rs.getString("behavior"));
      vs.setNotes(toMap(rs.getString("notes")));
      return vs;
    }

    @SuppressWarnings("unchecked")
    private <T> Map<String, T> toMap(String json) {
      if (json == null) {
        return null;
      }
      Map<String, Object> map = JsonUtil.toMap(json);
      return map.keySet().stream().collect(Collectors.toMap(k -> k, k -> (T) map.get(k)));
    }
  }

}

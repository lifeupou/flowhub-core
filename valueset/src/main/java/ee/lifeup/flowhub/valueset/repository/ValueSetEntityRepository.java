/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset.repository;

import ee.lifeup.flowhub.util.JsonUtil;
import ee.lifeup.flowhub.valueset.model.Entity;
import ee.lifeup.flowhub.valueset.util.range.DateRange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ValueSetEntityRepository {
  private final JdbcTemplate jdbcTemplate;

  @Autowired
  public ValueSetEntityRepository(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public List<Entity> load(String valueSetId) {
    String sql = "SELECT * FROM valueset_entity WHERE valueset = ? order by id";
    return jdbcTemplate.query(sql, new EntityRowMapper(), valueSetId);
  }

  public Entity load(String valueSetId, String code) {
    String sql = "SELECT * FROM valueset_entity WHERE valueset = ? and code = ?";
    try {
      return jdbcTemplate.queryForObject(sql, new EntityRowMapper(), valueSetId, code);
    } catch (EmptyResultDataAccessException ignored) {
      return null;
    }
  }

  public void create(String valueSetId, Entity entity) {
    try {
      String sql =
          "INSERT INTO valueset_entity (code, parent, attributes, valid, valueset) VALUES (?,?,?::jsonb,?::tstzrange,?)";
      jdbcTemplate.update(sql,
          entity.getCode(),
          entity.getParent(),
          JsonUtil.toJson(entity.getAttributes()),
          SqlUtil.tstzrange(entity.getValid()),
          valueSetId);
    } catch (DataAccessException e) {
      SqlUtil.handleException(e);
    }
  }

  public boolean update(String valueSetId, Entity entity) {
    String sql =
        "UPDATE valueset_entity SET parent = ?, attributes = ?::jsonb, valid = ?::tstzrange WHERE id = ? AND valueset = ?";
    return
    jdbcTemplate.update(sql,
        entity.getParent(),
        JsonUtil.toJson(entity.getAttributes()),
        SqlUtil.tstzrange(entity.getValid()),
        entity.getId(),
        valueSetId) > 0;

  }

  public void delete(String valueSetId, Long entityId) {
    String sql = "DELETE FROM valueset_entity WHERE id = ? AND valueset = ? and lower(valid) > now()";
    jdbcTemplate.update(sql, entityId, valueSetId);
  }

  public void deleteAll(String valueSetId, String entityCode) {
    String sql = "DELETE FROM valueset_entity WHERE code = ? AND valueset = ?";
    jdbcTemplate.update(sql, entityCode, valueSetId);
  }

  public boolean exists(String valueSetId, String entity) {
    String sql = "SELECT count(1) FROM valueset_entity WHERE code = ? AND valueset = ?";
    return jdbcTemplate.queryForObject(sql, Integer.class, entity, valueSetId) > 0;
  }

  public boolean overlaps(String valueSetId, String entity, Long excludeId, DateRange dateRange) {
    List<Object> args = new ArrayList<>();
    String sql = "SELECT count(1) FROM valueset_entity WHERE code = ? AND valueset = ? ";
    args.add(entity);
    args.add(valueSetId);
    if (excludeId != null) {
      sql += " AND id != ?";
      args.add(excludeId);
    }
    if (dateRange != null) {
      sql += " AND valid && ?::tstzrange";
      args.add(SqlUtil.tstzrange(dateRange));
    }
    return jdbcTemplate.queryForObject(sql, Integer.class, args.toArray()) > 0;
  }

  private static class EntityRowMapper implements RowMapper<Entity> {
    @Override
    public Entity mapRow(ResultSet rs, int rowNum) throws SQLException {
      Entity d = new Entity();
      d.setId(rs.getLong("id"));
      d.setCode(rs.getString("code"));
      d.setParent(rs.getString("parent"));
      d.setAttributes(JsonUtil.toMap(rs.getString("attributes")));
      d.setValid(new DateRange(rs.getString("valid")));
      return d;
    }
  }

}

/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset.util.range;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateRange extends Range<Date> {
  public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ssX";

  public DateRange() {
    //
  }

  public DateRange(String range) {
    super(range);
  }

  public DateRange(Date from, Date to) {
    setLower(from);
    setUpper(to);
    setLowerInclusive(true);
    setUpperInclusive(true);
  }

  @Override
  protected Date parse(String input) {
    try {
      return new SimpleDateFormat(DATE_FORMAT).parse(input);
    } catch (ParseException e) {
      throw new IllegalArgumentException("Invalid date format: " + input);
    }
  }

  @Override
  protected String format(Date endpoint) {
    return new SimpleDateFormat(DATE_FORMAT).format(endpoint);
  }

  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  @Override
  public Date getLower() {
    return super.getLower();
  }

  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  @Override
  public Date getUpper() {
    return super.getUpper();
  }

  @JsonIgnore
  @Override
  public boolean isLowerInclusive() {
    return super.isLowerInclusive();
  }

  @JsonIgnore
  @Override
  public boolean isUpperInclusive() {
    return super.isUpperInclusive();
  }

}

/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset;

import ee.lifeup.flowhub.util.JsonUtil;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ApiExceptionHandler {

  @org.springframework.web.bind.annotation.ExceptionHandler(ApiException.class)
  @ResponseBody
  private ResponseEntity<Object> handleApiException(ApiException ex, HttpServletRequest request) {
    HttpStatus status = HttpStatus.valueOf(ex.getHttpStatus());
    return new ResponseEntity<>(JsonUtil.toJson(geErrorMapResponse(ex.getCode(), ex.getLocalizedMessage())), status);
  }

  /**
   * Handles every {@link Exception} throwing.
   *
   * @param ex Any exception.
   * @return response entity with exception cause message as body.
   */
  @ExceptionHandler(Exception.class)
  public ResponseEntity<?> handleAllExceptions(Exception ex) {
    Throwable cause = ex.getCause();
    String message;
    if (cause != null) {
      message = cause.getLocalizedMessage();
    } else if (ex.getMessage() != null) {
      message = ex.getLocalizedMessage();
    } else {
      message = ex.toString();
    }
    return new ResponseEntity<>(geErrorMapResponse(message, String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value())), HttpStatus.INTERNAL_SERVER_ERROR);
  }

  private Map<String, String> geErrorMapResponse(String code, String message) {
    Map<String, String> resp = new HashMap<>(2);
    resp.put("code", code);
    resp.put("message", message);
    return resp;
  }

}

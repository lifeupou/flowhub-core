/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset;

import ee.lifeup.flowhub.valueset.model.*;
import ee.lifeup.flowhub.valueset.repository.ValueSetAttributeRepository;
import ee.lifeup.flowhub.valueset.repository.ValueSetRepository;
import ee.lifeup.flowhub.valueset.rules.AttributeValidationService;
import ee.lifeup.flowhub.valueset.util.EntityTree;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@AllArgsConstructor
public class ValueSetService {
  private final ValueSetRepository valueSetRepository;
  private final ValueSetAttributeRepository valueSetAttributeRepository;
  private final AttributeValidationService attributeValidationService;
  private final ValueSetEntityService entityService;

  public List<ValueSet> search() {
    return valueSetRepository.search();
  }

  // @Cacheable("valueset")
  public ValueSet loadFlat(String id) {
    return search().stream().filter(vs -> vs.getId().equals(id)).findFirst().map(vs -> {
      vs.setEntities(entityService.load(vs.getId()));
      vs.setAttributes(valueSetAttributeRepository.load(vs.getId()));
      return vs;
    }).orElseThrow(() -> new ApiException(404, "valueset not found"));
  }

  public ValueSet loadTree(String id) {
    ValueSet vs = loadFlat(id);
    vs.setEntities(EntityTree.buildTree(vs.getEntities()));
    return vs;
  }

  @Transactional
  public void save(ValueSet vs) {
    Validate.notNull(vs.getId());
    if (vs.getAttributes() == null) {
      vs.setAttributes(new ArrayList<>());
    }
    valueSetRepository.save(vs);

    if (!Arrays.asList(ValueSetBehavior.temporal, ValueSetBehavior.unique).contains(vs.getBehavior())) {
      throw new ApiException(400, vs.getBehavior() + " is not supported");
    }
    vs.getAttributes().forEach(this::validateAttribute);
    valueSetAttributeRepository.merge(vs.getId(), vs.getAttributes());

    if(vs.getEntities() != null && !vs.getEntities().isEmpty()){
      entityService.merge(vs.getId(), vs.getEntities());
    }
  }

  private void validateAttribute(ValueSetAttribute attr) {
    if (!AttributeDatatype.all.contains(attr.getDatatype())) {
      throw new ApiException(400, attr.getDatatype() + " is not allowed");
    }
    List<String> allowedParameters = AttributeParameter.params.get(attr.getDatatype());
    if (attr.getParameters() != null) {
      attr.getParameters().keySet().forEach(param -> {
        if (!allowedParameters.contains(param)) {
          throw new ApiException(400, param + " not allowed for " + attr.getDatatype() + " datatype");
        }
      });
    }
    if (attr.getDefaultValue() != null) {
      attributeValidationService.validate(attr.getDefaultValue(), attr);
    }
  }

  public boolean exists(String valueSetId, String entity) {
    ValueSet vs = valueSetRepository.load(valueSetId);
    if (vs == null) {
      return false;
    }
    return entityService.exists(valueSetId, entity);
  }

}

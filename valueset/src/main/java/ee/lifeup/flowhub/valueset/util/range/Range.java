/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset.util.range;

import org.apache.commons.lang3.ObjectUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Range<T extends Comparable<T>> {
  private T lower;
  private T upper;
  private boolean lowerInclusive = true;
  private boolean upperInclusive = true;

  private final static String INFINITY = "infinity";
  private static final Pattern pattern = Pattern.compile("(\\[|\\()\"?(.*)\"?,\"?(.*)?\"?(\\]|\\))");

  public Range() {
    //
  }

  public Range(String range) {
    fromString(range);
  }

  protected abstract T parse(String input);

  protected abstract String format(T endpoint);

  public String asString() {
    String lowerSign = getLower() != null && isLowerInclusive() ? "[" : "(";
    String upperSign = getUpper() != null && isUpperInclusive() ? "]" : ")";
    String lowerValue = getLower() == null ? "" : format(getLower());
    String upperValue = getUpper() == null ? "" : format(getUpper());
    return lowerSign + lowerValue + "," + upperValue + upperSign;
  }

  public boolean contains(T value) {
    if (value == null) {
      return false;
    }
    int l = ObjectUtils.compare(getLower(), value, false);
    int u = ObjectUtils.compare(value, getUpper(), true);
    return (isLowerInclusive() ? l <= 0 : l < 0) && (isUpperInclusive() ? u <= 0 : u < 0);

  }

  private void fromString(String rangeText) {
    if (rangeText == null || rangeText.isEmpty()) {
      return;
    }
    Matcher matcher = pattern.matcher(rangeText.trim());
    if (!matcher.matches()) {
      throw new IllegalArgumentException("invalid range");
    }
    setLower(parseEndpoint(matcher.group(2)));
    setLowerInclusive(isInclusive(matcher.group(1)));
    setUpper(parseEndpoint(matcher.group(3)));
    setUpperInclusive(isInclusive(matcher.group(4)));
  }

  private T parseEndpoint(String input) {
    if (input == null || "".equals(input) || INFINITY.equals(input)) {
      return null;
    }
    return parse(input);
  }

  private static boolean isInclusive(String boundLiteral) {
    switch (boundLiteral) {
    case "(":
    case ")":
      return false;
    case "[":
    case "]":
      return true;
    default:
      throw new IllegalArgumentException("Unknown bound: " + boundLiteral);
    }
  }

  public T getLower() {
    return lower;
  }

  public void setLower(T lower) {
    this.lower = lower;
  }

  public T getUpper() {
    return upper;
  }

  public void setUpper(T upper) {
    this.upper = upper;
  }

  public boolean isLowerInclusive() {
    return lowerInclusive;
  }

  public void setLowerInclusive(boolean lowerInclusive) {
    this.lowerInclusive = lowerInclusive;
  }

  public boolean isUpperInclusive() {
    return upperInclusive;
  }

  public void setUpperInclusive(boolean upperInclusive) {
    this.upperInclusive = upperInclusive;
  }

}

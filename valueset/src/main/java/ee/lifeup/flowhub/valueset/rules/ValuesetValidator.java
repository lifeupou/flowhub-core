/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset.rules;

import ee.lifeup.flowhub.valueset.ValueSetService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service("valuesetValidator")
public class ValuesetValidator implements RuleValidator<String> {
  private final ValueSetService valueSetService;

  @Autowired
  public ValuesetValidator(@Lazy ValueSetService valueSetService) {
    this.valueSetService = valueSetService;
  }

  @Override
  public boolean validate(Object value, String valueSetId) {
    if (value instanceof String) {
      String entity = (String) value;
      if (StringUtils.isEmpty(entity)) {
        return true;
      }
      return valueSetService.exists(valueSetId, entity);
    }

    throw new IllegalArgumentException("unknown type in valueset validator");
  }

}

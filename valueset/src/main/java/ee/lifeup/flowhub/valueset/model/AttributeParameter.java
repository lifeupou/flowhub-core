/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @see #EntityValidationService
 */
public class AttributeParameter {
  public static final String length = "length";
  public static final String range = "range";
  public static final String valueset = "valueset";

  public static final Map<String, List<String>> params;
  static {
    params = new HashMap<>();
    params.put(AttributeDatatype.text, Arrays.asList(length));
    params.put(AttributeDatatype.number, Arrays.asList(range));
    params.put(AttributeDatatype.date, Arrays.asList(range));
    params.put(AttributeDatatype.valueset, Arrays.asList(valueset));
  }

}

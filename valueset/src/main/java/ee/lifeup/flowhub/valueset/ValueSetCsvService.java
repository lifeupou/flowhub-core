/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset;

import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import com.univocity.parsers.csv.CsvWriter;
import com.univocity.parsers.csv.CsvWriterSettings;
import ee.lifeup.flowhub.util.DateUtil;
import ee.lifeup.flowhub.valueset.model.AttributeDatatype;
import ee.lifeup.flowhub.valueset.model.Entity;
import ee.lifeup.flowhub.valueset.model.ValueSetAttribute;
import ee.lifeup.flowhub.valueset.util.range.DateRange;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Stream.concat;

@Service
public class ValueSetCsvService {
  private static final String SEPARATOR = "#";
  private static final String ATTR_PREFIX = "attr" + SEPARATOR;
  private static final String DATE_FORMAT = DateUtil.ISO_DATETIME;

  public OutputStream compose(List<Entity> entities, List<ValueSetAttribute> attributes) {
    List<String> langs = findAllUsedLangs(entities, attributes);
    OutputStream out = new ByteArrayOutputStream();
    CsvWriterSettings settings = new CsvWriterSettings();
    settings.getFormat().setDelimiter(';');
    CsvWriter writer = new CsvWriter(out, settings);
    writer.writeHeaders(composeHeaders(attributes, langs));
    writer.writeRowsAndClose(entities.stream().map(e -> composeRow(e, attributes, langs)).collect(toList()));
    return out;
  }

  private List<String> composeHeaders(List<ValueSetAttribute> attributes, List<String> langs) {
    return concat(Stream.of("id", "code", "parent", "valid_from", "valid_to"), attributes.stream().flatMap(attr -> {
      if (AttributeDatatype.translations.equals(attr.getDatatype())) {
        return langs.stream().map(l -> ATTR_PREFIX + attr.getId() + SEPARATOR + l);
      }
      return Stream.of(ATTR_PREFIX + attr.getId());
    })).collect(toList());
  }

  @SuppressWarnings("unchecked")
  private List<String> findAllUsedLangs(List<Entity> entities, List<ValueSetAttribute> attributes) {
    List<String> translationsKeys = attributes.stream()
        .filter(a -> AttributeDatatype.translations.equals(a.getDatatype()))
        .map(a -> a.getId())
        .collect(toList());
    return entities.stream()
        .flatMap(e -> translationsKeys.stream()
            .flatMap(tr -> ((Map<String, ?>) e.getAttributes().get(tr)).keySet().stream()))
        .distinct()
        .collect(toList());
  }

  @SuppressWarnings("unchecked")
  private Object[] composeRow(Entity e, List<ValueSetAttribute> attrs, List<String> langs) {
    Collection<Object> values = new ArrayList<>();
    values.add(e.getId());
    values.add(e.getCode());
    values.add(e.getParent());
    values.add(e.getValid() == null ? "" : format(e.getValid().getLower()));
    values.add(e.getValid() == null ? "" : format(e.getValid().getUpper()));
    attrs.forEach(attr -> {
      if (AttributeDatatype.translations.equals(attr.getDatatype())) {
        langs.forEach(lang -> {
          values.add(((Map<String, ?>) e.getAttributes().get(attr.getId())).get(lang));
        });
        return;
      }
      values.add(e.getAttributes().get(attr.getId()));
    });
    return values.toArray(new Object[] {});
  }

  public List<Entity> parse(List<ValueSetAttribute> attributes, InputStream csv) {
    RowListProcessor processor = parseCsv(csv);
    List<String> headers = Arrays.asList(processor.getHeaders());
    return processor.getRows().stream().map(row -> {
      Entity d = new Entity();
      if (headers.indexOf("id") >= 0) {
        d.setId(Long.valueOf(row[headers.indexOf("id")]));
      }
      d.setCode(row[headers.indexOf("code")]);
      d.setParent(row[headers.indexOf("parent")]);

      DateRange dateRange = new DateRange();
      String valid_from = row[headers.indexOf("valid_from")];
      String valid_to = row[headers.indexOf("valid_to")];
      if(valid_from != null){
        dateRange.setLower(DateUtil.parse(valid_from));
      }
      if(valid_to != null){
        dateRange.setUpper(DateUtil.parse(valid_to));
      }
      d.setValid(dateRange);
      d.setAttributes(parseAttributes(attributes, headers, row));
      return d;
    }).collect(toList());
  }

  private Map<String, Object> parseAttributes(List<ValueSetAttribute> attributes, List<String> headers, String[] row) {
    Map<String, Object> values = new HashMap<>();
    attributes.forEach(attr -> {
      Object val = parseValue(attr, headers, row);
      if (val != null) {
        values.put(attr.getId(), val);
      }
    });
    return values;
  }

  private Object parseValue(ValueSetAttribute attr, List<String> headers, String[] row) {
    String csvkey = ATTR_PREFIX + attr.getId();
    if (AttributeDatatype.translations.equals(attr.getDatatype())) {
      String prefix = csvkey + SEPARATOR;
      return headers.stream()
          .filter(h -> h.startsWith(prefix))
          .map(h -> StringUtils.removeAll(h, prefix))
          .collect(toMap(lang -> lang, lang -> StringUtils.defaultString(row[headers.indexOf(prefix + lang)])));
    }
    String val = row[headers.indexOf(csvkey)];
    if (AttributeDatatype.booljan.equals(attr.getDatatype())) {
      return Boolean.parseBoolean(val);
    }
    if (AttributeDatatype.date.equals(attr.getDatatype())) {
      return DateUtil.parse(val);
    }
    if (AttributeDatatype.number.equals(attr.getDatatype())) {
      return Integer.valueOf(val);
    }
    return val;
  }

  private String format(Date date) {
    return date == null ? "" : new SimpleDateFormat(DATE_FORMAT).format(date);
  }

  private RowListProcessor parseCsv(InputStream csv) {
    RowListProcessor processor = new RowListProcessor();
    CsvParserSettings settings = new CsvParserSettings();
    settings.getFormat().setDelimiter(';');
    settings.setLineSeparatorDetectionEnabled(true);
    settings.setProcessor(processor);
    settings.setHeaderExtractionEnabled(true);
    new CsvParser(settings).parse(csv);
    return processor;
  }

}

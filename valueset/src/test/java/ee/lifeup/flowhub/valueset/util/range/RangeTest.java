/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset.util.range;

import org.junit.Assert;
import org.junit.Test;

public class RangeTest {

  @Test
  public void contains() {
    Assert.assertFalse(new IntRange("(,)").contains(null));
    Assert.assertFalse(new IntRange("(,20)").contains(null));
    Assert.assertFalse(new IntRange("(10,20)").contains(null));

    Assert.assertTrue(new IntRange("(,)").contains(2));
    Assert.assertTrue(new IntRange("(,20)").contains(2));
    Assert.assertFalse(new IntRange("(,20)").contains(20));
    Assert.assertTrue(new IntRange("(,20]").contains(20));
    Assert.assertFalse(new IntRange("(,20]").contains(21));

    Assert.assertFalse(new IntRange("(10,20)").contains(2));
    Assert.assertTrue(new IntRange("(10,20)").contains(12));
    Assert.assertFalse(new IntRange("(10,20)").contains(10));
    Assert.assertTrue(new IntRange("[10,20]").contains(10));
    Assert.assertFalse(new IntRange("[10,20]").contains(9));
  }

}

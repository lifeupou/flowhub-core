/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset;

import ee.lifeup.flowhub.util.JsonUtil;
import ee.lifeup.flowhub.valueset.model.AttributeDatatype;
import ee.lifeup.flowhub.valueset.model.ValueSet;
import ee.lifeup.flowhub.valueset.model.ValueSetAttribute;
import ee.lifeup.flowhub.valueset.model.ValueSetBehavior;
import ee.lifeup.flowhub.valueset.util.range.DateRange;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.text.SimpleDateFormat;
import java.util.*;

import static java.util.stream.Collectors.toMap;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = VsTestApplication.class)
@WebAppConfiguration
public class ValueSetTest extends BaseEndpointTest {

  @SuppressWarnings("unchecked")
  @Test
  public void attributeDefaultValues() throws Exception {
    List<ValueSetAttribute> attrs = new ArrayList<>();
    attrs.add(mewAttribute("textnull", AttributeDatatype.text, null));
    attrs.add(mewAttribute("textnenull", AttributeDatatype.text, "123"));
    attrs.add(mewAttribute("boolnull", AttributeDatatype.booljan, null));
    attrs.add(mewAttribute("boolnenull", AttributeDatatype.booljan, true));
    attrs.add(mewAttribute("numbernull", AttributeDatatype.number, null));
    attrs.add(mewAttribute("numbernenull", AttributeDatatype.number, 2));
    attrs.add(mewAttribute("transnull", AttributeDatatype.translations, null));
    attrs.add(mewAttribute("transnenull", AttributeDatatype.translations, Collections.singletonMap("et", "et")));

    String id = UUID.randomUUID().toString();
    ValueSet vs = new ValueSet();
    vs.setId(id);
    vs.setAttributes(attrs);
    vs.setBehavior("unique");
    put("/private/valueset/" + id, vs).andExpect(status().isNoContent());

    String resp =
        get("/private/valueset/" + id).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
    Map<String, Object> defaults = JsonUtil.fromJson(resp, ValueSet.class)
        .getAttributes()
        .stream()
        .collect(toMap(a -> a.getId(),
                       a -> a.getDefaultValue() == null ? "fhudwfzaebaaalieegfds" : a.getDefaultValue()));
    Assert.assertEquals(defaults.get("textnull"), "fhudwfzaebaaalieegfds");
    Assert.assertEquals(defaults.get("textnenull"), "123");
    Assert.assertEquals(defaults.get("boolnull"), "fhudwfzaebaaalieegfds");
    Assert.assertEquals(defaults.get("boolnenull"), true);
    Assert.assertEquals(defaults.get("numbernull"), "fhudwfzaebaaalieegfds");
    Assert.assertEquals(defaults.get("numbernenull"), 2);
    Assert.assertEquals(defaults.get("transnull"), "fhudwfzaebaaalieegfds");
    Assert.assertEquals(((Map<String, String>) defaults.get("transnenull")).get("et"), "et");
  }

  @Test
  public void attributeValidations() throws Exception {
    ValueSetAttribute attribute = new ValueSetAttribute();
    attribute.setDatatype("text");
    attribute.setId("akvalang");

    String id = UUID.randomUUID().toString();
    ValueSet vs = new ValueSet();
    vs.setId(id);
    vs.setAttributes(Collections.singletonList(attribute));
    vs.setBehavior("unique");
    put("/private/valueset/" + id, vs).andExpect(status().isNoContent());

    String entityUrl = "/private/valueset/" + id + "/entities";

    Map<String, Object> entity = new HashMap<>();
    entity.put("code", "a1");

    entity.put("attributes", Collections.singletonMap("doesnotexistsinattribute", id));
    post(entityUrl, JsonUtil.toJson(entity)).andExpect(status().isBadRequest());

    entity.put("attributes", Collections.singletonMap("akvalang", true)); // invalid type
    post(entityUrl, JsonUtil.toJson(entity)).andExpect(status().isBadRequest());

    entity.put("attributes", Collections.singletonMap("akvalang", "1234"));
    post(entityUrl, JsonUtil.toJson(entity)).andExpect(status().isNoContent());

    attribute.setParameters(Collections.singletonMap("length", "(3,5)"));// must be 4 symbols
    put("/private/valueset/" + id, vs).andExpect(status().isNoContent());

    entity.put("attributes", Collections.singletonMap("akvalang", "123"));
    post(entityUrl, JsonUtil.toJson(entity)).andExpect(status().isBadRequest());
    entity.put("attributes", Collections.singletonMap("akvalang", "12345"));
    post(entityUrl, JsonUtil.toJson(entity)).andExpect(status().isBadRequest());
    entity.put("attributes", Collections.singletonMap("akvalang", "1233"));
    entity.put("code", "a2");
    post(entityUrl, JsonUtil.toJson(entity)).andExpect(status().isNoContent());
  }

  @Test
  public void attributeOrder() throws Exception {
    String id = UUID.randomUUID().toString();
    ValueSet vs = new ValueSet();
    vs.setId(id);
    vs.setBehavior(ValueSetBehavior.temporal);
    vs.setAttributes(new ArrayList<>());

    vs.getAttributes().add(mewAttribute("one", "text", null));
    vs.getAttributes().add(mewAttribute("two", "text", null));
    vs.getAttributes().add(mewAttribute("three", "text", null));

    put("/private/valueset/" + id, vs).andExpect(status().isNoContent());

    get("/private/valueset/" + id).andExpect(status().isOk())
        .andExpect(jsonPath("$.attributes[0].id", equalTo("one")))
        .andExpect(jsonPath("$.attributes[1].id", equalTo("two")))
        .andExpect(jsonPath("$.attributes[2].id", equalTo("three")));

    List<ValueSetAttribute> reordered =
        Arrays.asList(vs.getAttributes().get(1), vs.getAttributes().get(2), vs.getAttributes().get(0));
    vs.setAttributes(reordered);
    put("/private/valueset/" + id, vs).andExpect(status().isNoContent());
    get("/private/valueset/" + id).andExpect(status().isOk())
        .andExpect(jsonPath("$.attributes[0].id", equalTo("two")))
        .andExpect(jsonPath("$.attributes[1].id", equalTo("three")))
        .andExpect(jsonPath("$.attributes[2].id", equalTo("one")));
  }

  @Test
  public void temporalTests() throws Exception {
    String id = UUID.randomUUID().toString();
    ValueSet vs = new ValueSet();
    vs.setId(id);
    vs.setBehavior(ValueSetBehavior.temporal);
    put("/private/valueset/" + id, vs).andExpect(status().isNoContent());
    get("/private/valueset/" + id).andExpect(status().isOk())
        .andExpect(jsonPath("$.id", equalTo(id)))
        .andExpect(jsonPath("$.entities").exists())
        .andExpect(jsonPath("$.version").doesNotExist());
    post("/private/valueset/" + id + "/versions", "{}").andExpect(status().is4xxClientError());
    post("/private/valueset/" + id + "/versions/1/copy", null).andExpect(status().is4xxClientError());
    post("/private/valueset/" + id + "/versions/1/publish", null).andExpect(status().is4xxClientError());
    Map<String, Object> entity = new HashMap<>();
    entity.put("code", "a1");
    post("/private/valueset/" + id + "/entities", JsonUtil.toJson(entity)).andExpect(status().isNoContent());
    get("/private/valueset/" + id).andExpect(status().isOk())
        .andExpect(jsonPath("$.entities").exists())
        .andExpect(jsonPath("$.entities[0]").exists())
        .andExpect(jsonPath("$.entities[0].code", equalTo("a1")));
  }

  @Test
  public void temporalDateOverlaps() throws Exception {
    String id = UUID.randomUUID().toString();
    ValueSet vs = new ValueSet();
    vs.setId(id);
    vs.setBehavior(ValueSetBehavior.temporal);
    put("/private/valueset/" + id, vs).andExpect(status().isNoContent());

    post("/private/valueset/" + id + "/entities", entity("a", "10", "20")).andExpect(status().isNoContent());
    post("/private/valueset/" + id + "/entities", entity("a", null, null)).andExpect(status().isBadRequest());
    post("/private/valueset/" + id + "/entities", entity("a", null, "5")).andExpect(status().isNoContent());
    post("/private/valueset/" + id + "/entities", entity("a", "6", "11")).andExpect(status().isBadRequest());
    post("/private/valueset/" + id + "/entities", entity("a", "11", "12")).andExpect(status().isBadRequest());
    post("/private/valueset/" + id + "/entities", entity("a", "12", "25")).andExpect(status().isBadRequest());
    post("/private/valueset/" + id + "/entities", entity("a", "21", "25")).andExpect(status().isNoContent());
    post("/private/valueset/" + id + "/entities", entity("a", "24", null)).andExpect(status().isBadRequest());
    post("/private/valueset/" + id + "/entities", entity("a", "26", null)).andExpect(status().isNoContent());
  }

  @Test
  public void uniqueDateOverlaps() throws Exception {
    String id = UUID.randomUUID().toString();
    ValueSet vs = new ValueSet();
    vs.setId(id);
    vs.setBehavior(ValueSetBehavior.unique);
    put("/private/valueset/" + id, vs).andExpect(status().isNoContent());

    post("/private/valueset/" + id + "/entities", entity("a", "10", "20")).andExpect(status().isNoContent());
    post("/private/valueset/" + id + "/entities", entity("a", null, null)).andExpect(status().isBadRequest());
    post("/private/valueset/" + id + "/entities", entity("a", null, "5")).andExpect(status().isBadRequest());
    post("/private/valueset/" + id + "/entities", entity("a", "6", "11")).andExpect(status().isBadRequest());
    post("/private/valueset/" + id + "/entities", entity("a", "11", "12")).andExpect(status().isBadRequest());
    post("/private/valueset/" + id + "/entities", entity("a", "12", "25")).andExpect(status().isBadRequest());
    post("/private/valueset/" + id + "/entities", entity("a", "21", "25")).andExpect(status().isBadRequest());
    post("/private/valueset/" + id + "/entities", entity("a", "24", null)).andExpect(status().isBadRequest());
    post("/private/valueset/" + id + "/entities", entity("a", "26", null)).andExpect(status().isBadRequest());
  }

  private String entity(String code, String from, String to) {
    try {
      Map<String, Object> entity = new HashMap<>();
      entity.put("code", "a1");
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      entity.put("valid",
                 new DateRange(from == null ? null : sdf.parse("2010-10-" + from),
                               to == null ? null : sdf.parse("2010-10-" + to)));
      return JsonUtil.toJson(entity);
    } catch (Exception e) {
      throw new RuntimeException("b00m");
    }
  }

  private ValueSetAttribute mewAttribute(String id, String type, Object defaultValue) {
    ValueSetAttribute attr = new ValueSetAttribute();
    attr.setId(id);
    attr.setDatatype(type);
    attr.setDefaultValue(defaultValue);
    return attr;
  }

}

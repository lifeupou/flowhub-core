/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.valueset;


import ee.lifeup.flowhub.util.JsonUtil;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

public abstract class BaseEndpointTest {
  protected MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
                                                  MediaType.APPLICATION_JSON.getSubtype(),
                                                  Charset.forName("utf8"));
  protected MockMvc mockMvc;

  @Autowired
  protected WebApplicationContext webApplicationContext;

  @Before
  public void setup() throws Exception {
    this.mockMvc = webAppContextSetup(webApplicationContext).build();
  }

  protected URI uri(String url, Object... args) {
    return UriComponentsBuilder.fromUriString(url).buildAndExpand(args).encode().toUri();
  }

  protected ResultActions get(URI uri) throws Exception {
    return mockMvc.perform(MockMvcRequestBuilders.get(uri));
  }

  protected ResultActions get(String uri) throws Exception {
    return get(uri(uri));
  }

  protected ResultActions post(URI uri, Object body) throws Exception {
    return mockMvc.perform(MockMvcRequestBuilders.post(uri).content(getString(body)).contentType(contentType));
  }

  protected ResultActions post(String uri, Object body) throws Exception {
    return post(uri(uri), body);
  }

  protected ResultActions put(URI uri, Object body) throws Exception {
    return mockMvc.perform(MockMvcRequestBuilders.put(uri).content(getString(body)).contentType(contentType));
  }

  protected ResultActions put(String uri, Object body) throws Exception {
    return put(uri(uri), body);
  }

  protected ResultActions delete(URI uri) throws Exception {
    return mockMvc.perform(MockMvcRequestBuilders.delete(uri));
  }

  protected ResultActions delete(String uri) throws Exception {
    return delete(uri(uri));
  }

  private String getString(Object body) {
    if (body == null) {
      return "";
    }
    if (body instanceof String) {
      return (String) body;
    }
    return JsonUtil.toJson(body);
  }

  protected ResultMatcher notFound() {
    return status().isBadRequest();
  }
}
